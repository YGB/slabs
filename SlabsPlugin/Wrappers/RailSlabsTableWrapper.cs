﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.Design;
using Topomatic.Alg.Runtime.Wrappers;
using Topomatic.Alg.Stationing;
using Topomatic.Cad.Foundation;
using Topomatic.ComponentModel;
using Topomatic.Controls;
using Topomatic.Stg;

namespace SlabsPlugin
{
    class RailSlabsTableWrapper : SimpleChangeTrackingWrapper, IActivator, ISupportClipboard
    {
        private RailSlabsTable m_Table;
        public sealed class RailSlabsTableItemWrapper : IStationingContainer
        {
            private RailSlabsTableWrapper m_Wrapper;
            private RailSlabsTableItem m_Item;
            private Alignment m_Alignment = null;

            internal RailSlabsTableItemWrapper(RailSlabsTableWrapper wrapper, RailSlabsTableItem item)
            {
                m_Wrapper = wrapper;
                m_Item = item;
                m_Alignment = wrapper.m_Table.Alignment;

            }
            public override string ToString()
            {
                return "Таблица раскладки рельсовых плит";
            }

            [DisplayName("Плита"), PropertyEditor(typeof(RegularSlabTypePropertyEditor))]
            public string Slab
            {
                get => m_Item.slab;
                set
                {
                    m_Item.slab = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Плита Профиль"), PropertyEditor(typeof(ProfileSlabTypePropertyEditor))]
            public string SlabProfile
            {
                get => m_Item.slabProfile;
                set
                {
                    m_Item.slabProfile = value;
                    m_Wrapper.IsChanged = true;
                }
            }


            [Category("Начало"), PropertyProvider(typeof(StationPropertyProvider))]
            public double StationStart
            {
                get => m_Item.stationStart;
                set
                {
                    m_Item.stationStart = value;
                    m_Wrapper.IsChanged = true;
                }
            }


            [Category("Конец"), PropertyProvider(typeof(StationPropertyProvider))]
            public double StationEnd
            {
                get => m_Item.stationEnd;
                set
                {
                    m_Item.stationEnd = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Угол")]
            public double Angle
            {
                get => m_Item.angle;
            }

            [DisplayName("Зазор до след.")]
            public string GapAfter
            {
                get {

                    var indx = m_Wrapper.m_Table.IndexOf(m_Item);
                    return indx < m_Wrapper.m_Table.Count-1 ? ((m_Wrapper.m_Table[indx+1] as RailSlabsTableItem).stationStart - (m_Wrapper.m_Table[indx] as RailSlabsTableItem).stationEnd).ToString("F3") : "0";
                }
            }

            [DisplayName("Зазор от пред.")]
            public string GapBefore
            {
                get
                {

                    var indx = m_Wrapper.m_Table.IndexOf(m_Item);
                    return indx > 0 ? ((m_Wrapper.m_Table[indx] as RailSlabsTableItem).stationStart - (m_Wrapper.m_Table[indx - 1] as RailSlabsTableItem).stationEnd).ToString("F3") : "0";
                }
            }

            [DisplayName("Точка вставки")]
            public Vector3D Location
            {
                get => m_Item.location;
            }


            [DisplayName("Конечная точка")]
            public Vector3D LocationEnd
            {
                get => m_Item.locationEnd;
            }

            [Browsable(false)]
            public RailSlabsTableItem Item
            {
                get
                {
                    return m_Item;
                }
                set
                {
                    m_Item = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [Browsable(false)]
            public IAlgStationing Stationing
            {
                get
                {
                    return m_Alignment.Stationing;
                }
            }
        }

        public RailSlabsTableWrapper(RailSlabsTable table)
        {
            m_Table = table;
            Items = new List<object>(m_Table.Count);
            for (int i = 0; i < m_Table.Count; i++)
            {
                Items.Add(new RailSlabsTableItemWrapper(this, (RailSlabsTableItem)m_Table[i]));
            }
        }

        #region IActivator Members

        public bool CanCreateInstance => true;

        public object CreateInstance()
        {
            return new RailSlabsTableItemWrapper(this, new RailSlabsTableItem());
        }

        #endregion

        #region ISupportClipboard Members

        private static readonly string m_ClipboardAliasName = "{D2E99C5E-0694-4048-A9DC-1A37653A109A}";

        public bool CanCopy => true;

        public bool CanPaste => true;

        public string AliasName => m_ClipboardAliasName;

        public void Load(object obj, StgNode node)
        {
            var item = obj as RailSlabsTableItemWrapper;
            if (item != null)
            {
                var RailSlabsTableItem = new RailSlabsTableItem();
                RailSlabsTableItem.LoadFromStg(node);
                item.Item = RailSlabsTableItem;
            }
        }

        public void Save(object obj, StgNode node)
        {
            var item = obj as RailSlabsTableItemWrapper;
            if (item != null)
            {
                item.Item.SaveToStg(node);
            }
        }

        #endregion

        #region SimpleChangeTrackingWrapper Members
        public override void AcceptChanges()
        {
            m_Table.Clear();
            for (int i = 0; i < Items.Count; i++)
            {
                var item = (RailSlabsTableItemWrapper)Items[i];
                m_Table.Add(new RailSlabsTableItem()
                {
                    slab = item.Slab,
                    slabProfile = item.SlabProfile,
                    angle = item.Angle,
                    location = item.Location,
                    stationEnd = item.StationEnd,
                    stationStart = item.StationStart
                }); ;
            }
        }

        public override bool IsReadOnly => false;

        #endregion

    }
}

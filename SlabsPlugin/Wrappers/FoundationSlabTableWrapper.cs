﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.Design;
using Topomatic.Alg.Runtime.Wrappers;
using Topomatic.Alg.Stationing;
using Topomatic.Cad.Foundation;
using Topomatic.ComponentModel;
using Topomatic.Controls;
using Topomatic.Stg;
using static Topomatic.Alg.Plan.PlanLine;

namespace SlabsPlugin
{
    class FoundationSlabTableWrapper : SimpleChangeTrackingWrapper, IActivator, ISupportClipboard
    {
        private FoundationSlabTable m_Table;
        public sealed class FoundationSlabTableItemWrapper : IStationingContainer
        {
            private FoundationSlabTableWrapper m_Wrapper;
            private FoundationSlabTableItem m_Item;
            private Alignment m_Alignment = null;
            private List<PlanPart> m_Plan_Parts;

            internal FoundationSlabTableItemWrapper(FoundationSlabTableWrapper wrapper, FoundationSlabTableItem item)
            {
                m_Wrapper = wrapper;
                m_Item = item;
                m_Alignment = wrapper.m_Table.Alignment;
                m_Plan_Parts = PlanConverter.AlignmentToRules(m_Alignment);

            }
            public override string ToString()
            {
                return "Таблица раскладки рельсовых плит";
            }

            [DisplayName("Плита"), PropertyEditor(typeof(RegularSlabTypePropertyEditor))]
            public string Slab
            {
                get => m_Item.slab;
                set
                {
                    m_Item.slab = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Высота")]
            public double Height
            {
                get => m_Item.height;
                set
                {
                    m_Item.height = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Ширина")]
            public double Width
            {
                get => m_Item.width;
                set
                {
                    m_Item.width = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Объем, м3")]
            public double Volume
            {
                get
                {
                    const double HMax = 150f* 0.001f;
                    double S1 = 1.060; // Это площадь сечения стандартной фундаментной плиты в прямом участке
                    double S2 = 1.060;
                    foreach (Vertex vertex in m_Alignment.Plan)
                    {
                        if (vertex.PlanData.StartStation != vertex.PlanData.EndStation && ((m_Item.stationStart >= vertex.PlanData.StartStation && m_Item.stationStart <= vertex.PlanData.EndStation)))
                        {
                            double dx = m_Item.stationStart;
                            double dL;
                            double dH = HMax; //максимальный dH в кривой 150, в переходных зависит от расстояния до прямого участка. в ММ
                            var pd = vertex.PlanData as SinglePlanData;
                            if (m_Item.stationStart >= pd.BTC && m_Item.stationStart <= pd.BCC) // Первая переходная
                            {
                                dx = m_Item.stationStart - pd.BTC;
                                dL = pd.BCC - pd.BTC;
                                dH = dx / dL * HMax;
                            } else if (m_Item.stationStart >= pd.ECC && m_Item.stationStart <= pd.ETC) // Вторая перерходая
                            {
                                dx = pd.ETC - m_Item.stationStart;
                                dL = pd.ETC - pd.ECC;
                                dH = dx / dL * HMax;
                            }
                            else // Кривая
                            {
                                dH = HMax; 
                            }
                            double h1 = 0.5625f* 0.001f + dH * 0.36;
                            double h4 = 538.5f* 0.001f + dH * 1.24137;
                            double L1 = 804.5f* 0.001f;
                            double L4 = 1996f* 0.001f;
                            double h6 = 562.5f* 0.001f + dH * 1.24137;
                            S1 = (h1 + h4) * 0.5 * L1 + (h4 + h6) * 0.5 * L4;
                        }
                    }

                    foreach (Vertex vertex in m_Alignment.Plan)
                    {
                        if (vertex.PlanData.StartStation != vertex.PlanData.EndStation && ((m_Item.stationEnd >= vertex.PlanData.StartStation && m_Item.stationEnd <= vertex.PlanData.EndStation)))
                        {
                            double dx = m_Item.stationEnd;
                            double dL;
                            double dH = HMax; //максимальный dH в кривой 150, в переходных зависит от расстояния до прямого участка. в ММ
                            var pd = vertex.PlanData as SinglePlanData;
                            if (m_Item.stationEnd >= pd.BTC && m_Item.stationEnd <= pd.BCC)
                            {
                                dx = m_Item.stationEnd - pd.BTC;
                                dL = pd.BCC - pd.BTC;
                                dH = dx / dL * HMax;
                            }
                            else if (m_Item.stationEnd >= pd.ECC && m_Item.stationEnd <= pd.ETC)
                            {
                                dx = pd.ETC - m_Item.stationEnd;
                                dL = pd.ETC - pd.ECC;
                                dH = dx / dL * HMax;
                            }
                            else
                            {
                                dH = HMax;
                            }
                            double h1 = 0.5625f* 0.001f + dH * 0.36;
                            double h4 = 538.5f* 0.001f + dH * 1.24137;
                            double L1 = 804.5f* 0.001f;
                            double L4 = 1996f* 0.001f;
                            double h6 = 562.5f* 0.001f + dH * 1.24137;
                            S1 = (h1 + h4) * 0.5 * L1 + (h4 + h6) * 0.5 * L4;

                        }
                    }
                    return (S1 + S2) / 2 * (m_Item.stationEnd - m_Item.stationStart);

                }
            }


            [Category("Начало"), PropertyProvider(typeof(StationPropertyProvider))]
            public double StationStart
            {
                get => m_Item.stationStart;
                set
                {
                    m_Item.stationStart = value;
                    m_Wrapper.IsChanged = true;
                }
            }


            [Category("Конец"), PropertyProvider(typeof(StationPropertyProvider))]
            public double StationEnd
            {
                get => m_Item.stationEnd;
                set
                {
                    m_Item.stationEnd = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Угол")]
            public double Angle
            {
                get => m_Item.angle;
            }

            [DisplayName("Зазор до след.")]
            public string GapAfter
            {
                get
                {

                    var indx = m_Wrapper.m_Table.IndexOf(m_Item);
                    return indx < m_Wrapper.m_Table.Count - 1 ? ((m_Wrapper.m_Table[indx + 1] as FoundationSlabTableItem).stationStart - (m_Wrapper.m_Table[indx] as FoundationSlabTableItem).stationEnd).ToString("F3") : "0";
                }
            }

            [DisplayName("Зазор от пред.")]
            public string GapBefore
            {
                get
                {

                    var indx = m_Wrapper.m_Table.IndexOf(m_Item);
                    return indx > 0 ? ((m_Wrapper.m_Table[indx] as FoundationSlabTableItem).stationStart - (m_Wrapper.m_Table[indx - 1] as FoundationSlabTableItem).stationEnd).ToString("F3") : "0";
                }
            }

            [DisplayName("Точка вставки")]
            public Vector3D Location
            {
                get => m_Item.location;
            }

            [DisplayName("Конечная точка")]
            public Vector3D LocationEnd
            {
                get => m_Item.locationEnd;
            }


            [Browsable(false)]
            public FoundationSlabTableItem Item
            {
                get
                {
                    return m_Item;
                }
                set
                {
                    m_Item = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [Browsable(false)]
            public IAlgStationing Stationing
            {
                get
                {
                    return m_Alignment.Stationing;
                }
            }
        }

        public FoundationSlabTableWrapper(FoundationSlabTable table)
        {
            m_Table = table;
            Items = new List<object>(m_Table.Count);
            for (int i = 0; i < m_Table.Count; i++)
            {
                Items.Add(new FoundationSlabTableItemWrapper(this, (FoundationSlabTableItem)m_Table[i]));
            }
        }

        #region IActivator Members

        public bool CanCreateInstance => true;

        public object CreateInstance()
        {
            return new FoundationSlabTableItemWrapper(this, new FoundationSlabTableItem());
        }

        #endregion

        #region ISupportClipboard Members

        private static readonly string m_ClipboardAliasName = "{D2E99C5E-0694-4048-A9DC-1A37653A109A}";

        public bool CanCopy => true;

        public bool CanPaste => true;

        public string AliasName => m_ClipboardAliasName;

        public void Load(object obj, StgNode node)
        {
            var item = obj as FoundationSlabTableItemWrapper;
            if (item != null)
            {
                var FoundationSlabTableItem = new FoundationSlabTableItem();
                FoundationSlabTableItem.LoadFromStg(node);
                item.Item = FoundationSlabTableItem;
            }
        }

        public void Save(object obj, StgNode node)
        {
            var item = obj as FoundationSlabTableItemWrapper;
            if (item != null)
            {
                item.Item.SaveToStg(node);
            }
        }

        #endregion

        #region SimpleChangeTrackingWrapper Members
        public override void AcceptChanges()
        {
            m_Table.Clear();
            for (int i = 0; i < Items.Count; i++)
            {
                var item = (FoundationSlabTableItemWrapper)Items[i];
                m_Table.Add(new FoundationSlabTableItem()
                {
                    height = item.Height,
                    width = item.Width,
                    angle = item.Angle,
                    location = item.Location,
                    stationEnd = item.StationEnd,
                    stationStart = item.StationStart
                });
            }
        }

        public override bool IsReadOnly => false;

        #endregion

    }
}

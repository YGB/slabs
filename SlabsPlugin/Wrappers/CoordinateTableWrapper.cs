﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.Design;
using Topomatic.Alg.Runtime.Wrappers;
using Topomatic.Alg.Stationing;
using Topomatic.Cad.Foundation;
using Topomatic.ComponentModel;
using Topomatic.Controls;
using Topomatic.Stg;

namespace SlabsPlugin
{
    class CoordinateTableWrapper : SimpleChangeTrackingWrapper, IActivator, ISupportClipboard
    {
        private CoordinateTable m_Table;
        public sealed class CoordinateTableItemWrapper : IStationingContainer
        {
            private CoordinateTableWrapper m_Wrapper;
            private CoordinateTableItem m_Item;
            private Alignment m_Alignment = null;

            internal CoordinateTableItemWrapper(CoordinateTableWrapper wrapper, CoordinateTableItem item)
            {
                m_Wrapper = wrapper;
                m_Item = item;
                m_Alignment = wrapper.m_Table.Alignment;

            }
            public override string ToString()
            {
                return "Таблица раскладки координат рельсовых плит";
            }

            [DisplayName("Плита"), PropertyEditor(typeof(RegularSlabTypePropertyEditor))]
            public string Slab
            {
                get => m_Item.slab;
                set
                {
                    m_Item.slab = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [Category("Пикентаж"), PropertyProvider(typeof(StationPropertyProvider))]
            public double StartStation
            {
                get => m_Item.startStation;
                set
                {
                    m_Item.startStation = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Нижний левый угол")]
            public Vector2D LowerLeft
            {
                get => m_Item.lowerLeft;
            }

            [DisplayName("Верхний левый угол")]
            public Vector2D UpperLeft
            {
                get => m_Item.upperLeft;
            }

            [DisplayName("Верхний правый угол")]
            public Vector2D UpperRight
            {
                get => m_Item.upperRight;
            }
            [DisplayName("Нижний правый угол")]
            public Vector2D LowerRight
            {
                get => m_Item.lowerRight;
            }


            [Browsable(false)]
            public CoordinateTableItem Item
            {
                get
                {
                    return m_Item;
                }
                set
                {
                    m_Item = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [Browsable(false)]
            public IAlgStationing Stationing
            {
                get
                {
                    return m_Alignment.Stationing;
                }
            }
        }

        public CoordinateTableWrapper(CoordinateTable table)
        {
            m_Table = table;
            Items = new List<object>(m_Table.Count);
            for (int i = 0; i < m_Table.Count; i++)
            {
                Items.Add(new CoordinateTableItemWrapper(this, (CoordinateTableItem)m_Table[i]));
            }
        }

        #region IActivator Members

        public bool CanCreateInstance => true;

        public object CreateInstance()
        {
            return new CoordinateTableItemWrapper(this, new CoordinateTableItem());
        }

        #endregion

        #region ISupportClipboard Members

        private static readonly string m_ClipboardAliasName = "{D2E99C5E-0694-4048-A9DC-1A37653A109A}";

        public bool CanCopy => true;

        public bool CanPaste => true;

        public string AliasName => m_ClipboardAliasName;

        public void Load(object obj, StgNode node)
        {
            var item = obj as CoordinateTableItemWrapper;
            if (item != null)
            {
                var CoordinateTableItem = new CoordinateTableItem();
                CoordinateTableItem.LoadFromStg(node);
                item.Item = CoordinateTableItem;
            }
        }

        public void Save(object obj, StgNode node)
        {
            var item = obj as CoordinateTableItemWrapper;
            if (item != null)
            {
                item.Item.SaveToStg(node);
            }
        }

        #endregion

        #region SimpleChangeTrackingWrapper Members
        public override void AcceptChanges()
        {
            m_Table.Clear();
            for (int i = 0; i < Items.Count; i++)
            {
                var item = (CoordinateTableItemWrapper)Items[i];
                m_Table.Add(new CoordinateTableItem()
                {
                    slab = item.Slab,
                    startStation = item.StartStation,
                    lowerLeft = item.LowerLeft,
                    upperLeft = item.UpperLeft,
                    upperRight = item.UpperRight,
                    lowerRight = item.LowerRight,

                });
            }
        }

        public override bool IsReadOnly => false;

        #endregion

    }
}

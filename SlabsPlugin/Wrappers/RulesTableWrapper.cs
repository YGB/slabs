﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.Design;
using Topomatic.Alg.Runtime.Wrappers;
using Topomatic.Alg.Stationing;
using Topomatic.ComponentModel;
using Topomatic.Controls;
using Topomatic.Dwg.Layer;
using Topomatic.Stg;
using static Topomatic.Alg.Plan.PlanLine;

namespace SlabsPlugin
{
    class RulesTableWrapper : SimpleChangeTrackingWrapper, IActivator, ISupportClipboard, ISupportInterpolation
    {
        private RulesTable m_Table;
        public sealed class RulesTableItemWrapper : IStationingContainer
        {
            private RulesTableWrapper m_Wrapper;
            private RulesTableItem m_Item;
            private Alignment m_Alignment = null;
            private List<PlanPart> m_Plan_Parts;

            internal RulesTableItemWrapper(RulesTableWrapper wrapper, RulesTableItem item)
            {
                m_Wrapper = wrapper;
                m_Item = item;
                m_Alignment = wrapper.m_Table.Alignment;
                m_Plan_Parts = PlanConverter.AlignmentToRules(m_Alignment);

            }
            public override string ToString()
            {
                return "Таблица правил";
            }

            [DisplayName("Плита"), PropertyEditor(typeof(RegularSlabTypePropertyEditor))]
            public string Slab
            {
                get => m_Item.slab;
                set
                {
                    m_Item.slab = value;

                    var profileSlab = value + "_Profile";

                    var cadView = SlabsConfig.Instance.cadView;
                    if (cadView == null)
                    {
                        cadView = SlabsConfig.Instance.module.CadView;
                        if (cadView == null)
                        {
                            profileSlab = null;
                        }

                    }

                    var layer = DrawingLayer.GetDrawingLayer(cadView);
                    if (layer != null)
                    {
                        var drawing = layer.Drawing;
                        var block = drawing.Blocks[profileSlab];

                        if (block == null)
                        {
                            profileSlab = null;
                        }
                    }

                    if (profileSlab != null)
                    {
                        m_Item.slabProfile = profileSlab;
                    }


                    m_Wrapper.IsChanged = true;
                }
            }

            [DisplayName("Плита профиль"), PropertyEditor(typeof(ProfileSlabTypePropertyEditor))]
            public string SlabProfile
            {
                get => m_Item.slabProfile;
                set
                {
                    m_Item.slabProfile = value;
                    m_Wrapper.IsChanged = true;
                }
            }


            [DisplayName("Зазор")]
            public double DefaultGap
            {
                get => m_Item.defaultGap;
                set
                {
                    m_Item.defaultGap = value;
                    m_Wrapper.IsChanged = true;
                }
            }


            [Category("Начало"), PropertyProvider(typeof(StationPropertyProvider)), StationFromPlan]
            public double StationStart
            {
                get => m_Item.stationStart;
                set
                {
                    int curIndx = m_Wrapper.Items.IndexOf(this);
                    if (curIndx > 0)
                    {
                        RulesTableItemWrapper prevItem = m_Wrapper.Items[curIndx - 1] as RulesTableItemWrapper;
                        if (value <= prevItem.StationEnd)
                        {
                            m_Item.stationStart = prevItem.StationEnd;
                            m_Wrapper.IsChanged = true;
                        }
                        else
                        {
                            m_Item.stationStart = value;
                            m_Wrapper.IsChanged = true;
                        }
                    }
                    else
                    {
                        m_Item.stationStart = value;
                        m_Wrapper.IsChanged = true;
                    }

                }
            }

            [DisplayName("Начало, м")]
            public double StationStartMeter
            {
                get => m_Item.stationStart;
                set
                {
                    int curIndx = m_Wrapper.Items.IndexOf(this);
                    if (curIndx > 0)
                    {
                        RulesTableItemWrapper prevItem = m_Wrapper.Items[curIndx - 1] as RulesTableItemWrapper;
                        if (value <= prevItem.StationEnd)
                        {
                            m_Item.stationStart = prevItem.StationEnd;
                            m_Wrapper.IsChanged = true;
                        }
                        else
                        {
                            m_Item.stationStart = value;
                            m_Wrapper.IsChanged = true;
                        }
                    }
                    else
                    {
                        m_Item.stationStart = value;
                        m_Wrapper.IsChanged = true;
                    }
                }

            }


            [Category("Конец"), PropertyProvider(typeof(StationPropertyProvider)), StationFromPlan]
            public double StationEnd
            {
                get => m_Item.stationEnd;
                set
                {
                    if (value < m_Item.stationStart)
                    {
                        m_Item.stationEnd = m_Item.stationStart;
                        m_Wrapper.IsChanged = true;
                    }
                    else
                    {
                        int curIndx = m_Wrapper.Items.IndexOf(this);
                        if (curIndx < m_Wrapper.Items.Count - 1)
                        {
                            RulesTableItemWrapper nextItem = m_Wrapper.Items[curIndx + 1] as RulesTableItemWrapper;
                            if (value >= nextItem.StationStart)
                            {
                                m_Item.stationEnd = nextItem.StationStart;
                                m_Wrapper.IsChanged = true;
                            }
                            else
                            {
                                m_Item.stationEnd = value;
                                m_Wrapper.IsChanged = true;
                            }
                        }
                        else
                        {
                            m_Item.stationEnd = value;
                            m_Wrapper.IsChanged = true;
                        }
                    }

                }
            }


            [DisplayName("Конец, м")]
            public double StationEndMeters
            {
                get => m_Item.stationEnd;
                set
                {
                    if (value < m_Item.stationStart)
                    {
                        m_Item.stationEnd = m_Item.stationStart;
                        m_Wrapper.IsChanged = true;
                    }
                    else
                    {
                        int curIndx = m_Wrapper.Items.IndexOf(this);
                        if (curIndx < m_Wrapper.Items.Count - 1)
                        {
                            RulesTableItemWrapper nextItem = m_Wrapper.Items[curIndx + 1] as RulesTableItemWrapper;
                            if (value >= nextItem.StationStart)
                            {
                                m_Item.stationEnd = nextItem.StationStart;
                                m_Wrapper.IsChanged = true;
                            }
                            else
                            {
                                m_Item.stationEnd = value;
                                m_Wrapper.IsChanged = true;
                            }
                        }
                        else
                        {
                            m_Item.stationEnd = value;
                            m_Wrapper.IsChanged = true;
                        }
                    }

                }
            }

            [Category("Расчетный конец"), PropertyProvider(typeof(StationPropertyProvider))]
            public double CalculatedEnd
            {
                get
                {
                    var cadView = SlabsConfig.Instance.cadView;
                    if (cadView == null)
                    {
                        cadView = SlabsConfig.Instance.module.CadView;
                        if (cadView == null)
                        {
                            return 0;
                        }

                    }

                    var layer = DrawingLayer.GetDrawingLayer(cadView);
                    if (layer != null)
                    {
                        var drawing = layer.Drawing;
                        var block = drawing.Blocks[m_Item.slab];

                        if (block == null)
                        {
                            return 0;
                        }
                        return m_Item.stationStart + (block.Bounds.Width + m_Item.defaultGap) * Math.Truncate((m_Item.stationEnd - m_Item.stationStart) / (block.Bounds.Width + m_Item.defaultGap));
                    }

                    return 0;
                }

            }



            [DisplayName("Остаток, мм")]
            public double Remainder
            {
                get
                {
                    var cadView = SlabsConfig.Instance.cadView;
                    if (cadView == null)
                    {
                        cadView = SlabsConfig.Instance.module.CadView;
                        if (cadView == null)
                        {
                            return 0;
                        }

                    }

                    var layer = DrawingLayer.GetDrawingLayer(cadView);
                    if (layer != null)
                    {
                        var drawing = layer.Drawing;
                        var block = drawing.Blocks[m_Item.slab];

                        if (block == null)
                        {
                            return 0;
                        }
                        var remainder = m_Item.stationEnd - (m_Item.stationStart + (block.Bounds.Width + m_Item.defaultGap) * Math.Truncate((m_Item.stationEnd - m_Item.stationStart) / (block.Bounds.Width + m_Item.defaultGap)));


                        return Math.Round(remainder * 1000, 1);
                    }

                    return 0;
                }

            }



            [DisplayName("Количество плит")]
            public double SlabCount
            {
                get
                {
                    var cadView = SlabsConfig.Instance.cadView;
                    if (cadView == null)
                    {
                        cadView = SlabsConfig.Instance.module.CadView;
                        if (cadView == null)
                        {
                            return 0;
                        }

                    }

                    var layer = DrawingLayer.GetDrawingLayer(cadView);
                    if (layer != null)
                    {
                        var drawing = layer.Drawing;
                        var block = drawing.Blocks[m_Item.slab];

                        if (block == null)
                        {
                            return 0;
                        }
                        return Math.Truncate((m_Item.stationEnd - m_Item.stationStart) / (block.Bounds.Width + m_Item.defaultGap));
                    }

                    return 0;
                }

            }


            [DisplayName("Компенсирующий зазор")]
            public double RecGap
            {
                get
                {
                    var cadView = SlabsConfig.Instance.cadView;
                    if (cadView == null)
                    {
                        cadView = SlabsConfig.Instance.module.CadView;
                        if (cadView == null)
                        {
                            return 0;
                        }

                    }

                    var layer = DrawingLayer.GetDrawingLayer(cadView);
                    if (layer != null)
                    {
                        var drawing = layer.Drawing;
                        var block = drawing.Blocks[m_Item.slab];

                        if (block == null)
                        {
                            return 0;
                        }
                        var remainder = m_Item.stationEnd - (m_Item.stationStart + (block.Bounds.Width + m_Item.defaultGap) * Math.Truncate((m_Item.stationEnd - m_Item.stationStart) / (block.Bounds.Width + m_Item.defaultGap)));


                        return m_Item.defaultGap + remainder / Math.Truncate((m_Item.stationEnd - m_Item.stationStart) / (block.Bounds.Width + m_Item.defaultGap));
                    }

                    return 0;
                }

            }

            [DisplayName("Описание")]
            public string Description
            {
                get
                {
                    string description = "";
                    foreach (var plantPart in m_Plan_Parts)
                    {
                        if (m_Item.stationStart >= plantPart.StartStation && m_Item.stationStart < plantPart.EndStation)
                        {
                            if (description == "")
                            {
                                description += plantPart.Description;
                            }
                            else
                            {
                                description += " | "  +  plantPart.Description;
                            }


                        }
                        else
                        {
                            if (m_Item.stationEnd > plantPart.StartStation && m_Item.stationEnd <= plantPart.EndStation)
                            {

                                if (description == "")
                                {
                                    description += plantPart.Description;
                                }
                                else
                                {
                                    description += " | " + plantPart.Description;
                                }

                            }
                        }

                    }
                    return description;
                }

            }

            [Browsable(false)]
            public RulesTableItem Item
            {
                get
                {
                    return m_Item;
                }
                set
                {
                    m_Item = value;
                    m_Wrapper.IsChanged = true;
                }
            }

            [Browsable(false)]
            public IAlgStationing Stationing
            {
                get
                {
                    return m_Alignment.Stationing;
                }
            }
        }

        public RulesTableWrapper(RulesTable table)
        {
            m_Table = table;
            Items = new List<object>(m_Table.Count);
            for (int i = 0; i < m_Table.Count; i++)
            {
                Items.Add(new RulesTableItemWrapper(this, (RulesTableItem)m_Table[i]));
            }
        }

        #region IActivator Members

        public bool CanCreateInstance => true;

        public object CreateInstance()
        {
            return new RulesTableItemWrapper(this, new RulesTableItem());
        }

        #endregion

        #region ISupportClipboard Members

        private static readonly string m_ClipboardAliasName = "{D2E99C5E-0694-4048-A9DC-1A37653A109A}";

        public bool CanCopy => true;

        public bool CanPaste => true;

        public string AliasName => m_ClipboardAliasName;



        public void Load(object obj, StgNode node)
        {
            var item = obj as RulesTableItemWrapper;
            if (item != null)
            {
                var rulesTableItem = new RulesTableItem();
                rulesTableItem.LoadFromStg(node);
                item.Item = rulesTableItem;
            }
        }

        public void Save(object obj, StgNode node)
        {
            var item = obj as RulesTableItemWrapper;
            if (item != null)
            {
                item.Item.SaveToStg(node);
            }
        }

        #endregion

        #region SimpleChangeTrackingWrapper Members
        public override void AcceptChanges()
        {
            m_Table.Clear();
            for (int i = 0; i < Items.Count; i++)
            {
                var item = (RulesTableItemWrapper)Items[i];
                m_Table.Add(new RulesTableItem() { slab = item.Slab,
                    slabProfile = item.SlabProfile,
                    defaultGap = item.DefaultGap,
                    stationEnd = item.StationEnd,
                    stationStart = item.StationStart
                });
            }
        }



        public bool Interpolate(object first, object second, object result)
        {

            if (first != null && second != null)
            {
                var f = first as RulesTableItemWrapper;
                var s = second as RulesTableItemWrapper;
                var res = result as RulesTableItemWrapper;
                res.StationEnd = s.StationStart;
                res.StationStart = f.StationEnd;
                res.Slab = f.Slab;
                res.DefaultGap = f.DefaultGap;
                return true;
            }
            return false;
        }

        public override bool IsReadOnly => false;

      

        public bool CanInterpolate 
        { 
                get {
                    if (m_Table.Count > 1)
                    {
                        return true;
                    } else
                    {
                        return false;
                    }
                }
            }
        }

        #endregion

    }


﻿using System.Collections.Generic;
using Topomatic.Cad.Foundation;
using Topomatic.Dwg.Layer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.ServiceClasses;
using Topomatic.ApplicationPlatform;
using Topomatic.Cad.Foundation;
using Topomatic.Dwg.Layer;
using Topomatic.Cad.View;
using System.IO;

namespace SlabsPlugin
{

    public sealed class SlabsConfig
    {

        public AppConfig appConfig;
        public ProjectConfig projectConfig;

        private static SlabsConfig instance = null;
        private static readonly object padlock = new object();

        public CadView cadView 
        {
            get; set;
        } 

        public Module module { get; set; }

        SlabsConfig()
        {

        }

        public static SlabsConfig Instance
        {
            get
            {
                lock (padlock)
                {
                    if (instance == null)
                    {
                        instance = new SlabsConfig();
                    }
                    return instance;
                }
            }
        }



        private List<SlabType> slabTypes;


        public string ImportDxf()
        {
            var projectDirectory = Path.GetDirectoryName(ApplicationHost.Current.ActiveProject.TargetProjectFile);
            const string settingsFileName = "DefaultSlabFilePath.txt";
            string settingsFilePath = Path.Combine(projectDirectory, settingsFileName);
            string filePath;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "dxf файлы (*.dxf)|*.dxf";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    using (StreamWriter writer = new StreamWriter(settingsFilePath))
                    {
                        writer.WriteLine(filePath);
                        defaultSlabsPath = filePath;
                        return filePath;
                    }
                }
            }
            return "";
        }

        public string ReadDefaultSlabsFile()
        {
            var projectDirectory = Path.GetDirectoryName(ApplicationHost.Current.ActiveProject.TargetProjectFile);
            const string settingsFileName = "DefaultSlabFilePath.txt";
            string settingsFilePath = Path.Combine(projectDirectory, settingsFileName);
            string filePath;
            if (File.Exists(settingsFilePath) == false)
            {

                filePath = ImportDxf();
            }
            else
            {
                using (StreamReader reader = new StreamReader(settingsFilePath))
                {
                    filePath = reader.ReadLine();

                }
                if (File.Exists(filePath) == false)
                {
                    filePath = ImportDxf();
                }
            }
            return filePath;
        }



        private string defaultSlabsPath;
        public string DefaultSlabsPath
        {
            get {
                if (defaultSlabsPath is null || defaultSlabsPath == "")
                {
                    defaultSlabsPath = ReadDefaultSlabsFile();
                    return defaultSlabsPath;
                }
                return defaultSlabsPath;
            }

            set => defaultSlabsPath = value;
        }

        public List<SlabType> SlabTypes
        {
            get => slabTypes;
            set => slabTypes = value;
        }

        /*
        public void LoadFromJson(string path, CadView cadView)
        {
            var config = JsonConvert.DeserializeObject<SlabsConfig>(path);
            instance =  config;
        }
        */

        public void AddBlockTypeToProject()
        {
            var layer = DrawingLayer.GetDrawingLayer(this.cadView);


        }
        /*
        public void SaveTestConfig(string path)
        {
            List<SlabType> slabs = new List<SlabType>();
            slabs.Add(new SlabType());
            var slab = new SlabType();
            slab.Name = "Плита_5330_2000";
            slab.DxfPath = "Плита_5330_2000.dxf";
            slab.Scale = 1.0;
            slabs.Add(slab);
            this.SlabTypes = slabs;
            string slabsText = JsonConvert.SerializeObject(this);
            System.IO.File.WriteAllText(path, slabsText);
        }
        */
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlabsPlugin
{
    /// <summary>
    /// Часть конфига, содержащая глобальные параметры, одинаковые для всех проектов.
    /// Все единицы измерения в метрах.
    /// </summary>
    public class AppConfig
    {
        
        public string MAJOR_SLAB = "NGP4.0-Р5330";
        public double MAJOR_GAP = 0.1;


    }
}

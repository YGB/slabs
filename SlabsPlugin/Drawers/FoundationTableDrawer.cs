﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Topomatic.Alg;
using Topomatic.Cad.Foundation;
using Topomatic.Cad.View;
using Topomatic.Dwg;
using Topomatic.Dwg.Entities;
using Topomatic.Dwg.Layer;


namespace SlabsPlugin.Drawers
{
    static public class FoundationTableDrawer
    {

        static public void DrawFoundationSlabWaterFillmentScheme(FoundationSlabTable table, RailSlabsTable railTable, Drawing drawing, double height, bool topDirection = true)
        {
            var watergapLabelTextSize = Settings.Setting<double>("WATERGAP_LABEL_TEXT_SIZE");
            var watergapSideDimHeight = Settings.Setting<double>("WATERGAP_SIDE_GAP_DIM_HEIGHT");
            var watergapSideGapLabelHeight = Settings.Setting<double>("WATERGAP_SIDE_GAP_LABEL_HEIGHT");
            var watergapSideGapLabelGap = Settings.Setting<double>("WATERGAP_SIDE_GAP_LABEL_GAP");
            double hydroHeight = Settings.Setting<double>("HYDRO_HEIGHT");
            double direction = topDirection ? 1 : -1;
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double hydro_gap_dim_height = Settings.Setting<double>("SCHEMA4_HYDRO_GAPS_DIMENSION_LABEL_HEIGHT");
            var layerName = "МодульРаскладкиПлит_Гидравлический шов";
            DwgLayer hydroLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                hydroLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                hydroLayer = drawing.Layers[layerName];
            }
            
            drawing.Layers.ActivateLayer(hydroLayer.Name);



            drawing.BeginUpdate();
            try
            {

                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    var foundationSlab =  drawing.Blocks[item.slab];

                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }


                    int distToPrev = 0;
                    double realdist;
                    if (previousItem != null)
                    {
                      
                        realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                        if (distToPrev > 20) {
                            continue;
                        }
                        var generatedBlockName = "Hydro" + distToPrev;


                        var blockSlab = drawing.Blocks[generatedBlockName];
                        if (blockSlab == null)
                        {
                            blockSlab = drawing.Blocks.Add(generatedBlockName);
                            //и квадрат

                            var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(-realdist, -hydroHeight), new Vector2D(-realdist, hydroHeight), new Vector2D(0.0, hydroHeight), new Vector2D(0.0, -hydroHeight) });
                            bline.Color = CadColor.Blue;
                            bline.Closed = true;

                            Vector2D[] points = new Vector2D[] { new Vector2D(-realdist, -hydroHeight), new Vector2D(-realdist, hydroHeight), new Vector2D(0.0, hydroHeight), new Vector2D(0.0, -hydroHeight) };
                            DwgHatch hatch = blockSlab.AddHatch(0, "Solid", points);
                            //Указываем свойства самой штриховки
                            hatch.Color = new CadColor(System.Drawing.Color.Blue);
                            hatch.Linetype = drawing.Linetypes.ByBlock;
                            hatch.Lineweight = Lineweight.ByBlock;
                            hatch.PatternScale = 1;
                        }

                        var slabLocation = new Vector2D(item.stationStart, height);
                        drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);


                        //Расстояние от конца последней рельсовой плиты, размещенной на блоке и  до конца фундаментной плиты.
                        var railSlabEndStation = railTable.Where(railSlab => (railSlab as RailSlabsTableItem).stationStart < previousItem.stationEnd &&
                        (railSlab as RailSlabsTableItem).stationStart > previousItem.stationStart).Max(railSlab => (railSlab as RailSlabsTableItem).stationEnd);
                        var firstGap = previousItem.stationEnd - railSlabEndStation;
                        if (firstGap > 0)
                        {
                            var firstGapSantimeters = (Math.Round(firstGap, 3) * 1000).ToString("#.##");

                            var railSlabStartStation = railTable.Where(railSlab => (railSlab as RailSlabsTableItem).stationStart < item.stationEnd &&
                           (railSlab as RailSlabsTableItem).stationStart > item.stationStart).Min(railSlab => (railSlab as RailSlabsTableItem).stationStart);

                            var secondGap = railSlabStartStation - item.stationStart;

                            var secondGapSantimeters = (Math.Round(secondGap, 3) * 1000).ToString("#.##");

                            var generated40gapName = "WaterGap" + firstGapSantimeters + realdist + secondGapSantimeters;
                            var blockgenerated40gap = drawing.Blocks[generated40gapName];
                            if (blockgenerated40gap == null)
                            {
                                blockgenerated40gap = drawing.Blocks.Add(generated40gapName);
                                var dim = blockgenerated40gap.AddDimAligned(new Vector2D(-realdist - firstGap, watergapSideDimHeight * direction), new Vector2D(-realdist, watergapSideDimHeight * direction), 
                                    new Vector2D(0,0));
                                dim.TextOverride = firstGapSantimeters;
                                dim.DimensionStyle = drawing.DimensionStyles[0];
                                dim.TextStyle = isocpeur_style;
                                dim.TextHeight = watergapLabelTextSize;
                                dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                                dim.Arrowhead2Type = AcDimArrowheadType.Oblique;
                                dim.TextPosition = new Vector2D(-realdist - firstGap - watergapSideGapLabelGap, watergapSideGapLabelHeight * direction);


                                var dim2 = blockgenerated40gap.AddDimAligned(new Vector2D(0, watergapSideDimHeight * direction), new Vector2D(0 + secondGap, watergapSideDimHeight * direction),
                                    new Vector2D(0,0));

                                dim2.TextOverride = firstGapSantimeters;
                                dim2.DimensionStyle = drawing.DimensionStyles[0];
                                dim2.TextStyle = isocpeur_style;
                                dim2.TextHeight = watergapLabelTextSize;
                                dim2.Arrowhead1Type = AcDimArrowheadType.Oblique;
                                dim2.Arrowhead2Type = AcDimArrowheadType.Oblique;
                                dim2.TextPosition = new Vector2D(secondGap + watergapSideGapLabelGap, watergapSideGapLabelHeight * direction);

                            }
                            drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockgenerated40gap.Name);

                        }

                    }

                    
                }
            }
            finally
            {
                drawing.EndUpdate();
            }



        }

        static public void DrawProjectFoundationSlabWaterFillmentScheme(Alignment sourceAlignment, Alignment targetAlignment, RailSlabsTable railTable, FoundationSlabTable table, Drawing drawing, double height, bool topDirection = true)
        {
            var watergapLabelTextSize = Settings.Setting<double>("WATERGAP_LABEL_TEXT_SIZE");
            var watergapSideDimHeight = Settings.Setting<double>("WATERGAP_SIDE_GAP_DIM_HEIGHT");
            var watergapSideGapLabelHeight = Settings.Setting<double>("WATERGAP_SIDE_GAP_LABEL_HEIGHT");
            var watergapSideGapLabelGap = Settings.Setting<double>("WATERGAP_SIDE_GAP_LABEL_GAP");
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            var direction = topDirection ? 1 : -1;
            double hydroHeight = Settings.Setting<double>("HYDRO_HEIGHT");
            double hydro_gap_dim_height = Settings.Setting<double>("SCHEMA4_HYDRO_GAPS_DIMENSION_LABEL_HEIGHT");

            var layerName = "МодульРаскладкиПлит_Гидравлический шов";
            DwgLayer hydroLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                hydroLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                hydroLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(hydroLayer.Name);

            drawing.BeginUpdate();
            try
            {

                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;
                    double realdist;
                    if (previousItem != null)
                    {
                        realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                        if (distToPrev > 20)
                        {
                            continue;
                        }
                        var generatedBlockName = "Hydro" + distToPrev;


                        var blockSlab = drawing.Blocks[generatedBlockName];
                        if (blockSlab == null)
                        {
                            blockSlab = drawing.Blocks.Add(generatedBlockName);
                            //и квадрат
                            var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(-realdist, -hydroHeight), 
                                new Vector2D(-realdist, hydroHeight), new Vector2D(0.0, hydroHeight), new Vector2D(0.0, -hydroHeight) });
                            bline.Color = CadColor.Blue;
                            bline.Closed = true;

                            Vector2D[] points = new Vector2D[] { new Vector2D(-realdist, -hydroHeight), 
                                new Vector2D(-realdist, hydroHeight), new Vector2D(0.0, hydroHeight), new Vector2D(0.0, -hydroHeight) };
                            DwgHatch hatch = blockSlab.AddHatch(0, "Solid", points);
                            //Указываем свойства самой штриховки
                            hatch.Color = new CadColor(System.Drawing.Color.Blue);
                            hatch.Linetype = drawing.Linetypes.ByBlock;
                            hatch.Lineweight = Lineweight.ByBlock;
                            hatch.PatternScale = 1;


                        }
                        double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                        var projScale = projectedStation / item.stationStart;
                        var slabLocation = new Vector2D(projectedStation, height);
                        drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);
                        //Расстояние от конца последней рельсовой плиты, размещенной на блоке и  до конца фундаментной плиты.
                        var railSlabEndStation = railTable.Where(railSlab => (railSlab as RailSlabsTableItem).stationStart < previousItem.stationEnd &&
                        (railSlab as RailSlabsTableItem).stationStart > previousItem.stationStart).Max(railSlab => (railSlab as RailSlabsTableItem).stationEnd);
                        var firstGap = previousItem.stationEnd - railSlabEndStation;
                        if (firstGap > 0)
                        {
                            var firstGapSantimeters = (Math.Round(firstGap, 3) * 1000).ToString("#.##");

                            var railSlabStartStation = railTable.Where(railSlab => (railSlab as RailSlabsTableItem).stationStart < item.stationEnd &&
                           (railSlab as RailSlabsTableItem).stationStart > item.stationStart).Min(railSlab => (railSlab as RailSlabsTableItem).stationStart);

                            var secondGap = railSlabStartStation - item.stationStart;

                            var secondGapSantimeters = (Math.Round(secondGap, 3) * 1000).ToString("#.##");

                            var generated40gapName = "WaterGap" + firstGapSantimeters + realdist + secondGapSantimeters + "_TOP";
                            var blockgenerated40gap = drawing.Blocks[generated40gapName];
                            if (blockgenerated40gap == null)
                            {
                                blockgenerated40gap = drawing.Blocks.Add(generated40gapName);
                                var dim = blockgenerated40gap.AddDimAligned(new Vector2D(-realdist - firstGap, watergapSideDimHeight * direction), new Vector2D(-realdist, watergapSideDimHeight * direction), 
                                    new Vector2D(0,0));
                                dim.TextOverride = firstGapSantimeters;
                                dim.DimensionStyle = drawing.DimensionStyles[0];
                                dim.TextStyle = isocpeur_style;
                                dim.TextHeight = watergapLabelTextSize;
                                dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                                dim.Arrowhead2Type = AcDimArrowheadType.Oblique;
                                dim.TextPosition = new Vector2D(-realdist - firstGap - watergapSideGapLabelGap, watergapSideGapLabelHeight * direction);


                                var dim2 = blockgenerated40gap.AddDimAligned(new Vector2D(0, watergapSideDimHeight * direction), new Vector2D(0 + secondGap, watergapSideDimHeight * direction),
                                    new Vector2D(0,0));

                                dim2.TextOverride = firstGapSantimeters;
                                dim2.DimensionStyle = drawing.DimensionStyles[0];
                                dim2.TextStyle = isocpeur_style;
                                dim2.TextHeight = watergapLabelTextSize;
                                dim2.Arrowhead1Type = AcDimArrowheadType.Oblique;
                                dim2.Arrowhead2Type = AcDimArrowheadType.Oblique;
                                dim2.TextPosition = new Vector2D(secondGap + watergapSideGapLabelGap, watergapSideGapLabelHeight * direction);

                            }
                            drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockgenerated40gap.Name);

                        }

                    }


                }
            }
            finally
            {

                drawing.EndUpdate();
            }



        }

        static public void DrawFoundationSlabScheme(FoundationSlabTable table, Drawing drawing, double height)
        {
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height;


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        //и квадрат
                        var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.6) });
                        
                        bline.Color = CadColor.ByBlock;
                        bline.Closed = true;



                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }

        }
        static public void DrawProjectFoundationSlabScheme(Alignment sourceAlignment, Alignment targetAlignment, FoundationSlabTable table, Drawing drawing, double height)
        {
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                double previousProjectedSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    double projScale = 1;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        projScale = (projectedStation - previousProjectedSta) / (item.stationStart - previousSta);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height;


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        //и квадрат
                        var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.6) });
                        bline.Closed = true;
                    }


                    var slabLocation = new Vector2D(projectedStation, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                    previousProjectedSta = projectedStation;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }

        }

        static public void DrawFoundationSlabScheme_Striped(FoundationSlabTable table, Drawing drawing, double height)
        {
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height;


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        //и квадрат
                        //var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.6) });
                        var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, 1.25), new Vector2D(0.0, -1.6), new Vector2D(item.width, -1.6), new Vector2D(item.width, 1.25) });
                        bline.Color = CadColor.ByBlock;
                        bline.Closed = false;

                        var stripedLine = drawing.Linetypes["GOST2.303 4"];

                        var bline2 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, 1.25), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, 1.25) });
                        bline2.Color = CadColor.ByBlock;
                        bline2.Closed = false;
                        bline2.Linetype = stripedLine;
                        bline2.LinetypeScale = 0.05;

                        var bline3 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6 + 0.4),  new Vector2D(item.width, -1.6 + 0.4)});
                        bline3.Color = CadColor.ByBlock;
                        bline3.Closed = false;
                        bline3.Linetype = stripedLine;
                        bline3.LinetypeScale = 0.05;


                        var bline4 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, 1.6 - 0.4), new Vector2D(item.width, 1.6 - 0.4) });
                        bline4.Color = CadColor.ByBlock;
                        bline4.Closed = false;
                        bline4.Linetype = stripedLine;
                        bline4.LinetypeScale = 0.05;


                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }

        }

        static public void DrawProjectFoundationSlabScheme_Striped( Alignment sourceAlignment, Alignment targetAlignment, FoundationSlabTable table, Drawing drawing,double height)
        {
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                double previousProjectedSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    double projScale = 1;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        projScale = (projectedStation - previousProjectedSta) / (item.stationStart - previousSta);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height;


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        //и квадрат
                        var bline = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.25), new Vector2D(0.0, 1.6),  new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.25) });
                        bline.Color = CadColor.ByBlock;
                        bline.Closed = false;

                        var stripedLine = drawing.Linetypes["GOST2.303 4"];

                        var bline2 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.25), new Vector2D(0.0, -1.6), new Vector2D(item.width, -1.6), new Vector2D(item.width, -1.25) });
                        bline2.Color = CadColor.ByBlock;
                        bline2.Closed = false;
                        bline2.Linetype = stripedLine;
                        bline2.LinetypeScale = 0.05;

                        var bline3 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6 + 0.4), new Vector2D(item.width, -1.6 + 0.4) });
                        bline3.Color = CadColor.ByBlock;
                        bline3.Closed = false;
                        bline3.Linetype = stripedLine;
                        bline3.LinetypeScale = 0.05;


                        var bline4 = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, 1.6 - 0.4), new Vector2D(item.width, 1.6 - 0.4) });
                        bline4.Color = CadColor.ByBlock;
                        bline4.Closed = false;
                        bline4.Linetype = stripedLine;
                        bline4.LinetypeScale = 0.05;

                    }


                    var slabLocation = new Vector2D(projectedStation, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                    previousProjectedSta = projectedStation;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }

        }

        static public void DrawFoundationSlabLabelsScheme(FoundationSlabTable table, Drawing drawing, double height, bool topDirection = true)
        {

            var foundationSlabLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_LABEL_TEXT_SIZE");
            var foundationSlabLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_LABEL_HEIGHT");
            var foundationSlabGapLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_GAP_LABEL_TEXT_SIZE");
            var foundationSlabGapLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_GAP_LABEL_HEIGHT");
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);

                        var dim = blockSlab.AddDimAligned(new Vector2D(0, 0), new Vector2D(item.width, 0), new Vector2D(item.width / 2, foundationSlabLabelTextHeight * dir));
                        dim.TextHeight = foundationSlabLabelTextSize;
                        var styleNames = drawing.DimensionStyles.Names;
                        dim.TextOverride = ((int)(Math.Round(item.width, 3) * 1000)).ToString();
                        dim.DimensionStyle = drawing.DimensionStyles[0];
                        dim.TextStyle = isocpeur_style;
                        dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                        dim.Arrowhead2Type = AcDimArrowheadType.Oblique;


                        if (previousItem != null && previousSta != -1)
                        {
                            var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                            var schemedist = item.stationStart - (previousSta + previousItem.width);

                            var len = ((int)(Math.Round(realdist, 3) * 1000));
                            if (len < 400)
                            {
                                var leader = new DwgLeader();
                                //точка полки выноски
                                leader.Position = new Vector2D(0.3, blockSlab.Bounds.Height * dir + 2 * dir);
                                //точки привязки выноски, может быть несколько
                                leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineUnderline;
                                //leader.Add(Vector2D.Empty);
                                leader.Add(new Vector2D(0 - schemedist / 2, foundationSlabGapLabelTextHeight * dir));


                                leader.Content = len.ToString();
                                if (len > 19 && len < 21)
                                {
                                    leader.Content = len.ToString() + " Деформационный шов";
                                }

                                if (len < 104 && len > 96)
                                {
                                    leader.Content = len.ToString() + " Выпуск воды из междупутья";
                                }

                                leader.Style = isocpeur_style;
                                leader.Height = foundationSlabGapLabelTextSize;
                                blockSlab.Add(leader);
                            }

                        }

                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawProjectFoundationSlabLabelsScheme(Alignment sourceAlignment,Alignment targetAlignment,FoundationSlabTable table, Drawing drawing, double height, bool topDirection = true)
        {
            var foundationSlabLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_LABEL_TEXT_SIZE");
            var foundationSlabLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_LABEL_HEIGHT");
            var foundationSlabGapLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_GAP_LABEL_TEXT_SIZE");
            var foundationSlabGapLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_GAP_LABEL_HEIGHT");
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double previousEndStation = -1;
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {
                double previousSta = -1;
                double previousProjectedSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;
                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    double projScale = 1;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        projScale = (projectedStation - previousProjectedSta) / (item.stationStart - previousSta);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";

                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        var dim = blockSlab.AddDimAligned(new Vector2D(0, 0), new Vector2D(item.width, 0), new Vector2D(item.width / 2, foundationSlabLabelTextHeight));
                        dim.TextHeight = foundationSlabLabelTextSize;
                        dim.TextOverride = ((int)(Math.Round(item.width, 3) * 1000)).ToString();
                        dim.DimensionStyle = drawing.DimensionStyles[0];
                        dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                        dim.Arrowhead2Type = AcDimArrowheadType.Oblique;
                        dim.TextStyle = isocpeur_style;
                        if (previousItem != null && previousSta != -1)
                        {
                            var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);

                            var schemedist = projectedStation - previousEndStation;

                            var len = ((int)(Math.Round(realdist, 3) * 1000));
                            if (len < 400)
                            {

                                var leader = new DwgLeader();
                                //точка полки выноски
                                leader.Position = new Vector2D(0.3, blockSlab.Bounds.Height * dir + 2 * dir);
                                //точки привязки выноски, может быть несколько
                                leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineUnderline;
                                //leader.Add(Vector2D.Empty);
                                leader.Add(new Vector2D(0 - realdist / 2, foundationSlabGapLabelTextHeight * dir));
                                leader.Content = len.ToString();
                                if (len > 19 && len < 21)
                                {
                                    leader.Content = len.ToString() + " Деформационный шов";
                                }

                                if (len < 104 && len > 96)
                                {
                                    leader.Content = len.ToString() + " Выпуск воды из междупутья";
                                }
                                leader.Style = isocpeur_style;
                                leader.Height = foundationSlabGapLabelTextSize;
                                blockSlab.Add(leader);
                            }

                        }

                    }

                    var slabLocation = new Vector2D(projectedStation, height);

                    var inserted = drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);
                    previousEndStation = inserted.Bounds.Max.X;
                    previousSta = item.stationStart;
                    previousProjectedSta = projectedStation;


                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }


        static public void DrawProjectAlternativeFoundationSlabLabelsScheme(Alignment sourceAlignment, Alignment targetAlignment, FoundationSlabTable table, Drawing drawing, double height, bool topDirection = true)
        {
            var foundationSlabAlternativeLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_ALTERNATIVE_LABEL_TEXT_SIZE");
            var foundationSlabAlternativeLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_ALTERNATIVE_LABEL_HEIGHT");

            var foundationSlabAlternativeGapLabelTextSize = Settings.Setting<double>("FOUNDATION_SLAB_ALTERNATIVE_GAP_LABEL_TEXT_SIZE");
            var foundationSlabAlternativeGapLabelTextHeight = Settings.Setting<double>("FOUNDATION_SLAB_ALTERNATIVE_GAP_LABEL_HEIGHT");

            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double previousEndStation = -1;
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {
                double previousSta = -1;
                double previousProjectedSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;
                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    double projScale = 1;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        projScale = (projectedStation - previousProjectedSta) / (item.stationStart - previousSta);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";

                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        var tx = blockSlab.AddText(((int)(Math.Round(item.width, 3) * 1000)).ToString(),
                            new Vector2D(item.width / 2, foundationSlabAlternativeLabelTextHeight * dir), foundationSlabAlternativeLabelTextSize, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                        tx.Style = isocpeur_style;
                        tx.Height = foundationSlabAlternativeLabelTextSize;
                        if (previousItem != null && previousSta != -1)
                        {
                            var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);

                            var schemedist = projectedStation - previousEndStation;

                            var len = ((int)(Math.Round(realdist, 3) * 1000));
                            if (len < 400)
                            {
                                var leader = new DwgLeader();
                                //точка полки выноски
                                leader.Position = new Vector2D(0.3, foundationSlabAlternativeGapLabelTextHeight * dir);
                                //точки привязки выноски, может быть несколько
                                leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineUnderline;
                                //leader.Add(Vector2D.Empty);
                                leader.Add(new Vector2D(0 - realdist / 2, (foundationSlabAlternativeGapLabelTextHeight - foundationSlabAlternativeGapLabelTextHeight /2) * dir));
                                leader.Content = len.ToString();
                                if (len > 19 && len < 21)
                                {
                                    leader.Content = len.ToString() + " Деформационный шов";
                                }

                                if (len < 104 && len > 96)
                                {
                                    leader.Content = len.ToString() + " Выпуск воды из междупутья";
                                }
                                leader.Style = isocpeur_style;
                                leader.Height = foundationSlabAlternativeGapLabelTextSize;
                                blockSlab.Add(leader);
                            }
                        }

                    }

                    var slabLocation = new Vector2D(projectedStation, height);

                    var inserted = drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);
                    previousEndStation = inserted.Bounds.Max.X;
                    previousSta = item.stationStart;
                    previousProjectedSta = projectedStation;


                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawFoundationSlabProfileScheme(FoundationSlabTable table, Drawing drawing, double height)
        {
            var layerName = "МодульРаскладкиПлит_Фундаментные плиты профиль";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    FoundationSlabTableItem item = table[i] as FoundationSlabTableItem;
                    FoundationSlabTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as FoundationSlabTableItem;
                    }

                    int distToPrev = 0;

                    if (previousItem != null && previousSta != -1)
                    {
                        var realdist = item.stationStart - (previousItem.stationStart + previousItem.width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }


                    var generatedBlockName = "FS" + ((int)(Math.Round(item.width, 3) * 1000)).ToString() + "D" + distToPrev.ToString("#.##") + "H" + height.ToString("#.##");
                    var foundSlabProfileName = "FoundationProfile" + generatedBlockName;
                    var foundSlabProfile = drawing.Blocks[foundSlabProfileName];
                    if (foundSlabProfile == null)
                    {
                        foundSlabProfile = drawing.Blocks.Add(foundSlabProfileName);
                        var fline = foundSlabProfile.AddPolyline(new Vector2D[] { new Vector2D(0.0, -0.1505), new Vector2D(0.0, 0.1505), new Vector2D(item.width, 0.1505), new Vector2D(item.width, -0.1505) });
                        fline.Color = CadColor.ByBlock;
                        fline.Closed = true;
                        Vector2D[] points = new Vector2D[] { new Vector2D(0.0, -0.1505), new Vector2D(0.0, 0.1505), new Vector2D(item.width, 0.1505), new Vector2D(item.width, -0.1505) };
                        DwgHatch hatch = foundSlabProfile.AddHatch(0, "ANSI31", points);
                    }

                

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, foundSlabProfile.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawConcreteFoundationFill(FoundationSlabTable table, CadView cadView, double height)
        {

        }
    
    }
}

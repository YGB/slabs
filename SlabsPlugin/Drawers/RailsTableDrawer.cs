﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Topomatic.Alg;
using Topomatic.Cad.Foundation;
using Topomatic.Cad.View;
using Topomatic.Dwg;
using Topomatic.Dwg.Entities;
using Topomatic.Dwg.Layer;

namespace SlabsPlugin.Drawers
{
    static class RailsTableDrawer
    {

        static public void DrawSlabsPlan()
        {
            throw new NotImplementedException();
        }



        static public void DrawRailWidthLabels(RailSlabsTable table, Drawing drawing, double height, Boolean topDirection = false)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Рельсовые плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    RailSlabsTableItem item = table[i] as RailSlabsTableItem;
                    RailSlabsTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as RailSlabsTableItem;
                    }

                    int distToPrev = 0;

                    var railSlab = drawing.Blocks[item.slab];
                    if (railSlab == null)
                    {
                        MessageBox.Show("Не найден рельсовый блок: " + item.slab);
                        break;
                    }

                    if (previousItem != null && previousSta != -1)
                    {
                        var previousItemSlab = drawing.Blocks[previousItem.slab];
                        if (previousItemSlab == null)
                        {
                            MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                            break;
                        }

                        var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "WidthLabel";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        


                            var width = item.stationEnd - item.stationStart;
                            var dim = blockSlab.AddDimAligned(new Vector2D(0, 0), new Vector2D(width, 0), new Vector2D(width / 2, 2 * dir));
                            dim.TextHeight = 2;
                            var styleNames = drawing.DimensionStyles.Names;
                            dim.TextOverride = ((int)(Math.Round(width, 3) * 1000)).ToString();
                            dim.DimensionStyle = drawing.DimensionStyles[0];
                            dim.TextStyle = isocpeur_style;
                            dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                            dim.Arrowhead2Type = AcDimArrowheadType.Oblique;


                        

                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawProjectRailWidthLabels(Alignment sourceAlignment, Alignment targetAlignment, RailSlabsTable table, Drawing drawing, double height, Boolean topDirection = false)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));


            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Рельсовые плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    RailSlabsTableItem item = table[i] as RailSlabsTableItem;
                    RailSlabsTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as RailSlabsTableItem;
                    }

                    int distToPrev = 0;

                    var railSlab = drawing.Blocks[item.slab];
                    if (railSlab == null)
                    {
                        MessageBox.Show("Не найден рельсовый блок: " + item.slab);
                        break;
                    }

                    if (previousItem != null && previousSta != -1)
                    {
                        var previousItemSlab = drawing.Blocks[previousItem.slab];
                        if (previousItemSlab == null)
                        {
                            MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                            break;
                        }

                        var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "WidthLabel";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                       

                            var width = item.stationEnd - item.stationStart;
                            var dim = blockSlab.AddDimAligned(new Vector2D(0, 0), new Vector2D(width, 0), new Vector2D(width / 2, 2 * dir));
                            dim.TextHeight = 2;
                            var styleNames = drawing.DimensionStyles.Names;
                            dim.TextOverride = ((int)(Math.Round(width, 3) * 1000)).ToString();
                            dim.DimensionStyle = drawing.DimensionStyles[0];
                            dim.TextStyle = isocpeur_style;
                            dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                            dim.Arrowhead2Type = AcDimArrowheadType.Oblique;
                        

                    }

                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    var projScale = Math.Abs(projectedStation / item.stationStart);
                    var slabLocation = new Vector2D(projectedStation, height);
                    //drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(projScale), 0, blockSlab.Name);

                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }


        static public void DrawRailLabels(RailSlabsTable table, Drawing drawing, double height, Boolean topDirection = false)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Рельсовые плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    RailSlabsTableItem item = table[i] as RailSlabsTableItem;
                    RailSlabsTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as RailSlabsTableItem;
                    }

                    int distToPrev = 0;

                    var railSlab = drawing.Blocks[item.slab];
                    if (railSlab == null)
                    {
                        MessageBox.Show("Не найден рельсовый блок: " + item.slab);
                        break;
                    }

                        if (previousItem != null && previousSta != -1)
                    {
                        var previousItemSlab = drawing.Blocks[previousItem.slab];
                        if (previousItemSlab == null)
                        {
                            MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                            break;
                        }

                        var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        /*var dim = blockSlab.AddDimAligned(new Vector2D(0, 1.6 * dir), new Vector2D(railSlab.Bounds.Width, 1.6 * dir), new Vector2D(railSlab.Bounds.Width / 2, 2 * dir));
                        dim.TextHeight = 2;
                        dim.TextOverride = ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString();
                        dim.DimensionStyle = drawing.DimensionStyles[0];*/
                        var tx = blockSlab.AddText(((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString(), 
                            new Vector2D(railSlab.Bounds.Width / 2, 2 * dir), .5, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                        tx.Style = isocpeur_style;

                        if (previousItem != null && previousSta != -1)
                        {
                            var previousItemSlab = drawing.Blocks[previousItem.slab];
                            if (previousItemSlab == null)
                            {
                                MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                                break;
                            }

                            var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                            var schemedist = item.stationStart - (previousSta + previousItemSlab.Bounds.Width);

                            var leader = new DwgLeader();
                            //точка полки выноски
                            leader.Position = new Vector2D(0, blockSlab.Bounds.Height  * dir + 2 * dir);
                            //точки привязки выноски, может быть несколько
                            leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineUnderline;
                            //leader.Add(Vector2D.Empty);
                            leader.Add(new Vector2D(0, 0));
                            leader.Content = ((int)(Math.Round(realdist, 3) * 1000)).ToString();
                            leader.Style = isocpeur_style;
                            leader.Height = 0.3;
                            blockSlab.Add(leader);
                                
                            }

                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }


        static public void DrawRailLabelsWithWidth(RailSlabsTable table, Drawing drawing, double height, Boolean topDirection = false)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Рельсовые плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    RailSlabsTableItem item = table[i] as RailSlabsTableItem;
                    RailSlabsTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as RailSlabsTableItem;
                    }

                    int distToPrev = 0;

                    var railSlab = drawing.Blocks[item.slab];
                    if (railSlab == null)
                    {
                        MessageBox.Show("Не найден рельсовый блок: " + item.slab);
                        break;
                    }

                    if (previousItem != null && previousSta != -1)
                    {
                        var previousItemSlab = drawing.Blocks[previousItem.slab];
                        if (previousItemSlab == null)
                        {
                            MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                            break;
                        }

                        var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        /*var dim = blockSlab.AddDimAligned(new Vector2D(0, 1.6 * dir), new Vector2D(railSlab.Bounds.Width, 1.6 * dir), new Vector2D(railSlab.Bounds.Width / 2, 2 * dir));
                        dim.TextHeight = 2;
                        dim.TextOverride = ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString();
                        dim.DimensionStyle = drawing.DimensionStyles[0];*/
                        var tx = blockSlab.AddText(((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString(),
                            new Vector2D(railSlab.Bounds.Width / 2, 2 * dir), .5, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                        tx.Style = isocpeur_style;

                        if (previousItem != null && previousSta != -1)
                        {
                            var previousItemSlab = drawing.Blocks[previousItem.slab];
                            if (previousItemSlab == null)
                            {
                                MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                                break;
                            }

                            var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                            var schemedist = item.stationStart - (previousSta + previousItemSlab.Bounds.Width);

                            var leader = new DwgLeader();
                            //точка полки выноски
                            leader.Position = new Vector2D(0, blockSlab.Bounds.Height * dir + 2 * dir);
                            //точки привязки выноски, может быть несколько
                            leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineUnderline;
                            //leader.Add(Vector2D.Empty);
                            leader.Add(new Vector2D(0, 0));
                            leader.Content = ((int)(Math.Round(realdist, 3) * 1000)).ToString();
                            leader.Style = isocpeur_style;
                            leader.Height = 0.3;
                            blockSlab.Add(leader);


                            var width = item.stationEnd - item.stationStart;
                            var dim = blockSlab.AddDimAligned(new Vector2D(0, 1.6 * dir), new Vector2D(width, 1.6 * dir), new Vector2D(width / 2, 1 * dir));
                            dim.TextHeight = 2;
                            var styleNames = drawing.DimensionStyles.Names;
                            dim.TextOverride = ((int)(Math.Round(width, 3) * 1000)).ToString();
                            dim.DimensionStyle = drawing.DimensionStyles[0];
                            dim.TextStyle = isocpeur_style;
                            dim.Arrowhead1Type = AcDimArrowheadType.Oblique;
                            dim.Arrowhead2Type = AcDimArrowheadType.Oblique;


                        }

                    }

                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawProjectRailLabels(Alignment sourceAlignment, Alignment targetAlignment, RailSlabsTable table, Drawing drawing, double height, Boolean topDirection = false)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));


            double dir = topDirection ? 1 : -1;
            var layerName = "МодульРаскладкиПлит_Рельсовые плиты подписи";


            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);

            drawing.BeginUpdate();
            try
            {

                double previousSta = -1;
                for (int i = 0; i < table.Count; i++)
                {
                    RailSlabsTableItem item = table[i] as RailSlabsTableItem;
                    RailSlabsTableItem previousItem = null;
                    if (i > 0)
                    {
                        previousItem = table[i - 1] as RailSlabsTableItem;
                    }

                    int distToPrev = 0;

                    var railSlab = drawing.Blocks[item.slab];
                    if (railSlab == null)
                    {
                        MessageBox.Show("Не найден рельсовый блок: " + item.slab);
                        break;
                    }

                    if (previousItem != null && previousSta != -1)
                    {
                        var previousItemSlab = drawing.Blocks[previousItem.slab];
                        if (previousItemSlab == null)
                        {
                            MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                            break;
                        }

                        var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                        distToPrev = (int)(Math.Round(realdist, 3) * 1000);
                    }

                    var generatedBlockName = "FS" + ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString() + "D" + distToPrev + "H" + height + "Label";


                    var blockSlab = drawing.Blocks[generatedBlockName];
                    if (blockSlab == null)
                    {
                        blockSlab = drawing.Blocks.Add(generatedBlockName);
                        /*var dim = blockSlab.AddDimAligned(new Vector2D(0, 1.6 * dir), new Vector2D(railSlab.Bounds.Width, 1.6 * dir), new Vector2D(railSlab.Bounds.Width / 2, 2 * dir));
                        dim.TextHeight = 2;
                        dim.TextOverride = ((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString();
                        dim.DimensionStyle = drawing.DimensionStyles[0];*/
                        var tx = blockSlab.AddText(((int)(Math.Round(railSlab.Bounds.Width, 3) * 1000)).ToString(),
                            new Vector2D(railSlab.Bounds.Width / 2, 2 * dir), .5, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);

                        tx.Style = isocpeur_style;

                        if (previousItem != null && previousSta != -1)
                        {
                            var previousItemSlab = drawing.Blocks[previousItem.slab];
                            if (previousItemSlab == null)
                            {
                                MessageBox.Show("Не найден рельсовый блок: " + previousItem.slab);
                                break;
                            }

                            var realdist = item.stationStart - (previousItem.stationStart + previousItemSlab.Bounds.Width);
                            var schemedist = item.stationStart - (previousSta + previousItemSlab.Bounds.Width);

                            var leader = new DwgLeader();
                            //точка полки выноски
                            leader.Position = new Vector2D(0, blockSlab.Bounds.Height / 2 * dir + 2 * dir);
                            //точки привязки выноски, может быть несколько
                            //leader.Add(Vector2D.Empty);
                            leader.Add(new Vector2D(0,0)) ;
                            leader.Content = ((int)(Math.Round(realdist, 3) * 1000)).ToString();
                            leader.Style = isocpeur_style;

                            leader.Height = 0.3;
                            blockSlab.Add(leader);

                            //var dim2 = blockSlab.AddDimAligned(new Vector2D(0, 1.6 * dir), new Vector2D(0 - schemedist, 1.6 * dir), new Vector2D(-(schemedist) / 2, 2.5 * dir));
                            //dim2.TextHeight = 2;
                            //dim2.TextOverride = ((int)(Math.Round(realdist, 3) * 1000)).ToString();
                            //dim2.DimensionStyle = drawing.DimensionStyles[0];
                        }

                    }

                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart, targetAlignment, sourceAlignment);
                    //var projScale = Math.Abs(projectedStation / item.stationStart);
                    var slabLocation = new Vector2D(projectedStation, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);
                    previousSta = item.stationStart;
                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }
        static public void DrawProjectRailLabels(RailSlabsTable table, CadView cadView, double height)
        {


        }
        static public void DrawPodlozhka83(RailSlabsTable table, Drawing drawing, double height)
        {

            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            foreach (RailSlabsTableItem item in table)
            {

                var layerName = "МодульРаскладкиПлит_Профиль промежуточного слоя";
                DwgLayer podlozhkaLayer;
                if (drawing.Layers.Names.ContainsKey(layerName) == false)
                {
                    podlozhkaLayer = drawing.Layers.Add(layerName);
                }
                else
                {
                    podlozhkaLayer = drawing.Layers[layerName];
                }
                
                var blockSlabProfile = drawing.Blocks[item.slabProfile];
                if (blockSlabProfile == null)
                {
                    MessageBox.Show(String.Format("Не найден блок профиля рельсовой плиты {0}", item.slabProfile));
                    return;
                }

                var blockname83 = "Podplozhka83" + ((int)(Math.Round(blockSlabProfile.Bounds.Width, 3) * 1000)).ToString();
                var block83 = drawing.Blocks[blockname83];
                if (block83 == null)
                {
                    block83 = drawing.Blocks.Add(blockname83);
                    var b83line = block83.AddPolyline(new Vector2D[] { new Vector2D(0, -0.0415),new Vector2D(0, 0.0415),
                                new Vector2D(blockSlabProfile.Bounds.Width, 0.0415),new Vector2D(blockSlabProfile.Bounds.Width, -0.0415) });
                    b83line.Color = CadColor.ByBlock;
                    b83line.Closed = true;
                    Vector2D[] points = new Vector2D[] { new Vector2D(0, -0.0415),new Vector2D(0, 0.0415),
                                new Vector2D(blockSlabProfile.Bounds.Width, 0.0415),new Vector2D(blockSlabProfile.Bounds.Width, -0.0415) };
                    DwgHatch hatch2 = block83.AddHatch(0, "ANSI31", points);
                }

                var block83location = new Vector2D(item.stationStart, 3 + blockSlabProfile.Bounds.Bottom + block83.Bounds.Bottom);

                drawing.Layers.ActivateLayer(podlozhkaLayer.Name);
                drawing.ActiveSpace.AddInsert(block83location, new Vector3D(1.0), 0, blockname83);

            }
        }

        static public void DrawSlabsScheme(RailSlabsTable table, Drawing drawing, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            var layerName = "МодульРаскладкиПлит_Рельсовые плиты";
            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);


            drawing.BeginUpdate();
            try
            {

                List<string> blocknames = new List<string>();

                foreach (RailSlabsTableItem item in table)
                {
                    var blockSlab = drawing.Blocks[item.slab];
                    if (blockSlab == null)
                    {
                        MessageBox.Show(String.Format("Не найден блок рельсовой плиты {0}", item.slab));
                        return;
                    }
                    var slabLocation = new Vector2D(item.stationStart - blockSlab.Bounds.Left, height);

                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);


                }
            }
            finally
            {

                drawing.EndUpdate();
            }
        }


        static public void DrawProjectSlabsScheme(Alignment sourceAlignment, Alignment targetAlignment, RailSlabsTable table, Drawing drawing, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            var layerName = "МодульРаскладкиПлит_Рельсовые плиты";
            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);


            drawing.BeginUpdate();
            try
            {

                List<string> blocknames = new List<string>();

                foreach (RailSlabsTableItem item in table)
                {
                    var blockSlab = drawing.Blocks[item.slab];
                    if (blockSlab == null)
                    {
                        MessageBox.Show(String.Format("Не найден блок рельсовой плиты {0}", item.slab));
                        return;
                    }
                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart - blockSlab.Bounds.Left, targetAlignment, sourceAlignment);
                    var slabLocation = new Vector2D(projectedStation, height);

                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlab.Name);


                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        public static void DrawSlabProfileScheme(RailSlabsTable table, Drawing drawing, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            var layerName = "МодульРаскладкиПлит_Рельсовые плиты профиль";
            DwgLayer railProfileLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railProfileLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railProfileLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railProfileLayer.Name);

            drawing.BeginUpdate();
            try
            {

                List<string> blocknames = new List<string>();

                foreach (RailSlabsTableItem item in table)
                {
                    var blockSlabProfile = drawing.Blocks[item.slabProfile];
                    if (blockSlabProfile == null)
                    {
                        MessageBox.Show(String.Format("Не найден блок рельсовой плиты {0}", item.slabProfile));
                        return;
                    }
                    var slabLocation = new Vector2D(item.stationStart, height);
                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlabProfile.Name);
                }
            }
            finally
            {

                drawing.EndUpdate();
            }

        }


        public static void DrawSlabsAsSimpleBlockScheme(RailSlabsTable table, Drawing drawing, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            var layerName = "МодульРаскладкиПлит_Рельсовые плиты";
            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);


            drawing.BeginUpdate();
            try
            {

                List<string> blocknames = new List<string>();

                foreach (RailSlabsTableItem item in table)
                {
                    var blockSlab = drawing.Blocks[item.slab];
                    if (blockSlab == null)
                    {
                        MessageBox.Show(String.Format("Не найден блок рельсовой плиты {0}", item.slab));
                        return;
                    }

                    var generatedSlabName = blockSlab.Name + "simplified";

                    var blockSlabSimplified = drawing.Blocks[generatedSlabName];
                    if (blockSlabSimplified == null)
                    {
                        blockSlabSimplified = drawing.Blocks.Add(generatedSlabName);
                        //и квадрат
                        var bline = blockSlabSimplified.AddPolyline(new Vector2D[] { blockSlab.Bounds.BottomLeft, blockSlab.Bounds.TopLeft, blockSlab.Bounds.TopRight, blockSlab.Bounds.BottomRight });
                        bline.Color = CadColor.ByBlock;
                        bline.Closed = true;
                    }
                

                    var slabLocation = new Vector2D(item.stationStart - blockSlab.Bounds.Left, height);

                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlabSimplified.Name);


                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }

        static public void DrawProjectSlabsAsSimpleBlockScheme(Alignment sourceAlignment, Alignment targetAlignment, RailSlabsTable table, Drawing drawing, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            var layerName = "МодульРаскладкиПлит_Рельсовые плиты";
            DwgLayer railLayer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                railLayer = drawing.Layers.Add(layerName);
            }
            else
            {
                railLayer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(railLayer.Name);


            drawing.BeginUpdate();
            try
            {

                List<string> blocknames = new List<string>();

                foreach (RailSlabsTableItem item in table)
                {
                    var blockSlab = drawing.Blocks[item.slab];
                    if (blockSlab == null)
                    {
                        MessageBox.Show(String.Format("Не найден блок рельсовой плиты {0}", item.slab));
                        return;
                    }


                    var generatedSlabName = blockSlab.Name + "simplified";
                    var blockSlabSimplified = drawing.Blocks[generatedSlabName];
                    if (blockSlabSimplified == null)
                    {
                        blockSlabSimplified = drawing.Blocks.Add(generatedSlabName);
                        //и квадрат
                        var bline = blockSlabSimplified.AddPolyline(new Vector2D[] { blockSlab.Bounds.BottomLeft, blockSlab.Bounds.TopLeft, blockSlab.Bounds.TopRight, blockSlab.Bounds.BottomRight });
                        bline.Color = CadColor.ByBlock;
                        bline.Closed = true;
                    }

                    double projectedStation = SlabsPlugin.ProjectStationToAlignment(item.stationStart - blockSlab.Bounds.Left, targetAlignment, sourceAlignment);
                    var slabLocation = new Vector2D(projectedStation, height);

                    drawing.ActiveSpace.AddInsert(slabLocation, new Vector3D(1.0), 0, blockSlabSimplified.Name);


                }
            }
            finally
            {
                drawing.EndUpdate();
            }
        }
    }
}


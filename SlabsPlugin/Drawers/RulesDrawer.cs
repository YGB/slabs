﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.Cad.Foundation;
using Topomatic.Dwg;
using Topomatic.Dwg.Entities;
using static SlabsPlugin.SlabsPlugin;

namespace SlabsPlugin.Drawers
{
    public static class RulesDrawer
    {


        public static void DrawPicketage(RulesTable table, Drawing drawing)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            var pkLineHeight = Settings.Setting<double>("PICKETAGE_LINE_HEIGHT");
            var pkLabelsHeight = Settings.Setting<double>("PK_LABELS_HEIGHT");
            var kmLabelTextSize = Settings.Setting<double>("KM_LABEL_TEXT_SIZE");
            var pkLabelTextSize = Settings.Setting<double>("PK_LABEL_TEXT_SIZE");

            if (table.Count > 0)
            {

                DwgLayer pkLayer;
                if (drawing.Layers.Names.ContainsKey("Пикетаж") == false)
                {
                    pkLayer = drawing.Layers.Add("Пикетаж");
                }
                else
                {
                    pkLayer = drawing.Layers["Пикетаж"];
                }
                drawing.Layers.ActivateLayer(pkLayer.Name);

                var block = drawing.Blocks["StStationing"];
                if (block != null)
                {
                    drawing.Blocks.Remove(block);
                    block = drawing.Blocks.Add("StStationing");
                }
                else
                {
                    block = drawing.Blocks.Add("StStationing");
                }


                var minPK = table.Min(p => ((RulesTableItem)p).stationStart);
                var maxPK = table.Max(p => ((RulesTableItem)p).stationEnd);


                List<double> kms = SlabsPlugin.GenKM(minPK, maxPK);


                List<Vector2D> st = new List<Vector2D>() { new Vector2D(kms[0], 0), new Vector2D(maxPK, 0) };
                var line = block.AddPolyline(st);
                if (kms.Count() > 0)
                {
                    foreach (var km in kms)
                    {
                        var x = km;
                        var y = pkLineHeight;
                        var vertexes = new List<Vector2D>();
                        vertexes.Add(new Vector2D(x, 0));
                        vertexes.Add(new Vector2D(x, y)); 
                        block.AddPolyline(vertexes);
                        int km_string = (int)(km / 1000);
                        var tx2 = block.AddText(String.Format("{0}", km_string),
                                new Vector3D(x, y + pkLabelsHeight, 0), 1.5, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.TopCenter);
                        tx2.Style = isocpeur_style;
                        tx2.Height = kmLabelTextSize;
                        for (int i = 1; i < 10; i++)
                        {
                            var xpk = km + i * 100;
                            if (xpk <= maxPK)
                            {
                                var ypk = y + 2;
                                var verpk = new List<Vector2D>();
                                verpk.Add(new Vector2D(xpk, 0));
                                verpk.Add(new Vector2D(xpk, ypk));
                                block.AddPolyline(verpk);
                                var tx1 = block.AddText(String.Format("{0}", i.ToString()),
                                    new Vector3D(xpk, ypk + pkLabelsHeight, 0), 1, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.TopCenter);
                                tx1.Style = isocpeur_style;
                                tx1.Height = pkLabelTextSize;
                                vertexes.Add(new Vector2D(xpk, 0));
                            }

                        }
                    }

                }
                drawing.ActiveSpace.AddInsert(new Vector2D(0, 0), new Vector3D(1.0), 0.0, "StStationing");
            }
            

        }

        public static void DrawWayDistanceLabel(double wayDistance, double bottomHeight, double topHeight, double minPK, double maxPK, Drawing drawing)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            string layerName = "МодульРаскладкиПлит_Междупутье";
            DwgLayer layer;
            if (drawing.Layers.Names.ContainsKey(layerName) == false)
            {
                layer = drawing.Layers.Add(layerName);
            }
            else
            {
                layer = drawing.Layers[layerName];
            }

            drawing.Layers.ActivateLayer(layer.Name);

            var block = drawing.Blocks[layer.Name];
            if (block != null)
            {
                drawing.Blocks.Remove(block);
                block = drawing.Blocks.Add(layer.Name);
            }
            else
            {
                block = drawing.Blocks.Add(layer.Name);
            }

            double currpk = minPK;
            while (currpk < maxPK)
            {
                var tx = block.AddText(wayDistance.ToString(),
                        new Vector2D(currpk, (topHeight - bottomHeight)/2), 0.4, 1.0, 1.57, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                tx.Style = isocpeur_style;
                tx.Height = 0.4;
                currpk += 40;
            }

            drawing.ActiveSpace.AddInsert(new Vector2D(minPK, bottomHeight), new Vector3D(1.0), 0.0, layer.Name);
        }

        public static void DrawWayAxis(RulesTable table, Drawing drawing, double height, string wayName)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            if (table.Count > 0)
            {

                var minPK = table.Min(p => ((RulesTableItem)p).stationStart);
                var maxPK = table.Max(p => ((RulesTableItem)p).stationEnd);
                var layerName = "МодульРаскладкиПлит_Ось пути";
                DwgLayer axisLayer;
                if (drawing.Layers.Names.ContainsKey(layerName) == false)
                {
                    axisLayer = drawing.Layers.Add(layerName);
                }
                else
                {
                    axisLayer = drawing.Layers[layerName];
                }

                drawing.Layers.ActivateLayer(axisLayer.Name);

                var generatedWayName = wayName + height.ToString("#.##");
                var block = drawing.Blocks[generatedWayName];
                if (block != null)
                {
                    drawing.Blocks.Remove(block);
                    block = drawing.Blocks.Add(generatedWayName);
                }
                else
                {
                    block = drawing.Blocks.Add(generatedWayName);
                }


                var vertexes = new List<Vector2D>();
                vertexes.Add(new Vector2D(0, 0));
                vertexes.Add(new Vector2D(maxPK - minPK, 0));
                var line =  block.AddPolyline(vertexes);
                line.Color = CadColor.Red;
    

                var tx = block.AddText(wayName,
                        new Vector2D(minPK - 2, 0), 1, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                tx.Color = CadColor.Red;
                tx.Style = isocpeur_style;
                tx.Height = 1.0;
                

                drawing.ActiveSpace.AddInsert(new Vector2D(minPK, height), new Vector3D(1.0), 0.0, generatedWayName);

            }
        }


        public static void DrawRulesBorders(RulesTable rulesTable, Alignment alignment, RailSlabsTable railSlabsTable, Drawing drawing, bool topDirection, double height)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
            List<StationBorder> borders = new List<StationBorder>();

            double endHeight = Settings.Setting<double>("PK_BORDER_TEXT_HEIGHT");
            double pkBorderTextSize = Settings.Setting<double>("PK_BORDER_TEXT_SIZE");

            double direction = topDirection ? 1 : -1;

            double? borderStationStart = null;
            double? borderStationEnd = null;
            int lastInsertIndx = 0;
            double lastEndStation = 0;
            var startLine = railSlabsTable.Min(p =>
            {
                var a = p as RailSlabsTableItem;
                return a.stationStart;
            });

            var endLine = railSlabsTable.Max(p =>
            {
                var a = p as RailSlabsTableItem;
                return a.stationEnd;
            });

            string borderDirection = topDirection ? "левый путь" : "правый путь";

            var block = drawing.Blocks[borderDirection];

            if (block == null)
            {
                block = drawing.Blocks.Add(borderDirection);
            }




            RulesTableItem firstRule = (RulesTableItem)rulesTable[0];


            List<double> bordersD = new List<double> { firstRule.stationStart };
            

            foreach (RulesTableItem rule in rulesTable)
            {
                var slabs = railSlabsTable.Where(p =>
                {
                    RailSlabsTableItem a = p as RailSlabsTableItem;
                    return a.stationStart >= rule.stationStart && a.stationStart < rule.stationEnd;
                });
                if (slabs.ToList().Count == 0) { continue; }
                var startPK = slabs.Min(p =>
                {
                    var a = p as RailSlabsTableItem;
                    return a.stationStart;
                });
                var endPK = slabs.Max(p =>
                {
                    var a = p as RailSlabsTableItem;
                    return a.stationEnd;
                });


                int firstSlabIndx = railSlabsTable.IndexOf(slabs.First() as RailSlabsTableItem);

                if (firstSlabIndx > 0)
                {
                    if ((railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > lastEndStation &&
                        (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > rule.stationStart)
                    {
                        startPK = (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationStart;
                    }
                }

                if (borderStationStart.HasValue == false || startPK < borderStationStart)
                {
                    borderStationStart = startPK;
                }

                if (borderStationEnd.HasValue == false || endPK > borderStationEnd)
                {
                    borderStationEnd = endPK;
                }
                StationBorder border = new StationBorder(startPK, endPK);
                if (borders.Count > 0)
                {
                    int lastIndx = borders.Count - 1;
                    if (borders[lastIndx].stationEnd > border.stationStart || border.stationStart - borders[lastIndx].stationEnd < 0.3)
                    {
                        borders[lastIndx] = new StationBorder(borders[lastIndx].stationStart, border.stationStart);
                    }
                }

                if (bordersD[bordersD.Count-1] != border.stationStart)
                {
                    bordersD.Add(border.stationStart);
                }

                bordersD.Add(border.stationEnd);
                borders.Add(border);
        }
            foreach (double border in bordersD)
            {

                int st;
                double plus;
                char? indx;



                var leader = new DwgLeader();
                //точка полки выноски
                leader.Position = new Vector2D(border, endHeight * direction);
                //точки привязки выноски, может быть несколько
                //leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.LastLineBottom;
                if (direction < 0)
                {
                    leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.FirstLineBottom;
                } else
                {
                    leader.HorizontalAlignment = Topomatic.Dwg.HorizontalAlignment.FirstLineUnderline;
                }
                //leader.Add(Vector2D.Empty);
                leader.Add(new Vector2D(border, 0));
                alignment.Stationing.StationToPk(border, out st, out indx, out plus);
                leader.Rotation = 1.5708;
                leader.Content = "ПК " + st + "+" + plus.ToString("#.##");
                leader.Style = isocpeur_style;
                leader.Height = pkBorderTextSize;
                block.Add(leader);

            }

            drawing.ActiveSpace.AddInsert(new Vector2D(0, height), new Vector3D(1.0), 0.0, borderDirection);
        }
        

        public static void DrawConcreteLayer(RulesTable table, RailSlabsTable rTable, Drawing drawing, double bottomHeight, double topHeight)
        {
            var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));

            DwgLayer asphaltHatchLayer;
            if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Заливка асфальта") == false)
            {
                asphaltHatchLayer = drawing.Layers.Add("МодульРаскладкиПлит_Заливка асфальта");
            }
            else
            {
                asphaltHatchLayer = drawing.Layers["МодульРаскладкиПлит_Заливка асфальта"];
            }
            drawing.Layers.ActivateLayer(asphaltHatchLayer.Name);



            double blockWIdth = Settings.Setting<double>("RAIL_BLOCK_WIDTH");

            var blockHalfWidth = blockWIdth/2;


            for (int ruleIndx = 0; ruleIndx < table.Count; ruleIndx++)
            {


            RulesTableItem rule = (RulesTableItem)table[ruleIndx];
            double start = -1;
            double end = -1;
            foreach (RailSlabsTableItem rail in rTable)
            {
                if (rail.stationStart >= rule.stationStart && rail.stationStart < rule.stationEnd)
                {
                    if (start == -1)
                    {
                        start = rail.stationStart;
                    }

                    if (rail.stationEnd > end)
                    {
                        end = rail.stationEnd + rule.defaultGap;
                    }
                }
            }


            double calculatedRuleWidth = end - start;


            var railBlock = drawing.Blocks[rule.slab];

            DwgHatch hatch = new DwgHatch();
            //Может содержать в себе несколько внутренних контуров, представленных в виде класса BoundaryPath
            BoundaryPathList boundaryPath = new BoundaryPathList();
            //Класс контура представляющий собой пололинию
            PolylineBoundaryPath bPolyline = new PolylineBoundaryPath();
            boundaryPath.Add(bPolyline);
            List<Vector2D> points = new List<Vector2D>();
            points.Add(new Vector2D(start, bottomHeight + blockHalfWidth));
            points.Add(new Vector2D(start, topHeight - blockHalfWidth));
            points.Add(new Vector2D(start + calculatedRuleWidth, topHeight - blockHalfWidth));
            points.Add(new Vector2D(start + calculatedRuleWidth, bottomHeight + blockHalfWidth));


            foreach (Vector2D g in points)
            {
                bPolyline.Add(new BugleVector2D(g, 0));
            }
            bPolyline.IsClosed = true;
            //Добавляем набор контуров в штриховку
            hatch.BoundaryPath.Assign(boundaryPath);
            //Указываем свойства самой штриховки
            hatch.PatternName = "Solid";
            hatch.Color = new CadColor(System.Drawing.Color.Silver);
            hatch.Linetype = drawing.Linetypes.ByBlock;
            hatch.Lineweight = Lineweight.ByBlock;
            hatch.PatternScale = 1;


            //Добавляем штриховку на чертёж
            drawing.ActiveSpace.Add(hatch);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using Topomatic.Alg;
using Topomatic.Alg.Core;
using Topomatic.Alg.Runtime.ServiceClasses;
using Topomatic.ApplicationPlatform;
using Topomatic.Cad.Foundation;
using Topomatic.Controls.Dialogs;
using Topomatic.Core.Collections;
using Topomatic.Core.ServiceClasses;
using Topomatic.Dwg;
using Topomatic.Dwg.Layer;
using Topomatic.Dwg.Entities;
using Topomatic.Cad.View;
using System.IO;
using Topomatic.Acax.Import.Dxf;
using System.Configuration;
using System.Reflection;
using System.Globalization;

namespace SlabsPlugin
{
    public static class Settings
    {
        public static T Setting<T>(string name)
        {
             

            Configuration appConfig = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
            AppSettingsSection AppSettings = appConfig.AppSettings;
            NumberFormatInfo nfi = new NumberFormatInfo()
            {
                NumberGroupSeparator = "",
                CurrencyDecimalSeparator = "."
            };
            try
            {
                return (T)Convert.ChangeType(AppSettings.Settings[name].Value, typeof(T), nfi);
            } catch (Exception e)
            {
                MessageBox.Show("Неверный тип значения в конфигурационном файле для ключа " + name);

                throw e;
            }
        }
    }



    public class SlabsPlugin : Topomatic.ApplicationPlatform.Module
    {



        public SlabsPlugin()
        {
            InitializeComponent();
        }

        public SlabsPlugin(IContainer container)
        {
            container.Add(this);
            InitializeComponent();
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return System.IO.Path.GetDirectoryName(path);
            }
        }



        private void CustomPopulateFoundationSlabTable(FoundationSlabTable foundationSlabtable, RailSlabsTable railSlabsTable, RulesTable rulesTable, Alignment alignment)
        {

            List<PlanPart> planParts = PlanConverter.AlignmentToRules(alignment);

            if (rulesTable.CheckEmptySlabs() == false)
            {
                MessageBox.Show(String.Format("Пустые имена блока в таблице правил подобъекта {0}. Заполните имена блоков.", alignment.GetName()));
                return;
            }

            const double ALLOWED_ERR = 1;

            var cadView = CadView;

            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;

            var entitiesToRemove = drawing.ActiveSpace.Where(ent => ent is DwgInsert).Where(ent => (ent as DwgInsert).Block.Name.StartsWith("Ф.блок"));

            drawing.ActiveSpace.Entities.RemoveAll(ent => entitiesToRemove.Contains(ent));

            drawing.Blocks.RemoveAll(p => p.Name.StartsWith("Ф.блок"));


            double startStation;
            double endStation;
            int foundationSlabCount = 0;

            var rules = rulesTable.ToList();

            if (rules.Count == 0)
            {
                return;
            }

            var currentRulesIndex = 0;

            var currentRul = rules[currentRulesIndex] as RulesTableItem?;

            var i = 0;


            int WATERGAP_NUMBER = Settings.Setting<int>("WATERGAP_NUMBER");
            int RAILSLABS_IN_FOUNDATIONSLAB_COUNT = Settings.Setting<int>("RAIL_SLABS_IN_FOUNDATION_SLAB_COUNT");


            while (i + RAILSLABS_IN_FOUNDATIONSLAB_COUNT < railSlabsTable.Count)
            {
                RailSlabsTableItem firstSlab = railSlabsTable[i] as RailSlabsTableItem;
                RailSlabsTableItem lastSlab = railSlabsTable[i + RAILSLABS_IN_FOUNDATIONSLAB_COUNT - 1] as RailSlabsTableItem;
                RailSlabsTableItem nextSlab = railSlabsTable[i + RAILSLABS_IN_FOUNDATIONSLAB_COUNT] as RailSlabsTableItem;

                string type;

                startStation = firstSlab.stationStart;
                endStation = lastSlab.stationEnd;


                double yt;
                double gt;

                //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                alignment.Transitions[0].RedProfile.SplineProfile.GetYG(firstSlab.stationStart, out yt, out gt);


                bool onCurve = false;
                var currentPlanPart = planParts.Where(p => p.StartStation > firstSlab.stationStart && p.EndStation < firstSlab.stationStart).ToList();
                if (currentPlanPart.Count > 0)
                {
                    if (currentPlanPart[0].Type == PlanPartType.Curve)
                    {
                        onCurve = true;
                    }
                }

                    if (Math.Abs(gt) < 0.002 || onCurve == false)
                    {

                        if (foundationSlabCount % WATERGAP_NUMBER == 0)

                        {
                            endStation = lastSlab.stationEnd + (nextSlab.stationStart - lastSlab.stationEnd) / 2.0 - 0.01;

                        }
                        else
                        {
                            RailSlabsTableItem previousSLab = railSlabsTable[i - 1] as RailSlabsTableItem;
                            if ((foundationSlabCount + 1) % WATERGAP_NUMBER == 0)
                            {

                                startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2.0 + 0.01;
                                endStation = lastSlab.stationEnd;
                            }
                            else
                            {
                                startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2 + 0.01; //0.01 - половина длины шва
                                endStation = lastSlab.stationEnd + (nextSlab.stationStart - lastSlab.stationEnd) / 2.0 - 0.01;
                            }
                        }
                    } else
                {
                    if (i > 0)
                    {
                        RailSlabsTableItem previousSLab = railSlabsTable[i - 1] as RailSlabsTableItem;
                        startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2 + 0.01; //0.01 - половина длины шва
                        endStation = lastSlab.stationEnd + (nextSlab.stationStart - lastSlab.stationEnd) / 2.0 - 0.01;
                    } else
                    {
                        startStation = firstSlab.stationStart;
                        endStation = lastSlab.stationEnd + (nextSlab.stationStart - lastSlab.stationEnd) / 2.0 - 0.01;
                    }
                }
                double distance = endStation - startStation;
                var generatedBlockName = "Ф.блок_" + ((int)(Math.Round(distance, 3) * 1000)).ToString();
                var block = drawing.Blocks[generatedBlockName];
                if (block == null)
                {
                    //если такого блока нет, создаем его
                    block = drawing.Blocks.Add(generatedBlockName);
                    //и квадрат
                    var line = block.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(distance, 1.6), new Vector2D(distance, -1.6) });
                    line.Color = CadColor.ByBlock;
                    line.Closed = true;
                }

                if (block.Bounds.Width == 0)
                {
                    drawing.Blocks.Remove(block);
                    block = drawing.Blocks.Add(generatedBlockName);
                    //и квадрат
                    var line = block.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(distance, 1.6), new Vector2D(distance, -1.6) });
                    line.Color = CadColor.ByBlock;
                    line.Closed = true;
                }


                Vector2D startPosition;
                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPosition);



                double angle;
                Vector2D pos;
                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out pos);
                var blockLen = block.Bounds.Width;
                List<double> intersectResultsRight = new List<double>();
                List<double> intersectResultsLeft = new List<double>();
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 270, 90, intersectResultsRight, 0);
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 90, 270, intersectResultsLeft, 0);
                List<double> intersections = intersectResultsRight.Concat(intersectResultsLeft).ToList();

                if (intersections.Count > 0)
                {
                    var posSta = intersections.Max();
                    Vector2D posFinal;

                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posFinal);
                    var v2 = posFinal - pos;
                    angle = v2.Angle;
                    Vector2D startPos;
                    alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPos);

                    //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation, out yt, out gt);
                    Vector3D startPos3D = new Vector3D(startPos.X, startPos.Y, yt);


                    Vector2D posEnd;

                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posEnd);
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation + distance + blockLen, out yt, out gt);
                    Vector3D posEnd3D = new Vector3D(posEnd.X, posEnd.Y, yt);


                    var foundationTableItem = new FoundationSlabTableItem() { 
                        stationStart = startStation,
                        stationEnd = startStation + distance, 
                        angle = angle, 
                        location = startPos3D,
                        locationEnd = posEnd3D,
                        slab = generatedBlockName, width = distance };

                    var inserted = false;

                    var currentRuleIndex = -1;

                    foreach (RulesTableItem rule in rulesTable)
                    {
                        if (foundationTableItem.stationStart >= rule.stationStart - ALLOWED_ERR && foundationTableItem.stationStart <= rule.stationEnd) // ALLOWED_ERR это допущение для второстепенного пути
                        {
                            currentRuleIndex = rulesTable.IndexOf(rule);
                        }
                    }

                    if (currentRuleIndex != -1)
                    {
                        RulesTableItem? rule = rulesTable[currentRuleIndex] as RulesTableItem?;
                        if (currentRuleIndex < rulesTable.Count - 1)
                        {
                            RulesTableItem? nextRule = rulesTable[currentRuleIndex += 1] as RulesTableItem?;
                            if (foundationTableItem.stationEnd <= rule.Value.stationEnd || rule.Value.stationEnd == nextRule.Value.stationStart) // -0.3 это допущение для второстепенного пути
                            {
                                foundationSlabtable.Add(foundationTableItem);
                                i += RAILSLABS_IN_FOUNDATIONSLAB_COUNT;
                                inserted = true;
                                foundationSlabCount += 1;
                            } else //HORRIBLE HACK
                            {
                                lastSlab = railSlabsTable[i + 1] as RailSlabsTableItem;
                                if (lastSlab.stationEnd >= rule.Value.stationEnd)
                                {
                                    lastSlab = railSlabsTable[i] as RailSlabsTableItem;
                                }

                                if (foundationSlabCount % WATERGAP_NUMBER == 0)
                                {
                                    type = "F";
                                    startStation = firstSlab.stationStart;
                                    endStation = lastSlab.stationEnd;

                                }
                                else
                                {
                                    RailSlabsTableItem previousSLab = railSlabsTable[i - 1] as RailSlabsTableItem;
                                    if ((foundationSlabCount + 1) % WATERGAP_NUMBER == 0)
                                    {
                                        type = "T";
                                        startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2.0 + 0.01;
                                        endStation = lastSlab.stationEnd;
                                    }
                                    else //if (foundationSlabCount - 2 % 3 == 0)
                                    {

                                        type = "S";
                                        startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2 + 0.01;
                                        endStation = lastSlab.stationEnd;
                                    }
                                }



                                distance = endStation - startStation;
                                generatedBlockName = "Ф.блок_" + ((int)(Math.Round(distance, 3) * 1000)).ToString();
                                block = drawing.Blocks[generatedBlockName];
                                if (block == null)
                                {
                                    //если такого блока нет, создаем его
                                    block = drawing.Blocks.Add(generatedBlockName);
                                    //и квадрат
                                    var line = block.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(distance, 1.6), new Vector2D(distance, -1.6) });
                                    line.Color = CadColor.ByBlock;
                                    line.Closed = true;
                                }


                                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPosition);
                                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out pos);
                                blockLen = block.Bounds.Width;
                                intersectResultsRight = new List<double>();
                                intersectResultsLeft = new List<double>();
                                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 270, 90, intersectResultsRight, 0);
                                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 90, 270, intersectResultsLeft, 0);
                                intersections = intersectResultsRight.Concat(intersectResultsLeft).ToList();

                                if (intersections.Count > 0)
                                {
                                    posSta = intersections.Max();

                                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posFinal);
                                    v2 = posFinal - pos;
                                    angle = v2.Angle;
                                    alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPos);



                                    //zdes


                                    //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation, out yt, out gt);
                                    startPos3D = new Vector3D(startPos.X, startPos.Y, yt);


 

                                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posEnd);
                                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation + distance + blockLen, out yt, out gt);
                                    posEnd3D = new Vector3D(posEnd.X, posEnd.Y, yt);


                                    foundationTableItem = new FoundationSlabTableItem() { stationStart = startStation, 
                                        stationEnd = startStation + distance, 
                                        angle = angle, 
                                        location = startPos3D, 
                                        locationEnd = posEnd3D,
                                        slab = generatedBlockName, 
                                        width = distance };
                                    foundationSlabtable.Add(foundationTableItem);
                                    if (firstSlab.stationEnd == lastSlab.stationEnd)
                                    {
                                        i += 1;
                                        foundationSlabCount = 0;
                                    }
                                    else
                                    {
                                        i += 2;
                                        foundationSlabCount = 0;
                                    }
                                    inserted = true;
                                } else
                                {
                                    i += 1;
                                }
                            }
                        }
                        else
                        {
                            foundationSlabtable.Add(foundationTableItem);
                            i += RAILSLABS_IN_FOUNDATIONSLAB_COUNT;
                            inserted = true;
                            foundationSlabCount += 1;
                        }
                    }
                    if (inserted == false)
                    {
                        foundationSlabCount = 0;
                        i += 1;
                    }
                }
                else
                {
                    i += 1;
                }
            }

            if (i < railSlabsTable.Count) // Последняя плита
            {
                RailSlabsTableItem firstSlab = railSlabsTable[i] as RailSlabsTableItem;
                RailSlabsTableItem lastSlab = railSlabsTable[railSlabsTable.Count - 1] as RailSlabsTableItem;


                string type;

                if (foundationSlabCount % WATERGAP_NUMBER == 0)
                {
                    type = "F";
                    startStation = firstSlab.stationStart;
                    endStation = lastSlab.stationEnd;

                }
                else
                {
                    RailSlabsTableItem previousSLab = railSlabsTable[i - 1] as RailSlabsTableItem;
                    if ((foundationSlabCount + 1) % WATERGAP_NUMBER == 0)
                    {
                        type = "T";
                        startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2.0 + 0.01;
                        endStation = lastSlab.stationEnd;
                    }
                    else //if (foundationSlabCount - 2 % 3 == 0)
                    {

                        type = "S";
                        startStation = firstSlab.stationStart - (firstSlab.stationStart - previousSLab.stationEnd) / 2 + 0.01;
                        endStation = lastSlab.stationEnd;
                    }
                }
                var distance = endStation - startStation;
                var generatedBlockName = "Ф.блок_" + ((int)(Math.Round(distance, 3) * 1000)).ToString();
                var block = drawing.Blocks[generatedBlockName];
                if (block == null)
                {
                    //если такого блока нет, создаем его
                    block = drawing.Blocks.Add(generatedBlockName);
                    //и квадрат
                    var line = block.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(distance, 1.6), new Vector2D(distance, -1.6) });
                    line.Color = CadColor.ByBlock;
                    line.Closed = true;
                }



                Vector2D startPosition;
                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPosition);



                double angle;
                Vector2D pos;
                alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out pos);
                var blockLen = block.Bounds.Width;
                List<double> intersectResultsRight = new List<double>();
                List<double> intersectResultsLeft = new List<double>();
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 270, 90, intersectResultsRight, 0);
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width, 90, 270, intersectResultsLeft, 0);
                List<double> intersections = intersectResultsRight.Concat(intersectResultsLeft).ToList();

                if (intersections.Count > 0)
                {
                    var posSta = intersections.Max();
                    Vector2D posFinal;

                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posFinal);
                    var v2 = posFinal - pos;
                    angle = v2.Angle;
                    Vector2D startPos;
                    alignment.Plan.CompoundLine.StaOffsetToPos(startStation, 0, out startPos);

                    double yt;
                    double gt;

                    //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation, out yt, out gt);
                    Vector3D startPos3D = new Vector3D(startPos.X, startPos.Y, yt);

                    Vector2D posEnd;

                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posEnd);
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(startStation + distance + blockLen, out yt, out gt);
                    Vector3D posEnd3D = new Vector3D(posEnd.X, posEnd.Y, yt);


                    var foundationTableItem = new FoundationSlabTableItem() { stationStart = startStation,
                        stationEnd = startStation + distance, 
                        angle = angle,
                        location = startPos3D, 
                        locationEnd = posEnd3D,
                        slab = generatedBlockName, 
                        width = distance };
                    foundationSlabtable.Add(foundationTableItem);
                }
            }


            foreach (var block in drawing.Blocks)
            {
                if (block.Name.StartsWith("Ф.блок") && block.Bounds.Width == 0)
                {
                    throw new Exception("JUY");
                }
            }

        }

        private void RecalculateRulesTableFromMainAlignment(Alignment main, Alignment sub)
        {
            object value;
            if (main.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                var plugin = (RulesTablePlugin)value;

                if (plugin.RulesTable.CheckEmptySlabs() == false)
                {
                    MessageBox.Show(String.Format("Пустые имена блока в таблице правил подобъекта {0}. Заполните имена блоков.", main.GetName()));
                    return;
                }



                object svalue;

                if (sub.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out svalue))
                {
                    var splugin = (RulesTablePlugin)svalue;
                    splugin.RulesTable = new RulesTable(sub);

                    foreach (RulesTableItem rule in plugin.RulesTable)
                    {
                        RulesTableItem newrule = new RulesTableItem() { slab = rule.slab, defaultGap = rule.defaultGap, slabProfile = rule.slabProfile };
                        Vector2D pos;
                        double offset;
                        double sta;
                        sub.Plan.CompoundLine.StaOffsetToPos(rule.stationStart, 0, out pos);
                        sub.Plan.CompoundLine.PosToStaOffset(pos, out sta, out offset);
                        newrule.stationStart = sta;
                        sub.Plan.CompoundLine.StaOffsetToPos(rule.stationEnd, 0, out pos);
                        sub.Plan.CompoundLine.PosToStaOffset(pos, out sta, out offset);
                        if (sta != null) {
                            newrule.stationEnd = sta;
                        } else {
                            newrule.stationEnd = sub.Stationing.Stations.Max();
                        }
                        splugin.RulesTable.Add(newrule);

                    }
                }
            }

        }


        private void DrawFoundationSlabs(FoundationSlabTable table)
        {
            Alignment alignment = m_Watcher.CoreAlignment;
            if (alignment == null)
            {
            }
            object value;
            var cadView = CadView;
            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;
            DwgLayer foundationLayer;
            if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Фундаментная плиты") == false)
            {
                foundationLayer = drawing.Layers.Add("МодульРаскладкиПлит_Фундаментная плиты");
                drawing.Layers.ActivateLayer(foundationLayer.Name);
            } else
            {
                foundationLayer = drawing.Layers["МодульРаскладкиПлит_Фундаментная плиты"];
                drawing.Layers.ActivateLayer(foundationLayer.Name);
            }


            foundationLayer.EraseEntities();
            drawing.BeginUpdate();
            try
            {

                foreach (FoundationSlabTableItem item in table)
                {
                    var blockSlab = drawing.Blocks[item.slab];
                    if (blockSlab == null)
                    {
                        //если такого блока нет, создаем его
                        blockSlab = drawing.Blocks.Add(item.slab);
                        //и квадрат
                        var line = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.6) });
                        line.Color = CadColor.ByBlock;
                        line.Closed = true;
                    } else if (blockSlab.Bounds.Width == 0)
                    {
                        drawing.Blocks.Remove(blockSlab);
                        blockSlab = drawing.Blocks.Add(item.slab);
                        //и квадрат
                        var line = blockSlab.AddPolyline(new Vector2D[] { new Vector2D(0.0, -1.6), new Vector2D(0.0, 1.6), new Vector2D(item.width, 1.6), new Vector2D(item.width, -1.6) });
                        line.Color = CadColor.ByBlock;
                        line.Closed = true;
                    }


                    drawing.ActiveSpace.AddInsert(item.location, new Vector3D(1.0), item.angle, blockSlab.Name);
                }
            }
            finally
            {
                cadView.Unlock();
                cadView.Invalidate();
                drawing.EndUpdate();
            }
        }

        private RulesTable PopulateCoordinateTableFromMainAlignment(RulesTable mainRulesTable, Alignment mainRailsAlignment, Alignment alignment)
        {
            RulesTable rulesTable = new RulesTable(alignment);

            if (rulesTable.CheckEmptySlabs() == false)
            {
                MessageBox.Show(String.Format("Пустые имена блока в таблице правил подобъекта {0}. Заполните имена блоков.", alignment.GetName()));
                return rulesTable;
            }


            foreach (RulesTableItem rule in mainRulesTable)
            {
                double sta;
                double offset;
                Vector2D pos;
                RulesTableItem newRule = rule;
                if (mainRailsAlignment.Plan.CompoundLine.StaOffsetToPos(rule.stationStart, 0, out pos))
                {
                    if (alignment.Plan.CompoundLine.PosToStaOffset(pos, out sta, out offset))
                    {
                        newRule.stationStart = sta;
                    }
                }


                if (mainRailsAlignment.Plan.CompoundLine.StaOffsetToPos(rule.stationEnd, 0, out pos))
                {
                    if (alignment.Plan.CompoundLine.PosToStaOffset(pos, out sta, out offset))
                    {
                        newRule.stationEnd = sta;
                    }
                }

                rulesTable.Add(newRule);
            }
            return rulesTable;
        }

        private void PopulateRailsSlabsFromMainAlignment(RailSlabsTable mainRailsSlab, Alignment mainRailsAlignment, RailSlabsTable railSlabsTable, CoordinateTable coordinateTable, Alignment alignment)
        {
            var cadView = CadView;
            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;

            foreach (RailSlabsTableItem item in mainRailsSlab)
            {
                var block = drawing.Blocks[item.slab];
                if (block == null)
                {
                    continue;
                }

                var blockDeltaLeft = block.Bounds.Left;

                double offset;
                double sta;
                alignment.Plan.CompoundLine.PosToStaOffset((Vector2D)item.location, out sta, out offset);
                Vector2D pos;
                alignment.Plan.CompoundLine.StaOffsetToPos(sta, 0, out pos);
                double angle;

                var blockLen = block.Bounds.Width;
                List<double> intersectResultsRight = new List<double>();
                List<double> intersectResultsLeft = new List<double>();
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width + blockDeltaLeft, 270, 90, intersectResultsRight, 0);
                alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width + blockDeltaLeft, 90, 270, intersectResultsLeft, 0);
                List<double> intersections = intersectResultsRight.Concat(intersectResultsLeft).ToList();
                if (intersections.Count > 0)
                {
                    var posSta = intersections.Max();
                    Vector2D posFinal;
                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posFinal);
                    var v2 = posFinal - pos;
                    sta += blockDeltaLeft;
                    var width = block.Bounds.Width;
                    double yt;
                    double gt;

                    //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(sta, out yt, out gt);
                    Vector3D pos3D = new Vector3D(pos.X, pos.Y, yt);


                    Vector2D posEnd;

                    alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posEnd);
                    alignment.Transitions[0].RedProfile.SplineProfile.GetYG(item.stationStart + blockLen, out yt, out gt);
                    Vector3D posEnd3D = new Vector3D(posEnd.X, posEnd.Y, yt);


                    var railSlabItem = new RailSlabsTableItem() { stationStart = sta,
                        width = width, 
                        angle = v2.Angle, 
                        location = pos3D, 
                        locationEnd = posEnd3D,
                        slab = item.slab, 
                        slabProfile = item.slabProfile, 
                        stationEnd = sta + blockLen };
                    var rec = PointsFromInsertPointsAngle(block, pos, v2.Angle);
                    var coordinateTableItem = new CoordinateTableItem() { slab = block.Name, startStation = item.stationStart, lowerLeft = rec.lowerLeft, upperLeft = rec.upperLeft, lowerRight = rec.lowerRight, upperRight = rec.upperRight };
                    coordinateTable.Add(coordinateTableItem);

                    railSlabsTable.Add(railSlabItem);
                }
                else
                {
                    continue;
                }
            }

        }


        private void PopulateRailSlabs(RulesTable rulesTable, RailSlabsTable railSlabsTable, CoordinateTable coordinateTable)
        {

            Alignment alignment = m_Watcher.CoreAlignment;
            if (rulesTable.CheckEmptySlabs() == false)
            {
                MessageBox.Show(String.Format("Пустые имена блока в таблице правил подобъекта {0}. Заполните имена блоков.", alignment.GetName()));
                return;
            }
            if (alignment == null)
            {
                return;
            }
            double staOffset = -1;



            for (int ruleIndx = 0; ruleIndx < rulesTable.Count; ruleIndx++)
            {
                RulesTableItem rule = (RulesTableItem)rulesTable[ruleIndx];
                var cadView = CadView;
                var layer = DrawingLayer.GetDrawingLayer(cadView);
                var drawing = layer.Drawing;
                var block = drawing.Blocks[rule.slab];
                if (block == null)
                {
                    MessageBox.Show(String.Format("Блок {0} не найден в ситуации плана", rule.slab));
                    return;
                }

                var blockDeltaLeft = block.Bounds.Left;


                int numInserts = 0;

                if (staOffset == -1)
                {
                    staOffset = rule.stationStart;
                }

                if (ruleIndx > 0)
                {
                    RulesTableItem lastRule = (RulesTableItem)rulesTable[ruleIndx - 1];
                    if (lastRule.stationEnd != rule.stationStart)
                    {
                        staOffset = rule.stationStart;
                    }
                }



                while (staOffset + block.Bounds.Width + rule.defaultGap < rule.stationEnd)
                {
                    Vector2D pos;
                    alignment.Plan.CompoundLine.StaOffsetToPos(staOffset - blockDeltaLeft, 0, out pos);
                    var blockLen = block.Bounds.Width;
                    List<double> intersectResultsRight = new List<double>();
                    List<double> intersectResultsLeft = new List<double>();
                    alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width + blockDeltaLeft, 270, 90, intersectResultsRight, 0);
                    alignment.Plan.CompoundLine.SectArc(pos, block.Bounds.Width + blockDeltaLeft, 90, 270, intersectResultsLeft, 0);
                    List<double> intersections = intersectResultsRight.Concat(intersectResultsLeft).ToList();
                    if (intersections.Count > 0)
                    {
                        var posSta = intersections.Max();
                        Vector2D posFinal;
                        alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posFinal);
                        var v2 = posFinal - pos;

                        double yt;
                        double gt;

                        //Получение высотной отментки и уклона, первый аргумент - индекс станции (пикетта в alignment.Stationing
                        alignment.Transitions[0].RedProfile.SplineProfile.GetYG(staOffset, out yt, out gt);
                        Vector3D pos3D = new Vector3D(pos.X, pos.Y, yt);

                        Vector2D posEnd; 

                        alignment.Plan.CompoundLine.StaOffsetToPos(posSta, 0, out posEnd);
                        alignment.Transitions[0].RedProfile.SplineProfile.GetYG(staOffset + blockLen, out yt, out gt);
                        Vector3D posEnd3D = new Vector3D(posEnd.X, posEnd.Y, yt);


                        var railSlabItem = new RailSlabsTableItem() { stationStart = staOffset,
                            angle = v2.Angle,
                            location = pos3D, slab = rule.slab,
                            locationEnd = posEnd3D,
                            slabProfile = rule.slabProfile, 
                            stationEnd = staOffset + blockLen };

                        var rec = PointsFromInsertPointsAngle(block, pos, v2.Angle);
                        var coordinateTableItem = new CoordinateTableItem() { slab = block.Name, startStation = railSlabItem.stationStart, lowerLeft = rec.lowerLeft, upperLeft = rec.upperLeft, lowerRight = rec.lowerRight, upperRight = rec.upperRight };
                        coordinateTable.Add(coordinateTableItem);

                        railSlabsTable.Add(railSlabItem);
                        staOffset += rule.defaultGap + blockLen;
                        numInserts += 1;
                    }
                    else
                    {
                        break;
                    }
                }

                if (ruleIndx < rulesTable.Count - 1)
                {
                    RulesTableItem nextRule = (RulesTableItem)rulesTable[ruleIndx + 1];
                    if (rule.stationEnd == nextRule.stationStart)
                    {
                        rule.stationEnd = staOffset;
                        rulesTable[ruleIndx] = rule;
                        nextRule.stationStart = staOffset;
                        rulesTable[ruleIndx + 1] = nextRule;
                    }
                }

                object value;
            }
        }

        private void actFillRulesWithCurves_Execute(object sender, ExecuteEventArgs e)
        {
            Alignment alignment = m_Watcher.CoreAlignment;
            if (alignment == null)
            {
                return;
            }
            object value;
            List<PlanPart> curves = PlanConverter.AlignmentToRules(alignment);
            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                var plugin = (RulesTablePlugin)value;
                plugin.RulesTable = new RulesTable(alignment);



                foreach (PlanPart curve in curves)
                {
                    plugin.RulesTable.Add(new RulesTableItem() { stationStart = curve.StartStation, stationEnd = curve.EndStation });
                }
            }

        }
        private void actDrawRailSlab_Execute(object sender, ExecuteEventArgs e)
        {
            Alignment alignment = m_Watcher.CoreAlignment;
            if (alignment == null)
            {
                return;
            }

            Alignment subAlignment = null;
            List<Alignment> alignments = AlignmentWatcher.GetAlignments(p => p != alignment).ToList();
            if (alignments.Count != 0)
            {
                subAlignment = AlignmentWatcher.GetAlignments(p => p != alignment).First();
            }

            object value;
            var cadView = CadView;
            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;
            //Получаем плагин
            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                //Редактируем таблицу
                var plugin = (RulesTablePlugin)value;
                var railSlabsTable = plugin.RailsSlabsTable;

                DwgLayer railLayer;
                if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Рельсовые плиты") == false)
                {
                    railLayer = drawing.Layers.Add("МодульРаскладкиПлит_Рельсовые плиты");
                    drawing.Layers.ActivateLayer(railLayer.Name);
                }
                else
                {
                    railLayer = drawing.Layers["МодульРаскладкиПлит_Рельсовые плиты"];
                    drawing.Layers.ActivateLayer(railLayer.Name);
                }

                railLayer.EraseEntities();

                drawing.BeginUpdate();
                try
                {
                    foreach (RailSlabsTableItem item in railSlabsTable)
                    {
                        var blockSlab = drawing.Blocks[item.slab];
                        drawing.ActiveSpace.AddInsert(item.location, new Vector3D(1.0), item.angle, blockSlab.Name);

                    }
                }
                finally
                {
                    cadView.Unlock();
                    cadView.Invalidate();
                    drawing.EndUpdate();
                }

                var foundationSlabTable = plugin.FoundationSlabTable;
                DrawFoundationSlabs(foundationSlabTable);

                if (subAlignment != null)
                {
                    ActivateAlignment(subAlignment);

                    cadView = CadView;
                    layer = DrawingLayer.GetDrawingLayer(cadView);
                    drawing = layer.Drawing;
                    //Получаем плагин
                    if (subAlignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                    {
                        //Редактируем таблицу
                        plugin = (RulesTablePlugin)value;
                        railSlabsTable = plugin.RailsSlabsTable;

                        if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Рельсовые плиты") == false)
                        {
                            railLayer = drawing.Layers.Add("МодульРаскладкиПлит_Рельсовые плиты");
                            drawing.Layers.ActivateLayer(railLayer.Name);
                        }
                        else
                        {
                            railLayer = drawing.Layers["МодульРаскладкиПлит_Рельсовые плиты"];
                            drawing.Layers.ActivateLayer(railLayer.Name);
                        }

                        railLayer.EraseEntities();

                        drawing.BeginUpdate();
                        try
                        {
                            foreach (RailSlabsTableItem item in railSlabsTable)
                            {
                                var blockSlab = drawing.Blocks[item.slab];
                                drawing.ActiveSpace.AddInsert(item.location, new Vector3D(1.0), item.angle, blockSlab.Name);

                            }
                        }
                        finally
                        {
                            cadView.Unlock();
                            cadView.Invalidate();
                            drawing.EndUpdate();
                        }

                        foundationSlabTable = plugin.FoundationSlabTable;
                        DrawFoundationSlabs(foundationSlabTable);
                    }

                    ActivateAlignment(alignment);
                }


            }
        }

        public static List<double> GenKM(double start, double end)
        {
            List<double> kms = new List<double>();
            start = start % 1000 == 0 ? start : start - start % 1000;

            for (double i = start; i < end; i += 1000)
            {
                kms.Add(i);
            }

            return kms;
        }

        private string ImportDxf()
        {
            var projectDirectory = Path.GetDirectoryName(ApplicationHost.Current.ActiveProject.TargetProjectFile);
            const string settingsFileName = "DefaultSlabFilePath.txt";
            string settingsFilePath = Path.Combine(projectDirectory, settingsFileName);
            string filePath;

            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Filter = "dxf файлы (*.dxf)|*.dxf";
                openFileDialog.FilterIndex = 2;
                openFileDialog.RestoreDirectory = true;

                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    //Get the path of specified file
                    filePath = openFileDialog.FileName;

                    using (StreamWriter writer = new StreamWriter(settingsFilePath))
                    {
                        writer.WriteLine(filePath);
                        return filePath;
                    }
                }
            }
            return "";
        }

        private void AlignmentNamesMessage(Alignment bottom_alignment, Alignment top_alignment)
        {
            MessageBox.Show(String.Format("Для отрисовки выбраны пути: правый (нижний) путь {0} и левый (верхний) путь {1}", bottom_alignment.GetName(), top_alignment.GetName()));
        }

        private void actDrawScheme_Execute(object sender, ExecuteEventArgs e)
        {
            var slabPath = SlabsConfig.Instance.ReadDefaultSlabsFile();

            var action = ApplicationHost.Current.AddIns["ID_TABLES_SINGLE_DRAWING"] as CallAction;
            if (action != null)
            {
                using (var drawing = new Drawing())
                {

                    ImportDefaultDxfToDrawing(drawing);

                    Alignment alignment_top = null;
                    Alignment alignment_bottom = null;
                    double minPK = 0;
                    double maxPK = 0;

                    List<Alignment> alignments = AlignmentWatcher.GetAlignments(p => p is Alignment).ToList();

                    double wayGap = Settings.Setting<double>("WAY_GAP");
                    double bottomHeight = Settings.Setting<double>("SCHEMA1_BOTTOM_HEIGHT");
                    double topHeight = bottomHeight + wayGap;

                    if (alignments.Count >= 2)
                    {
                        //Topomatic.Cad.View.Hints.CadCursors.GetDouble(CadView, ref wayGap, "Введите значение междупутья");
                        alignment_bottom = m_Watcher.CoreAlignment;

                        alignment_top = AlignmentWatcher.GetAlignments(p => p != alignment_bottom).First();
                        AlignmentNamesMessage(alignment_bottom, alignment_top);
                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0) { return; }

                        object value2;
                        alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value2);
                        var plugin2 = (RulesTablePlugin)value2;
                        var railSlabsTable2 = plugin2.RailsSlabsTable;
                        if (railSlabsTable2.Count == 0) { return; }
                        var minPK1 = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        var minPK2 = railSlabsTable2.Min(p => (p as RailSlabsTableItem).stationStart);
                        var maxPK1 = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                        var maxPK2 = railSlabsTable2.Max(p => (p as RailSlabsTableItem).stationEnd);

                        minPK = minPK1 < minPK2 ? minPK1 : minPK2;
                        maxPK = maxPK1 > maxPK2 ? maxPK1 : maxPK2;

                    }
                    else
                    {

                        alignment_bottom = m_Watcher.CoreAlignment;
                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0)
                        {
                            return;
                        }
                        minPK = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        maxPK = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                    }

                    object value;

                    //Получаем плагин
                    DwgLayer railLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Рельсовые плиты") == false)
                    {
                        railLayer = drawing.Layers.Add("МодульРаскладкиПлит_Рельсовые плиты");
                    }
                    else
                    {
                        railLayer = drawing.Layers["МодульРаскладкиПлит_Рельсовые плиты"];
                    }

                    DwgLayer railProfileLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Профиль рельсовые плиты") == false)
                    {
                        railProfileLayer = drawing.Layers.Add("МодульРаскладкиПлит_Профиль рельсовые плиты");
                    }
                    else
                    {
                        railProfileLayer = drawing.Layers["МодульРаскладкиПлит_Профиль рельсовые плиты"];
                    }

                    DwgLayer foundationLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Фундаментные плиты") == false)
                    {
                        foundationLayer = drawing.Layers.Add("МодульРаскладкиПлит_Фундаментные плиты");
                    }
                    else
                    {
                        foundationLayer = drawing.Layers["МодульРаскладкиПлит_Фундаментные плиты"];
                    }

                    DwgLayer foundationProfileLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Профиль фундаментные плиты") == false)
                    {
                        foundationProfileLayer = drawing.Layers.Add("МодульРаскладкиПлит_Профиль фундаментные плиты");
                    }
                    else
                    {
                        foundationProfileLayer = drawing.Layers["МодульРаскладкиПлит_Профиль фундаментные плиты"];
                    }

                    DwgLayer podlozhkaLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Профиль промежуточного слоя") == false)
                    {
                        podlozhkaLayer = drawing.Layers.Add("МодульРаскладкиПлит_Профиль промежуточного слоя");
                    }
                    else
                    {
                        podlozhkaLayer = drawing.Layers["МодульРаскладкиПлит_Профиль промежуточного слоя"];
                    }


                    DwgLayer osnovaniaLayer;
                    if (drawing.Layers.Names.ContainsKey("МодульРаскладкиПлит_Профиль основания") == false)
                    {
                        osnovaniaLayer = drawing.Layers.Add("МодульРаскладкиПлит_Профиль основания");
                    }
                    else
                    {
                        osnovaniaLayer = drawing.Layers["МодульРаскладкиПлит_Профиль основания"];
                    }



                    if (alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                    {
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)value;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;
                        Drawers.RulesDrawer.DrawConcreteLayer(plugin.RulesTable, railSlabsTable, drawing, bottomHeight, topHeight);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, bottomHeight, alignment_bottom.GetName());
                        Drawers.RailsTableDrawer.DrawSlabsScheme(railSlabsTable, drawing, bottomHeight);
                        Drawers.RailsTableDrawer.DrawSlabProfileScheme(railSlabsTable, drawing, 3);
                        Drawers.RailsTableDrawer.DrawRailWidthLabels(railSlabsTable, drawing, bottomHeight, false);

                        Drawers.FoundationTableDrawer.DrawFoundationSlabScheme_Striped(foundationSlabTable, drawing, bottomHeight);
                        Drawers.FoundationTableDrawer.DrawFoundationSlabWaterFillmentScheme(foundationSlabTable, railSlabsTable, drawing, bottomHeight, false);
                        Drawers.FoundationTableDrawer.DrawFoundationSlabLabelsScheme(foundationSlabTable, drawing, bottomHeight, false);
                        Drawers.RailsTableDrawer.DrawPodlozhka83(railSlabsTable, drawing, bottomHeight);
                        Drawers.RulesDrawer.DrawPicketage(plugin.RulesTable, drawing);


                        RailSlabsTableItem tslab = (railSlabsTable.First() as RailSlabsTableItem);
                        var slabBottom = drawing.Blocks[tslab.slabProfile].Bounds.Bottom;

                        Drawers.FoundationTableDrawer.DrawFoundationSlabProfileScheme(foundationSlabTable, drawing, 3 + slabBottom - 0.0415 * 2 - 0.150);

                        drawing.BeginUpdate();
                        try
                        {
                            // В отдельную функцию FlattenAlignment, которая на вход принмает пикетаж и рисует его в плоском виде.

                            RailSlabsTableItem tslab2 = (railSlabsTable.First() as RailSlabsTableItem);
                            var slabBottom2 = drawing.Blocks[tslab2.slabProfile].Bounds.Bottom;

                            var blockHalfWidth = 1.25;

                            for (int ruleIndx = 0; ruleIndx < plugin.RulesTable.Count; ruleIndx++)
                            {


                                RulesTableItem rule = (RulesTableItem)plugin.RulesTable[ruleIndx];
                                double start = -1;
                                double end = -1;
                                foreach (RailSlabsTableItem rail in plugin.RailsSlabsTable)
                                {
                                    if (rail.stationStart >= rule.stationStart && rail.stationStart < rule.stationEnd)
                                    {
                                        if (start == -1)
                                        {
                                            start = rail.stationStart;
                                        }

                                        if (rail.stationEnd > end)
                                        {
                                            end = rail.stationEnd + rule.defaultGap;
                                        }
                                    }
                                }


                                double calculatedRuleWidth = end - start;


                                var railBlock = drawing.Blocks[rule.slab];

                                var firstFundProfName = "firstFundProfName" + ((int)rule.stationStart).ToString() + (int)calculatedRuleWidth;
                                var firstFundProf = drawing.Blocks[firstFundProfName];
                                if (firstFundProf == null)
                                {
                                    firstFundProf = drawing.Blocks.Add(firstFundProfName);
                                    var fline = firstFundProf.AddPolyline(new Vector2D[] { new Vector2D(0.0, -0.060), new Vector2D(0.0, 0.060),
                                new Vector2D(calculatedRuleWidth, 0.060), new Vector2D(calculatedRuleWidth, -0.060) });
                                    fline.Color = new CadColor(System.Drawing.Color.SandyBrown);
                                    fline.Closed = true;
                                    Vector2D[] points3 = new Vector2D[] { new Vector2D(0.0, -0.060), new Vector2D(0.0, 0.060),
                                new Vector2D(calculatedRuleWidth, 0.060), new Vector2D(calculatedRuleWidth, -0.060) };
                                    DwgHatch hatch3 = firstFundProf.AddHatch(0, "ANSI31", points3);
                                    hatch3.Color = new CadColor(System.Drawing.Color.SandyBrown);
                                }

                                var secondFundProfName = "secondFundProfName" + ((int)rule.stationStart).ToString() + (int)calculatedRuleWidth;
                                var secondFundProf = drawing.Blocks[secondFundProfName];
                                if (secondFundProf == null)
                                {
                                    secondFundProf = drawing.Blocks.Add(secondFundProfName);
                                    var fline = secondFundProf.AddPolyline(new Vector2D[] { new Vector2D(0.0, -0.075), new Vector2D(0.0, 0.075),
                                new Vector2D(calculatedRuleWidth, 0.075), new Vector2D(calculatedRuleWidth, -0.075) });
                                    fline.Color = new CadColor(System.Drawing.Color.SandyBrown);
                                    fline.Closed = true;
                                    Vector2D[] points4 = new Vector2D[] { new Vector2D(0.0, -0.075), new Vector2D(0.0, 0.075),
                                new Vector2D(calculatedRuleWidth, 0.075), new Vector2D(calculatedRuleWidth, -0.075) };
                                    DwgHatch hatch4 = secondFundProf.AddHatch(0, "AR-CONC", points4);
                                    hatch4.Color = new CadColor(System.Drawing.Color.SandyBrown);
                                    hatch4.PatternScale = 0.05;
                                }
                                drawing.Layers.ActivateLayer(osnovaniaLayer.Name);
                                drawing.ActiveSpace.AddInsert(new Vector2D(start, 3 + slabBottom2 - 0.0415 * 2 - 0.1505 * 2 - 0.060), new Vector3D(1.0), 0.0, firstFundProfName);
                                drawing.ActiveSpace.AddInsert(new Vector2D(start, 3 + slabBottom2 - 0.0415 * 2 - 0.1505 * 2 - 0.060 * 2 - 0.075), new Vector3D(1.0), 0.0, secondFundProfName);


                            }
                        }
                        finally
                        {
                            drawing.EndUpdate();
                        }
                    }


                    object topvalue;

                    if (alignment_top != null && alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out topvalue))
                    {
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)topvalue;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, topHeight, alignment_top.GetName());
                        Drawers.RailsTableDrawer.DrawProjectSlabsScheme(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight);
                        Drawers.RailsTableDrawer.DrawProjectRailWidthLabels(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight, true);

                        Drawers.FoundationTableDrawer.DrawProjectFoundationSlabScheme_Striped(alignment_bottom, alignment_top, foundationSlabTable, drawing, topHeight);
                        Drawers.FoundationTableDrawer.DrawProjectFoundationSlabWaterFillmentScheme(alignment_bottom, alignment_top, railSlabsTable, foundationSlabTable, drawing, topHeight);
                        Drawers.FoundationTableDrawer.DrawProjectFoundationSlabLabelsScheme(alignment_bottom, alignment_top, foundationSlabTable, drawing, topHeight, true);
                    }

                    action.PerformExecute(new ExecuteEventArgs(drawing, "Чертеж 1", "DXF"));
                }
            }
        }

        public struct StationBorder
        {
            public StationBorder(double start, double end)
            {
                stationStart = start;
                stationEnd = end;
            }
            public double stationStart;
            public double stationEnd;
        }

        private void actDrawSchemeFive_Execute(object sender, ExecuteEventArgs e)
        {



            var action = ApplicationHost.Current.AddIns["ID_TABLES_SINGLE_DRAWING"] as CallAction;
            if (action != null)
            {
                using (var drawing = new Drawing())
                {
                    var isocpeur_style = drawing.Styles.FindEqualityOrCreate(Settings.Setting<string>("FONT_STYLE"));
                    ImportDefaultDxfToDrawing(drawing);



                    var layerName = "МодульРаскладкиПлит_Схема 5";


                    DwgLayer schemeLayer;
                    if (drawing.Layers.Names.ContainsKey(layerName) == false)
                    {
                        schemeLayer = drawing.Layers.Add(layerName);
                    }
                    else
                    {
                        schemeLayer = drawing.Layers[layerName];
                    }

                    const double topHeight = 5;
                    const double bottomHeight = -5;
                    const double pkTextSize = 2;
                    const double textSize = 2;
                    const double firstRowTextHeight = 3;
                    const double secondRowTextHeight = 8;
                    const double thirdRowTextHeight = 12;
                    const double pkTextSideGap = 0;
                    Alignment alignment_top = null;
                    Alignment alignment_bottom = null;
                    double minPK = 0;
                    double maxPK = 0;
                    List<Alignment> alignments = AlignmentWatcher.GetAlignments(p => p is Alignment).ToList();
                    double wayGap = 5;

                    double? borderStationStart = null;
                    double? borderStationEnd = null;
                    if (alignments.Count >= 2)
                    {
                        //Topomatic.Cad.View.Hints.CadCursors.GetDouble(CadView, ref wayGap, "Введите значение междупутья");
                        alignment_bottom = m_Watcher.CoreAlignment;

                        alignment_top = AlignmentWatcher.GetAlignments(p => p != alignment_bottom).First();
                        AlignmentNamesMessage(alignment_bottom, alignment_top);

                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0) { return; }

                        object value2;
                        alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value2);
                        var plugin2 = (RulesTablePlugin)value2;
                        var railSlabsTable2 = plugin2.RailsSlabsTable;
                        if (railSlabsTable2.Count == 0) { return; }
                        var minPK1 = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        var minPK2 = railSlabsTable2.Min(p => (p as RailSlabsTableItem).stationStart);
                        var maxPK1 = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                        var maxPK2 = railSlabsTable2.Max(p => (p as RailSlabsTableItem).stationEnd);

                        minPK = minPK1 < minPK2 ? minPK1 : minPK2;
                        maxPK = maxPK1 > maxPK2 ? maxPK1 : maxPK2;

                    }
                    else
                    {

                        alignment_bottom = m_Watcher.CoreAlignment;
                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0)
                        {
                            return;
                        }
                        minPK = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        maxPK = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                    }
                    object value_bot;

                    int lastInsertIndx;
                    double lastEndStation;

                    if (alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value_bot))
                    {
                        lastInsertIndx = 0;
                        lastEndStation = 0;
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)value_bot;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;
                        var rulesTable = plugin.RulesTable;
                        List<StationBorder> borders = new List<StationBorder>();
                        var startLine = railSlabsTable.Min(p =>
                        {
                            var a = p as RailSlabsTableItem;
                            return a.stationStart;
                        });

                        var endLine = railSlabsTable.Max(p =>
                        {
                            var a = p as RailSlabsTableItem;
                            return a.stationEnd;
                        });

                        drawing.BeginUpdate();
                        try
                        {
                            var block = drawing.Blocks["правый путь"];

                            if (block == null)
                            {
                                block = drawing.Blocks.Add("правый путь");
                                var bline1 = block.AddPolyline(new Vector2D[] { new Vector2D(startLine, 0), new Vector2D(endLine, 0) });
                                bline1.Color = CadColor.Red;
                                var bline6 = block.AddPolyline(new Vector2D[] { new Vector2D(startLine, -firstRowTextHeight - textSize),
                            new Vector2D(endLine, -firstRowTextHeight - textSize) });
                            }

                            if (rulesTable.Count == 0) { return; }
                            foreach (RulesTableItem rule in rulesTable)
                            {
                                var slabs = railSlabsTable.Where(p =>
                                {
                                    RailSlabsTableItem a = p as RailSlabsTableItem;
                                    return a.stationStart >= rule.stationStart && a.stationStart < rule.stationEnd;
                                });
                                if (slabs.ToList().Count == 0) { return; }
                                var startPK = slabs.Min(p =>
                                {
                                    var a = p as RailSlabsTableItem;
                                    return a.stationStart;
                                });
                                var endPK = slabs.Max(p =>
                                {
                                    var a = p as RailSlabsTableItem;
                                    return a.stationEnd;
                                });



                                int firstSlabIndx = railSlabsTable.IndexOf(slabs.First() as RailSlabsTableItem);

                                if (firstSlabIndx > 0)
                                {
                                    if ((railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > lastEndStation &&
                                        (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > rule.stationStart)
                                    {
                                        startPK = (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationStart;
                                    }
                                }




                                if (borderStationStart.HasValue == false || startPK < borderStationStart)
                                {
                                    borderStationStart = startPK;
                                }

                                if (borderStationEnd.HasValue == false || endPK > borderStationEnd)
                                {
                                    borderStationEnd = endPK;
                                }


                                StationBorder border = new StationBorder(startPK, endPK);
                                if (borders.Count > 0)
                                {
                                    int lastIndx = borders.Count - 1;
                                    if (borders[lastIndx].stationEnd > border.stationStart || border.stationStart - borders[lastIndx].stationEnd < 0.3)
                                    {
                                        borders[lastIndx] = new StationBorder(borders[lastIndx].stationStart, border.stationStart);
                                    }
                                }

                                borders.Add(border);

                                var count = slabs.Count();
                                var slabName = rule.slab;

                                var blockname = "Bot5" + (int)(Math.Round(startPK, 3) * 1000) + slabName + count;



                                var tx4 = block.AddText(rule.slab + " - " + count + " шт.",
                                    new Vector2D(startPK + (endPK - startPK) / 2, -secondRowTextHeight), textSize, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                                tx4.Style = isocpeur_style;
                                var tx5 = block.AddText((endPK - startPK).ToString("#.##"),
                                    new Vector2D(startPK + (endPK - startPK) / 2, -firstRowTextHeight), textSize, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                                tx5.Style = isocpeur_style;
                            }

                            foreach (StationBorder border in borders)
                            {
                                var bline2 = block.AddPolyline(new Vector2D[] { new Vector2D(border.stationStart, 0), new Vector2D(border.stationStart, -firstRowTextHeight - textSize) });
                                int st;
                                double plus;
                                char? indx;
                                alignment_bottom.Stationing.StationToPk(border.stationStart, out st, out indx, out plus);
                                var tx2 = block.AddText("ПК " + st + "+" + plus.ToString("#.##"),
                                    new Vector2D(border.stationStart + pkTextSideGap, -secondRowTextHeight), pkTextSize, 1.0, 1.57, 0.0,
                                    Topomatic.Dwg.TextAlignment.MiddleRight);
                                tx2.Style = isocpeur_style;
                                var bline3 = block.AddPolyline(new Vector2D[] { new Vector2D(border.stationEnd, 0),
                            new Vector2D(border.stationEnd, -firstRowTextHeight - textSize) });

                                alignment_bottom.Stationing.StationToPk(border.stationEnd, out st, out indx, out plus);
                                var tx3 = block.AddText("ПК " + st + "+" + plus.ToString("#.##"),
                                    new Vector2D(border.stationEnd - pkTextSideGap, -secondRowTextHeight), pkTextSize, 1.0, 1.57, 0.0,
                                    Topomatic.Dwg.TextAlignment.MiddleRight);
                                tx3.Style = isocpeur_style;
                            }

                            drawing.ActiveSpace.AddInsert(new Vector2D(0, bottomHeight), new Vector3D(1.0), 0.0, "правый путь");
                        }
                        finally
                        {
                            CadView.Unlock();
                            CadView.Invalidate();
                            drawing.EndUpdate();
                        }
                    }

                    object value_top;
                    if (alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value_top))
                    {
                        lastInsertIndx = 0;
                        lastEndStation = 0;
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)value_top;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;
                        var rulesTable = plugin.RulesTable;
                        List<StationBorder> borders = new List<StationBorder>();

                        drawing.Layers.ActivateLayer(schemeLayer.Name);

                        var block = drawing.Blocks["левый путь"];

                        var startLine = railSlabsTable.Min(p =>
                        {
                            var a = p as RailSlabsTableItem;
                            return a.stationStart;
                        });

                        var endLine = railSlabsTable.Max(p =>
                        {
                            var a = p as RailSlabsTableItem;
                            return a.stationEnd;
                        });

                        if (block == null)
                        {
                            block = drawing.Blocks.Add("левый путь");
                            var bline1 = block.AddPolyline(new Vector2D[] { new Vector2D(startLine, 0), new Vector2D(endLine, 0) });
                            bline1.Color = CadColor.Red;

                            var bline6 = block.AddPolyline(new Vector2D[] { new Vector2D(startLine, firstRowTextHeight +textSize),
                            new Vector2D(endLine, firstRowTextHeight + textSize) });
                        }

                        foreach (RulesTableItem rule in rulesTable)
                        {
                            var slabs = railSlabsTable.Where(p =>
                            {
                                RailSlabsTableItem a = p as RailSlabsTableItem;
                                return a.stationStart >= rule.stationStart && a.stationStart < rule.stationEnd;
                            });


                            if (slabs.Count() == 0) {
                                continue;
                            }

                            var startPK = slabs.Min(p =>
                            {
                                var a = p as RailSlabsTableItem;
                                return a.stationStart;
                            });

                            int firstSlabIndx = railSlabsTable.IndexOf(slabs.First() as RailSlabsTableItem);

                            if (firstSlabIndx > 0)
                            {
                                if ((railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > lastEndStation &&
                                    (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationEnd > rule.stationStart)
                                {
                                    startPK = (railSlabsTable[firstSlabIndx - 1] as RailSlabsTableItem).stationStart;
                                }
                            }


                            var endPK = slabs.Max(p =>
                            {
                                var a = p as RailSlabsTableItem;
                                return a.stationEnd;
                            });



                            StationBorder border = new StationBorder(startPK, endPK);
                            if (borders.Count > 0)
                            {
                                int lastIndx = borders.Count - 1;
                                if (borders[lastIndx].stationEnd > border.stationStart || border.stationStart - borders[lastIndx].stationEnd < 0.3)
                                {
                                    borders[lastIndx] = new StationBorder(borders[lastIndx].stationStart, border.stationStart);
                                }
                            }
                            borders.Add(border);


                            var count = slabs.Count();
                            var slabName = rule.slab;


                            if (borderStationStart.HasValue == false || startPK < borderStationStart)
                            {
                                borderStationStart = startPK;
                            }

                            if (borderStationEnd.HasValue == false || endPK > borderStationEnd)
                            {
                                borderStationEnd = endPK;
                            }

                            drawing.BeginUpdate();
                            try
                            {
                                var tx4 = block.AddText(rule.slab + " - " + count + " шт.",
                                    new Vector2D(startPK + (endPK - startPK) / 2, secondRowTextHeight), textSize, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);
                                tx4.Style = isocpeur_style;
                                var tx5 = block.AddText((endPK - startPK).ToString("#.##"),
                                    new Vector2D(startPK + (endPK - startPK) / 2, firstRowTextHeight), textSize, 1.0, 0.0, 0.0, Topomatic.Dwg.TextAlignment.MiddleCenter);

                                tx5.Style = isocpeur_style;


                            }
                            finally
                            {
                                CadView.Unlock();
                                CadView.Invalidate();
                                drawing.EndUpdate();
                            }
                        }


                        foreach (StationBorder border in borders)
                        {
                            var bline2 = block.AddPolyline(new Vector2D[] { new Vector2D(border.stationStart, 0), new Vector2D(border.stationStart,
                        firstRowTextHeight + textSize) });
                            int st;
                            double plus;
                            char? indx;
                            alignment_top.Stationing.StationToPk(border.stationStart, out st, out indx, out plus);
                            var tx2 = block.AddText("ПК " + st + "+" + plus.ToString("#.##"),
                                new Vector2D(border.stationStart + pkTextSideGap, secondRowTextHeight), pkTextSize, 1.0, 1.57, 0.0, Topomatic.Dwg.TextAlignment.MiddleLeft);
                            tx2.Style = isocpeur_style;
                            var bline3 = block.AddPolyline(new Vector2D[] { new Vector2D(border.stationEnd, 0), new Vector2D(border.stationEnd,
                        firstRowTextHeight + textSize) });
                            alignment_top.Stationing.StationToPk(border.stationEnd, out st, out indx, out plus);
                            var tx3 = block.AddText("ПК " + st + "+" + plus.ToString("#.##"),
                                new Vector2D(border.stationEnd - pkTextSideGap, secondRowTextHeight), pkTextSize, 1.0, 1.57, 0.0, Topomatic.Dwg.TextAlignment.MiddleLeft);
                            tx3.Style = isocpeur_style;
                        }

                        drawing.ActiveSpace.AddInsert(new Vector2D(0, 5), new Vector3D(1.0), 0.0, "левый путь");

                    }




                    const string borderBlockName = "Границы";
                    var blockBorder = drawing.Blocks[borderBlockName];

                    if (blockBorder == null)
                    {
                        blockBorder = drawing.Blocks.Add(borderBlockName);
                        if (borderStationStart.HasValue && borderStationEnd.HasValue)
                        {
                            blockBorder.AddPolyline(new Vector2D[] { new Vector2D(borderStationStart.Value, 0), new Vector2D(borderStationEnd.Value, 0) });
                            blockBorder.AddPolyline(new Vector2D[] { new Vector2D(borderStationStart.Value, 0), new Vector2D(borderStationEnd.Value, 0) });
                            blockBorder.AddPolyline(new Vector2D[] { new Vector2D(borderStationStart.Value, -5), new Vector2D(borderStationStart.Value, 5) });
                            blockBorder.AddPolyline(new Vector2D[] { new Vector2D(borderStationEnd.Value, -5), new Vector2D(borderStationEnd.Value, 5) });
                        }

                        drawing.ActiveSpace.AddInsert(new Vector2D(0, 0), new Vector3D(1.0), 0.0, borderBlockName);
                    }


                    action.PerformExecute(new ExecuteEventArgs(drawing, "Чертеж 5", "DXF"));
                }
            }
        }

        private void actDrawSchemeFour_Execute(object sender, ExecuteEventArgs e)
        {

            var gap_size_1 = Settings.Setting<double>("SCHEMA4_GAP_SIZE_1");
            var gap_size_2 = Settings.Setting<double>("SCHEMA4_GAP_SIZE_2");
            var action = ApplicationHost.Current.AddIns["ID_TABLES_SINGLE_DRAWING"] as CallAction;
            if (action != null)
            {
                using (var drawing = new Drawing())
                {

                    ImportDefaultDxfToDrawing(drawing);
                    Alignment alignment_top = null;
                    Alignment alignment_bottom = null;
                    double minPK = 0;
                    double maxPK = 0;

                    List<Alignment> alignments = AlignmentWatcher.GetAlignments(p => p is Alignment).ToList();
                    double wayGap = Settings.Setting<double>("WAY_GAP");
                    if (alignments.Count >= 2)
                    {
                        //Topomatic.Cad.View.Hints.CadCursors.GetDouble(CadView, ref wayGap, "Введите значение междупутья");
                        alignment_bottom = m_Watcher.CoreAlignment;

                        alignment_top = AlignmentWatcher.GetAlignments(p => p != alignment_bottom).First();
                        AlignmentNamesMessage(alignment_bottom, alignment_top);
                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0) { return; }

                        object value2;
                        alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value2);
                        var plugin2 = (RulesTablePlugin)value2;
                        var railSlabsTable2 = plugin2.RailsSlabsTable;
                        if (railSlabsTable2.Count == 0) { return; }
                        var minPK1 = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        var minPK2 = railSlabsTable2.Min(p => (p as RailSlabsTableItem).stationStart);
                        var maxPK1 = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                        var maxPK2 = railSlabsTable2.Max(p => (p as RailSlabsTableItem).stationEnd);

                        minPK = minPK1 < minPK2 ? minPK1 : minPK2;
                        maxPK = maxPK1 > maxPK2 ? maxPK1 : maxPK2;

                    }
                    else
                    {

                        alignment_bottom = m_Watcher.CoreAlignment;
                        if (alignment_bottom == null)
                        {
                            return;
                        }
                        object value1;
                        alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value1);
                        var plugin1 = (RulesTablePlugin)value1;
                        var railSlabsTable1 = plugin1.RailsSlabsTable;
                        if (railSlabsTable1.Count == 0)
                        {
                            return;
                        }
                        minPK = railSlabsTable1.Min(p => (p as RailSlabsTableItem).stationStart);
                        maxPK = railSlabsTable1.Max(p => (p as RailSlabsTableItem).stationEnd);
                    }


                    var bottomHeight = Settings.Setting<double>("SCHEMA4_BOTTOM_HEIGHT");
                    var topHeight = bottomHeight + wayGap;

                    object value;
                    if (alignment_bottom.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                    {
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)value;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;


                        Drawers.RulesDrawer.DrawConcreteLayer(plugin.RulesTable, railSlabsTable, drawing, bottomHeight, topHeight);
                        Drawers.RailsTableDrawer.DrawSlabsScheme(railSlabsTable, drawing, bottomHeight);
                        Drawers.FoundationTableDrawer.DrawFoundationSlabScheme_Striped(foundationSlabTable, drawing, bottomHeight);
                        Drawers.RailsTableDrawer.DrawRailLabels(railSlabsTable, drawing, bottomHeight);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, bottomHeight, alignment_bottom.GetName());
                        Drawers.RulesDrawer.DrawWayDistanceLabel(wayGap * 1000, bottomHeight, topHeight, minPK, maxPK, drawing);

                        Drawers.RailsTableDrawer.DrawSlabsAsSimpleBlockScheme(railSlabsTable, drawing, bottomHeight - gap_size_1);
                        Drawers.RailsTableDrawer.DrawRailLabels(railSlabsTable, drawing, bottomHeight - gap_size_1);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, bottomHeight - gap_size_1, alignment_bottom.GetName());
                        Drawers.RulesDrawer.DrawWayDistanceLabel(wayGap * 1000, bottomHeight - gap_size_1, topHeight - gap_size_1, minPK, maxPK, drawing);


                        Drawers.FoundationTableDrawer.DrawFoundationSlabScheme(foundationSlabTable, drawing, bottomHeight - gap_size_2);
                        //Drawers.FoundationTableDrawer.DrawFoundationSlabWaterFillmentScheme(foundationSlabTable, railSlabsTable, drawing, bottomHeight - gap_size_2);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, bottomHeight - gap_size_2, alignment_bottom.GetName());
                        Drawers.RulesDrawer.DrawWayDistanceLabel(wayGap * 1000, bottomHeight - gap_size_2, topHeight - gap_size_2, minPK, maxPK, drawing);
                        Drawers.FoundationTableDrawer.DrawProjectAlternativeFoundationSlabLabelsScheme(alignment_bottom, alignment_bottom, foundationSlabTable, drawing, bottomHeight - gap_size_2, false);

                        Drawers.RulesDrawer.DrawRulesBorders(plugin.RulesTable, alignment_bottom, plugin.RailsSlabsTable, drawing, false, bottomHeight);

                    }

                    object topvalue;

                    if (alignment_top != null && alignment_top.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out topvalue))
                    {
                        //Редактируем таблицу
                        var plugin = (RulesTablePlugin)topvalue;
                        var railSlabsTable = plugin.RailsSlabsTable;
                        var foundationSlabTable = plugin.FoundationSlabTable;
                        Drawers.RailsTableDrawer.DrawProjectSlabsScheme(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight);
                        Drawers.RailsTableDrawer.DrawProjectRailLabels(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight, true);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, topHeight, alignment_top.GetName());
                        Drawers.FoundationTableDrawer.DrawProjectFoundationSlabScheme_Striped(alignment_bottom, alignment_top, foundationSlabTable, drawing, topHeight);

                        Drawers.RailsTableDrawer.DrawProjectSlabsAsSimpleBlockScheme(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight - gap_size_1);
                        Drawers.RailsTableDrawer.DrawProjectRailLabels(alignment_bottom, alignment_top, railSlabsTable, drawing, topHeight - gap_size_1, true);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, topHeight - gap_size_1, alignment_top.GetName());

                        Drawers.FoundationTableDrawer.DrawProjectFoundationSlabScheme(alignment_bottom, alignment_top, foundationSlabTable, drawing, topHeight - gap_size_2);
                        //Drawers.FoundationTableDrawer.DrawProjectFoundationSlabWaterFillmentScheme(alignment_bottom, alignment_top, railSlabsTable, foundationSlabTable, drawing, topHeight - gap_size_2);
                        Drawers.RulesDrawer.DrawWayAxis(plugin.RulesTable, drawing, topHeight - gap_size_2, alignment_top.GetName());
                        Drawers.FoundationTableDrawer.DrawProjectAlternativeFoundationSlabLabelsScheme(alignment_bottom, alignment_top, foundationSlabTable, drawing, topHeight - gap_size_2, true);


                        Drawers.RulesDrawer.DrawRulesBorders(plugin.RulesTable, alignment_top, plugin.RailsSlabsTable, drawing, true, topHeight);
                    }

                    action.PerformExecute(new ExecuteEventArgs(drawing, "Чертеж 4", "DXF"));
                }

            }
        }



        private CallAction actDrawSchemeFour;
        private CallAction actEditSlabRulesTable;
        private CallAction actEditRailSlabsTable;
        private CallAction actPopulateRailSlab;
        private CallAction actDrawRailSlab;
        private CallAction actDrawScheme;
        private CallAction actEditFoundationSlabsTable;
        private CallAction actEditCoordinateTable;
        private CallAction actFillRulesWithCurves;
        private CallAction actPopulateRailSlabFromMainAlignment;
        private CallAction actDrawSchemeFive;

        private CallAction actRecalculateRules;


        private CallAction actImportDefaultDxfToActiveAlignment;
        private CallAction actImportDefaultDxfToAllAlignments;

        private CallAction actSaveReports;

        private void actImportDefaultDxfToActiveAlignment_Execute(object sender, ExecuteEventArgs e)
        {
            SlabsConfig.Instance.ImportDxf();
            var cadView = CadView;
            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;
            ImportDefaultDxfToDrawing(drawing);


            string message = "В результате импорта блоков, имена блоков в таблице правил отрисовки могут оказаться некорректными. \n" +
                "Очистить имена блоков в таблице правил (записи в таблице будут сохранены)?";
            string caption = "Очистка таблицы правил";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons);
            if (result == DialogResult.Yes)
            {
                var alignment = m_Watcher.CoreAlignment;
                object value;
                if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                {
                    var plugin = (RulesTablePlugin)value;
                    var rules = plugin.RulesTable;
                    for (int i = 0; i < rules.Count; i++)
                    {
                        RulesTableItem item = (RulesTableItem)rules[i];
                        item.slab = null;
                        item.slabProfile = null;
                        rules[i] = item;
                    }
                    plugin.RailsSlabsTable = new RailSlabsTable(plugin);
                    plugin.FoundationSlabTable = new FoundationSlabTable(plugin);
                    plugin.CoordinateTable = new CoordinateTable(plugin);
                }
            }

            MessageBox.Show("Импорт завершен.");

        }

        private void actImportDefaultDxfToAllAlignments_Execute(object sender, ExecuteEventArgs e)
        {
            SlabsConfig.Instance.ImportDxf();
            Alignment coreAlignment = m_Watcher.CoreAlignment;
            var alignments = AlignmentWatcher.GetAlignments(p => p is Alignment);


            string message = "В результате импорта блоков, имена блоков в таблицах правил отрисовки могут оказаться некорректными. \n" +
                "Очистить имена блоков в таблице правил (записи в таблице будут сохранены)?";
            string caption = "Очистка таблицы правил";
            MessageBoxButtons buttons = MessageBoxButtons.YesNo;
            DialogResult result;
            result = MessageBox.Show(message, caption, buttons);
            bool clearTables = false;
            if (result == DialogResult.Yes)
            {
                var alignment = m_Watcher.CoreAlignment;
                object value;
                if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                {
                    clearTables = true;
                }
            }


            foreach (var alignment in alignments)
            {
                ActivateAlignment(alignment);
                var cadView = CadView;
                var layer = DrawingLayer.GetDrawingLayer(cadView);
                var drawing = layer.Drawing;
                ImportDefaultDxfToDrawing(drawing);
                if (clearTables == true)
                {
                    object value;
                    if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
                    {
                        var plugin = (RulesTablePlugin)value;
                        var rules = plugin.RulesTable;
                        for (int i = 0; i < rules.Count; i++)
                        {
                            RulesTableItem item = (RulesTableItem)rules[i];
                            item.slab = null;
                            item.slabProfile = null;
                            rules[i] = item;
                        }
                        plugin.RailsSlabsTable = new RailSlabsTable(plugin);
                        plugin.FoundationSlabTable = new FoundationSlabTable(plugin);
                        plugin.CoordinateTable = new CoordinateTable(plugin);
                    }
                }
            }

            ActivateAlignment(coreAlignment);

            MessageBox.Show("Импорт завершен.");
        }


        private void DeleteDefaultBlocks(Drawing drawing)
        {
            var entitiesToRemove = drawing.ActiveSpace.Where(el => el is DwgInsert).Where(el => (el as DwgInsert).Block.Name.StartsWith("NGP")).ToList();
            drawing.ActiveSpace.Entities.RemoveAll(el => entitiesToRemove.Contains(el));
            drawing.Blocks.RemoveAll(el => el.Name.StartsWith("NGP"));


        }

        private void ImportDefaultDxfToDrawing(Drawing drawing)
        {
            var dxfPath = SlabsConfig.Instance.DefaultSlabsPath;
            DeleteDefaultBlocks(drawing);
            var prevEntities = drawing.ActiveSpace.ToList();
            using (FileStream SourceStream = File.Open(dxfPath, FileMode.Open))
            {
                AcaxImporter.ImportDxf(drawing, SourceStream);
                var newEntities = drawing.ActiveSpace.Where(el => prevEntities.Contains(el) == false);
                drawing.ActiveSpace.Entities.RemoveAll(el => newEntities.Contains(el));

            }

        }

        public static double ProjectStationToAlignment(double station, Alignment originalAlignment, Alignment targetAlignment)
        {

            try
            {
                Vector2D pos;
                if (originalAlignment.Plan.CompoundLine.StaOffsetToPos(station, 0, out pos))
                {
                    double offset;
                    targetAlignment.Plan.CompoundLine.PosToStaOffset(pos, out station, out offset);

                }
            }
            catch
            {

            }

            return station;
        }

        const double GAP_ERR = 0.00000001;


        private double FindIdealGap(RulesTableItem rule, DwgBlock block)
        {
            double StandartGap = Settings.Setting<double>("DEFAULT_FOUNDATION_SLAB_GAP");
            double MaximumDelta = Settings.Setting<double>("DEFAULT_FOUNDATION_SLAB_GAP_DELTA");
            double MinGap = StandartGap - MaximumDelta;
            double MaxGap = StandartGap + MaximumDelta;
            //var slabCount = Math.Truncate((rule.stationEnd - rule.stationStart) / (block.Bounds.Width + rule.defaultGap));
            var remainder = rule.stationEnd - (rule.stationStart +
                (block.Bounds.Width + rule.defaultGap) * Math.Truncate((rule.stationEnd -
                rule.stationStart) / (block.Bounds.Width + rule.defaultGap)));

            var minSlabCount = Math.Truncate((rule.stationEnd -
                rule.stationStart) / (block.Bounds.Width + MaxGap));
            //Смотрим, может ли быть изменено максимальное количество плит при изменении зазора
            var maxSlabCount = Math.Truncate((rule.stationEnd -
                rule.stationStart) / (block.Bounds.Width + MinGap));

            //MinSlabCount не интересует. Если мы при увеличении зазора не может положить тоже количество плит, что и изначально, это означает что:
            //а) вероятно, мы не сможет без зазора положитьл минимальное количетсво плит (т.е. получается остаток равный длины плиты, которую мы не смогли уложить
            //б) есть такое значения зазор для большего количества плит, при котором они укладываются без остатка (т.е. границы плиты совпадает 
            //с концом участка и дальше его пересекает, из-за чего мы не может уложить плиту

            var maxSLabCountMinGapRemainder = rule.stationEnd - (rule.stationStart +
            (block.Bounds.Width + MinGap) * maxSlabCount);
            var maxSlabCountMaxGapRemainder = rule.stationEnd - (rule.stationStart +
                (block.Bounds.Width + MaxGap) * maxSlabCount);


            //maxSLabCountMinGapRemainder положительный, т.к. какой-то остаток остается.
            //если maxSlabCountMaxGapRemainder меньше нуля, значит мы можем дотянуть границу плиты до конца участка
            if (maxSlabCountMaxGapRemainder < 0)
            {
                var properGap = MaxGap - (0 - maxSlabCountMaxGapRemainder) / maxSlabCount;
                return properGap - GAP_ERR;
            } else
            {
                return 0;
            }

        }



        public struct Report
        {
            public string name;
            public string reportPath;
            public double section_length;
            public int slabCount;
            public int amount_expansion_joints;
            public int amount_fundaments_joint;
            public List<Topomatic.Stg.IStgSerializable> foundationSlabs;
            public List<Topomatic.Stg.IStgSerializable> railSlabs;
            public double V1;
            public double V2;
            public double V3;
            public double V4;
            public double V5;
            public double V6;
            public double V7;
            public double V8;
            public double V9;
            public double V10;
            public double V11;
            public double V12;
            public double V13;
            public double V14;
            public double V15;
            public double V16;
            public double V17;
            public double V18;
            public double V19;
            public double V20;
            public double V21;
            public double V22;
            public double V23;
            public double V24;
            public double V25;
            public double V26;
            public double V27;
            public double V28;
            public double V29;
            public double V30;
            public double V31;
            public double V32;
            public double V33;
        }
        

        private void WriteReport(StreamWriter writer, Report report)
        {


            var foundationSlabsDictionary = new Dictionary<double, List<FoundationSlabTableItem>>();
            foreach (FoundationSlabTableItem slab in report.foundationSlabs)
            {
                if (foundationSlabsDictionary.Keys.Contains(slab.width) == false)
                {
                    foundationSlabsDictionary[slab.width] = new List<FoundationSlabTableItem>();
                }
                foundationSlabsDictionary[slab.width].Add(slab);
            }

            var railSlabsDictionary = new Dictionary<string, List<RailSlabsTableItem>>();
            foreach (RailSlabsTableItem railSlab in report.railSlabs)
            {
                if (railSlabsDictionary.Keys.Contains(railSlab.slab) == false)
                {
                    railSlabsDictionary[railSlab.slab] = new List<RailSlabsTableItem>();
                }
                railSlabsDictionary[railSlab.slab].Add(railSlab);
            }


            writer.WriteLine(String.Format("Протяженность;{0}", report.section_length));
            writer.WriteLine(String.Format("Количество фундаментных плит;{0}", report.slabCount));
            writer.WriteLine(String.Format("report.V1;{0}", report.V1));
            foreach (var key in foundationSlabsDictionary.Keys)
            {
                writer.WriteLine(String.Format("report.V2 Фундаменгтная плита L-{0};{1}", key, foundationSlabsDictionary[key].Count));
            }
            writer.WriteLine(String.Format("Количество дефшвов;{0}", report.amount_expansion_joints));
            writer.WriteLine(String.Format("report.V3;{0}", report.V3));
            writer.WriteLine(String.Format("report.V4;{0}", report.V4));
            writer.WriteLine(String.Format("report.V5;{0}", report.V5));
            writer.WriteLine(String.Format("report.V6;{0}", report.V6));
            writer.WriteLine(String.Format("report.V7;{0}", report.V7));
            writer.WriteLine(String.Format("report.V8;{0}", report.V8));
            writer.WriteLine(String.Format("report.V9;{0}", report.V9));
            writer.WriteLine(String.Format("report.V10;{0}", report.V10));
            writer.WriteLine(String.Format("report.V11;{0}", report.V11));
            writer.WriteLine(String.Format("report.V12;{0}", report.V12));
            writer.WriteLine(String.Format("report.V13;{0}", report.V13));
            foreach (string railKey in railSlabsDictionary.Keys)
            {
                var slab = railSlabsDictionary[railKey][0];
                var slabLength = slab.stationEnd - slab.stationStart;
                switch (slabLength)
                {
                    case 5330:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, 1.27 * railSlabsDictionary[railKey].Count));
                        break;
                    case 4832:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, 1.17 * railSlabsDictionary[railKey].Count));
                        break;
                    case 4788:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, 1.16 * railSlabsDictionary[railKey].Count));
                        break;
                    case 4742:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, 1.16 * railSlabsDictionary[railKey].Count));
                        break;
                    case 4245:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, 1.06 * railSlabsDictionary[railKey].Count));
                        break;
                    default:
                        writer.WriteLine(String.Format("report.V14 Рельсовая плита L={0};{0}", railKey, (slabLength / 4100) * railSlabsDictionary[railKey].Count));
                        break;
                }
            }

            foreach (string railKey in railSlabsDictionary.Keys)
            {

                writer.WriteLine(String.Format("report.V15 Рельсовая плита {0};{1}", railKey, railSlabsDictionary[railKey].Count));
            }

            foreach (string railKey in railSlabsDictionary.Keys)
            {
                var slab = railSlabsDictionary[railKey][0];
                var slabLength = slab.stationEnd - slab.stationStart;
                writer.WriteLine(String.Format("report.V16 Рельсовая плита {0};{1}", railKey, slab.width * railSlabsDictionary[railKey].Count * slabLength));
            }
            writer.WriteLine(String.Format("report.V17;{0}", report.V17));
            writer.WriteLine(String.Format("report.V18;{0}", report.V18));
            writer.WriteLine(String.Format("report.V19;{0}", report.V19));
            writer.WriteLine(String.Format("report.V20;{0}", report.V20));
            writer.WriteLine(String.Format("report.V21;{0}", report.V21));
            writer.WriteLine(String.Format("report.V22;{0}", report.V22));
            writer.WriteLine(String.Format("report.V23;{0}", report.V23));
            writer.WriteLine(String.Format("report.V24;{0}", report.V24));
            writer.WriteLine(String.Format("report.V25;{0}", report.V25));
            writer.WriteLine(String.Format("report.V26;{0}", report.V26));
            writer.WriteLine(String.Format("report.V27;{0}", report.V27));
            writer.WriteLine(String.Format("report.V28;{0}", report.V8));
            writer.WriteLine(String.Format("report.V29;{0}", report.V29));
            writer.WriteLine(String.Format("report.V30;{0}", report.V30));
            writer.WriteLine(String.Format("report.V31;{0}", report.V31));
            writer.WriteLine(String.Format("report.V33;{0}", report.V33));
        }


        private void actSaveReports_Execute(object sender, ExecuteEventArgs e)
        {
            Alignment alignment = m_Watcher.CoreAlignment;

            var cadView = CadView;
            var layer = DrawingLayer.GetDrawingLayer(cadView);
            var drawing = layer.Drawing;

            var projectDirectory = Path.GetDirectoryName(ApplicationHost.Current.ActiveProject.TargetProjectFile);
            var reportsDirectoryRoot = Path.Combine(projectDirectory, "Отчеты");
            if (Directory.Exists(reportsDirectoryRoot) == false)
            {
                Directory.CreateDirectory(reportsDirectoryRoot);
            }

            var reportsDirectory = Path.Combine(reportsDirectoryRoot, alignment.GetName());
            if (Directory.Exists(reportsDirectory) == false)
            {
                Directory.CreateDirectory(reportsDirectory);
            }



            object value;
            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                var plugin = (RulesTablePlugin)value;

                List<PlanPart> planParts = PlanConverter.AlignmentToRules(alignment);


                var lineParts = planParts.Where(part => part.Type == PlanPartType.Line).ToList();
                string reportName = "Прямые.csv";
                string reportPath = Path.Combine(reportsDirectory, reportName);
                var lineReport = new Report();

                lineReport.foundationSlabs = new List<Topomatic.Stg.IStgSerializable>();
                lineReport.railSlabs =  new List<Topomatic.Stg.IStgSerializable>();
                lineReport.name = reportName;
                lineReport.reportPath = reportPath;
                lineReport = ConstructReport(lineParts, plugin, lineReport);

                using (StreamWriter writer = new StreamWriter(reportPath))
                {
                    WriteReport(writer, lineReport);
                }


                var curveParts = planParts.Where(part => part.Type == PlanPartType.Curve).ToList();
                string curveReportName = "Кривые.csv";
                string curveReportPath = Path.Combine(reportsDirectory, curveReportName);
                var curveReport = new Report();

                curveReport.foundationSlabs = new List<Topomatic.Stg.IStgSerializable>();
                curveReport.railSlabs = new List<Topomatic.Stg.IStgSerializable>();
                curveReport.name = curveReportName;
                curveReport.reportPath = curveReportPath;
                curveReport = ConstructReport(lineParts, plugin, curveReport);

                using (StreamWriter writer = new StreamWriter(curveReportPath))
                {
                    WriteReport(writer, curveReport);
                }




            }
        }


        public Report ConstructReport(List<PlanPart> parts, RulesTablePlugin plugin, Report report)
        {

                var rules = plugin.RulesTable;
                var rails = plugin.RailsSlabsTable;

                foreach (var part in parts)
                {

                    FoundationSlabTable foundations = plugin.FoundationSlabTable;

                    var foundationSlabs = foundations.Where(el => (el as FoundationSlabTableItem).stationStart >= part.StartStation &&
                        (el as FoundationSlabTableItem).stationEnd <= part.EndStation).ToList();
                    report.foundationSlabs.AddRange(foundationSlabs);

                    var railSlabs = rails.Where(el => (el as RailSlabsTableItem).stationStart >= part.StartStation &&
                        (el as RailSlabsTableItem).stationEnd <= part.EndStation).ToList();
                    report.railSlabs.AddRange(railSlabs);

                    //1 нужно выбрать все фундаментные плиты котолрые начинаются на этом участке
                    //2 нужно выбрать все рельсовые плиты, которые лежат на этих бетонных плитах
                    //3 подумать, что делать с пикетажем.

                    var foundationSlabsDictionary = new Dictionary<double, List<FoundationSlabTableItem>>();

                    foreach (FoundationSlabTableItem slab in foundationSlabs)
                    {
                        if (foundationSlabsDictionary.Keys.Contains(slab.width) == false)
                        {
                            foundationSlabsDictionary[slab.width] = new List<FoundationSlabTableItem>();
                        }
                        foundationSlabsDictionary[slab.width].Add(slab);
                    }

                    var railSlabsDictionary = new Dictionary<string, List<RailSlabsTableItem>>();
                    foreach (RailSlabsTableItem railSlab in railSlabs)
                    {
                        if (railSlabsDictionary.Keys.Contains(railSlab.slab) == false)
                        {
                            railSlabsDictionary[railSlab.slab] = new List<RailSlabsTableItem>();
                        }
                        railSlabsDictionary[railSlab.slab].Add(railSlab);
                    }

                    var amount_expansion_joints = 0;
                    for (int i = 0; i < foundationSlabs.Count - 2; i++)
                    {
                        if ((foundationSlabs[i + 1] as FoundationSlabTableItem).stationStart - (foundationSlabs[i] as FoundationSlabTableItem).stationEnd < 21)
                        {
                            amount_expansion_joints++;
                        }
                    }


                    const int EMBEDDED_PARTS_EXPANSION_JOINT = 6;
                    const double EMBEDDED_PART_WEIGHT = 0.009;
                    const double ELASTOMER_HIGHT = 0.244;
                    const double FOUNDATION_WIDTH = 3.2;
                    const double CONCRETE_SECTION_JACK = 0.05;
                    const double CULVERT_LENGTH = 1;

                    var foundation_slab_cross_section_area = part.Description.Contains("Прямая") ? 0.9536 : 1.184;


                    var slabCount = foundationSlabs.Count();
                    report.slabCount += slabCount;

                    double section_length = part.EndStation - part.StartStation;
                    report.section_length += section_length;

                    report.V1 += section_length * FOUNDATION_WIDTH;
                    report.amount_expansion_joints += amount_expansion_joints;


                    report.V3 += EMBEDDED_PARTS_EXPANSION_JOINT * amount_expansion_joints;
                    report.V4 += EMBEDDED_PARTS_EXPANSION_JOINT * amount_expansion_joints * EMBEDDED_PART_WEIGHT;
                    report.V5 += section_length * FOUNDATION_WIDTH;
                    double v6 = 0;
                    foreach (var key in foundationSlabsDictionary.Keys)
                    {
                        var slab_lengh = key;
                        var sectionLength = foundationSlabsDictionary[key].Max(slab => slab.stationEnd) - foundationSlabsDictionary[key].Min(slab => slab.stationStart);
                        v6 += key * foundationSlabsDictionary[key].Count * foundation_slab_cross_section_area;

                    }
                    report.V6 += v6;
                    report.V7 += slabCount;
                    var amount_fundaments_joint = slabCount;
                    report.amount_fundaments_joint += amount_fundaments_joint;
                    report.V8 += FOUNDATION_WIDTH * amount_fundaments_joint * ELASTOMER_HIGHT;
                    report.V9 += FOUNDATION_WIDTH * slabCount;
                    report.V10 += FOUNDATION_WIDTH * slabCount * 0.0006;
                    report.V11 += railSlabs.Count;
                    report.V12 += railSlabs.Count * 0.197;
                    report.V13 += FOUNDATION_WIDTH * section_length;
                    report.V17 += railSlabs.Count * 2.7 * 0.203 * 2;
                    var amount_rails_joint = railSlabs.Count;
                    report.V18 += 0.24 * 8 * section_length * 0.34 * 3.2 * amount_fundaments_joint + 0.34 * 2.5 * amount_rails_joint;
                    report.V19 += CONCRETE_SECTION_JACK * section_length;
                    var v20 = section_length / CULVERT_LENGTH;
                    report.V20 += v20;
                    var v21 = v20 * 2;
                    report.V21 += v21;
                    report.V22 += (v20 + v21) * 2;
                    var v23 = 0;
                    report.V23 += v23;
                    var v24 = 0;
                    report.V24 += v24;
                    var v25 = (v23 + v24) * 0.37;
                    report.V25 += v25;
                    var v26 = (v23 + v24) * 0.00615;
                    report.V26 += v26;
                    var v27 = part.Description.Contains("Прямая") ? section_length * 0.65 * 1.26 : section_length * 0.24 * 1.26;
                    report.V27 += v27;
                    var v28 = part.Description.Contains("Прямая") ? section_length * 2.5 : section_length * 1.8;
                    report.V28 += v28;
                    var v29 = part.Description.Contains("Прямая") ? section_length * 2 : section_length * 2;
                    report.V29 += v29;
                    var v30 = part.Description.Contains("Прямая") ? section_length * 2.5 : section_length * 1.8;
                    report.V30 += v30;
                    var v31 = section_length;
                    report.V31 += v31;
                    var v33 = section_length;
                    report.V33 += v33;

                } // tut


            return report;
        }

        public static void function1_func(double[] x, ref double func, object obj)
        {
            // this callback calculates f(x0,x1) = 100*(x0+3)^4 + (x1-3)^4

            double combinedBlocksWidth = 0;
            for (int i = 0; i < x.Length; i++)
            {
                combinedBlocksWidth += x[i] * AvailableBlockWidths[i];
            }

            if (combinedBlocksWidth > SectionLength)
            {
                combinedBlocksWidth = SectionLength + combinedBlocksWidth;
            }

            func = Math.Pow(SectionLength - combinedBlocksWidth, 4);
        }


        private static double[] AvailableBlockWidths { get; set; }

        private static double SectionLength { get; set; }


        public static double[] TestLbfgs_numdiff_for_gradient()
        {
            //
            // This example demonstrates minimization of f(x,y) = 100*(x+3)^4+(y-3)^4
            // using numerical differentiation to calculate gradient.
            //
            double[] x = new double[AvailableBlockWidths.Length] ;
            double epsg = 1.0e-6;
            double epsf = 0;
            double epsx = 0;
            //double diffstep = 1.0e-6;
            double diffstep = 1.0e-6;
            int maxits = 0;
            alglib.minlbfgsstate state;
            alglib.minlbfgsreport rep;

            alglib.minlbfgscreatef(1, x, diffstep, out state);
            alglib.minlbfgssetcond(state, epsg, epsf, epsx, maxits);
            alglib.minlbfgsoptimize(state, function1_func, null, null);
            alglib.minlbfgsresults(state, out x, out rep);



            double combinedBlocksWidth = 0;
            for (int i = 0; i < x.Length; i++)
            {
                combinedBlocksWidth += x[i] * AvailableBlockWidths[i];
            }

            MessageBox.Show(String.Format("КолПлит0: {0} ДлинПлит0 : {1} ДлинУчастка: {2} остаток: {3}", alglib.ap.format(x, x.Length), 
                String.Join(",",AvailableBlockWidths.Select(p=>p.ToString()).ToArray()), 
                SectionLength,
                Math.Abs(SectionLength - combinedBlocksWidth)));

            return x;
        }


        private void actRecalculateRules_Execute(object sender, ExecuteEventArgs e)
        {

            Alignment alignment = m_Watcher.CoreAlignment;

            if (alignment == null)
            {
                return;
            }
            object value;

            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                var plugin = (RulesTablePlugin)value;
                var rules = plugin.RulesTable;
                var newRulesTable = new RulesTable(alignment);

                double StandartGap = Settings.Setting<double>("DEFAULT_FOUNDATION_SLAB_GAP");
                double MaximumDelta = Settings.Setting<double>("DEFAULT_FOUNDATION_SLAB_GAP_DELTA");
                double MinGap = StandartGap - MaximumDelta;
                double MaxGap = StandartGap + MaximumDelta;

                var layer = DrawingLayer.GetDrawingLayer(CadView);
                if (layer != null)
                {
                    var drawing = layer.Drawing;

                    var available_blocks = drawing.Blocks.Names.Keys.Where(el => ((el.StartsWith("NGP") && !el.EndsWith("_dim") && !el.EndsWith("_Profile"))));
                    foreach (RulesTableItem rule in rules)
                    {

                        var block = drawing.Blocks[rule.slab];

                        var maxSlabCount = Math.Truncate((rule.stationEnd -
                            rule.stationStart) / (block.Bounds.Width + MinGap));


                        var remainder = rule.stationEnd - (rule.stationStart +
                            (block.Bounds.Width + rule.defaultGap) * Math.Truncate((rule.stationEnd -
                            rule.stationStart) / (block.Bounds.Width + rule.defaultGap)));

                        //var gap = FindIdealGap(rule, block);

                        // Если остаток меньше GAP_ERR (0.000000001), то оставить как есть, в противном случае попытаться пересчитать добором (с использованием других плит)
                        if (remainder < GAP_ERR) {
                            newRulesTable.Add(new RulesTableItem() { 
                                defaultGap = rule.defaultGap,
                                slab = rule.slab,
                                slabProfile = rule.slabProfile,
                                stationEnd = rule.stationEnd,
                                stationStart = rule.stationStart,

                            });
                        } else
                        {
                            var tryBlocks = available_blocks.Where(b => drawing.Blocks[b].Bounds.Width < block.Bounds.Width).ToList();

                            var tryBlocks_DWG = tryBlocks.Select(blockName => drawing.Blocks[blockName]).ToList();
                            var minBlock_width = tryBlocks_DWG.Min(b => b.Bounds.Width);
                            var blockCount = (int)Math.Truncate((rule.stationEnd -
                            rule.stationStart) / (minBlock_width + rule.defaultGap - Settings.Setting<double>("DEFAULT_FOUNDATION_SLAB_GAP_DELTA")));



                            bool ruleFound = false;
                            List<List<RulesTableItem>> variants = new List<List<RulesTableItem>> (); 
                            foreach (var nublock in tryBlocks)
                            {

                                int stepBack = 1;
                                List<RulesTableItem> variant = new List<RulesTableItem>();
                                while (stepBack < maxSlabCount + 1)
                                {
                                    var mainSlabcount = maxSlabCount - stepBack;
                                    var newEndStation = rule.stationStart + (block.Bounds.Width + MaxGap) * mainSlabcount;
                                    var mainMaxRemainder = rule.stationEnd - newEndStation;
                                    var maxNewSlabCount = Math.Truncate((rule.stationEnd - newEndStation) / (drawing.Blocks[nublock].Bounds.Width + MinGap));
                                    var maxNewRemainder = rule.stationEnd - (newEndStation + (drawing.Blocks[nublock].Bounds.Width + MaxGap) * maxNewSlabCount);
                                    if (maxNewRemainder < 0)
                                    {
                                        var nugap = MaxGap - (0 - maxNewRemainder) / (maxNewSlabCount + mainSlabcount);
                                        var nuEndstation = rule.stationStart +
                                        (block.Bounds.Width + nugap) * mainSlabcount;

                                        if (mainSlabcount != 0)
                                        {
                                            variant.Add(new RulesTableItem()
                                            {
                                                defaultGap = nugap - GAP_ERR,
                                                slab = block.Name,
                                                slabProfile = drawing.Blocks[block.Name + "_Profile"] != null ? block.Name + "_Profile" : null,
                                                stationEnd = nuEndstation,
                                                stationStart = rule.stationStart,

                                            });
                                        }
                                        variant.Add(new RulesTableItem()
                                        {
                                            defaultGap = nugap - GAP_ERR,
                                            slab = nublock,
                                            slabProfile = drawing.Blocks[nublock + "_Profile"] != null ? nublock + "_Profile" : null,
                                            stationEnd = rule.stationEnd,
                                            stationStart = nuEndstation,

                                        });
                                        ruleFound = true;
                                        break;
                                    }
                                    else
                                    {
                                        stepBack++;
                                    }

                                }
                                if (variant.Count > 0)
                                {
                                    variants.Add(variant);
                                }
                            } 
                            
                            //Нахожу вариант,  у которого зазор ближе к 0.1
                            if (variants.Count == 0)
                            {
                                newRulesTable.Add(rule);
                            } else
                            {
                                double avgGap = 0;
                                int variantIndx = 0;
                                foreach(var variant in variants)
                                {
                                    double tempGap = 0;
                                    foreach(var rRule in variant)
                                    {
                                        tempGap += rRule.defaultGap;
                                    }

                                    var tAvgGap = tempGap / variant.Count;

                                    if (Math.Abs(0.1 - tAvgGap) < Math.Abs(0.1 - avgGap))
                                    {
                                        avgGap = tAvgGap;
                                        variantIndx = variants.IndexOf(variant);
                                    }
                                }

                                foreach(var tRule in variants[variantIndx])
                                {
                                    newRulesTable.Add(tRule);
                                }

                            }

                        }
                        
                    }

                    plugin.RulesTable = newRulesTable;
                }
            }
        }




        private void InitializeComponent()
        {
            SlabsConfig.Instance.module = this;



            this.actImportDefaultDxfToActiveAlignment = new CallAction();
            this.actImportDefaultDxfToActiveAlignment.Caption = "_Импортировать стандартный блоки на активный подобъект";
            this.actImportDefaultDxfToActiveAlignment.Cmd = "";
            this.actImportDefaultDxfToActiveAlignment.UID = "ID_actImportDefaultDxfToActiveAlignment";
            this.actImportDefaultDxfToActiveAlignment.Execute += new ExecuteEventHandler(this.actImportDefaultDxfToActiveAlignment_Execute);
            this.Items.Add(this.actImportDefaultDxfToActiveAlignment);


            this.actImportDefaultDxfToAllAlignments = new CallAction();
            this.actImportDefaultDxfToAllAlignments.Caption = "_Импортировать стандартный блоки на все подобъекты";
            this.actImportDefaultDxfToAllAlignments.Cmd = "";
            this.actImportDefaultDxfToAllAlignments.UID = "actImportDefaultDxfToAllAlignments";
            this.actImportDefaultDxfToAllAlignments.Execute += new ExecuteEventHandler(this.actImportDefaultDxfToAllAlignments_Execute);
            this.Items.Add(this.actImportDefaultDxfToAllAlignments);



            this.actRecalculateRules = new CallAction();
            this.actRecalculateRules.Caption = "_Рассчитать укладку без остатка";
            this.actRecalculateRules.Cmd = "";
            this.actRecalculateRules.UID = "ID_actRecalculateRules";
            this.actRecalculateRules.Execute += new ExecuteEventHandler(this.actRecalculateRules_Execute);
            this.Items.Add(this.actRecalculateRules);

            this.actDrawSchemeFour = new CallAction();
            this.actDrawSchemeFour.Caption = "_Построить схему 4";
            this.actDrawSchemeFour.Cmd = "";
            this.actDrawSchemeFour.UID = "ID_actDrawSchemeFour";
            this.actDrawSchemeFour.Execute += new ExecuteEventHandler(this.actDrawSchemeFour_Execute);
            this.Items.Add(this.actDrawSchemeFour);

            this.actDrawSchemeFive = new CallAction();
            this.actDrawSchemeFive.Caption = "_Построить схему 5";
            this.actDrawSchemeFive.Cmd = "";
            this.actDrawSchemeFive.UID = "ID_actDrawSchemeFive";
            this.actDrawSchemeFive.Execute += new ExecuteEventHandler(this.actDrawSchemeFive_Execute);
            this.Items.Add(this.actDrawSchemeFive);

            this.actPopulateRailSlabFromMainAlignment = new CallAction();
            this.actPopulateRailSlabFromMainAlignment.Caption = "_Заполнить таблицу рельсовых плит по главной таблице";
            this.actPopulateRailSlabFromMainAlignment.Cmd = "";
            this.actPopulateRailSlabFromMainAlignment.UID = "ID_actPopulateRailSlabFromMainAlignment";
            this.actPopulateRailSlabFromMainAlignment.Execute += new ExecuteEventHandler(this.actPopulateRailSlabFromMainAlignment_Execute);
            this.Items.Add(this.actPopulateRailSlabFromMainAlignment);


            this.actFillRulesWithCurves = new CallAction();
            this.actFillRulesWithCurves.Caption = "_Разбить по плану линии ";
            this.actFillRulesWithCurves.Cmd = "";
            this.actFillRulesWithCurves.UID = "ID_SLAB_FillRulesWithCurves";
            this.actFillRulesWithCurves.Execute += new ExecuteEventHandler(this.actFillRulesWithCurves_Execute);
            this.Items.Add(this.actFillRulesWithCurves);

            this.actEditCoordinateTable = new CallAction();
            this.actEditCoordinateTable.Caption = "_Открыть таблицу координат рельсовых плит";
            this.actEditCoordinateTable.Cmd = "";
            this.actEditCoordinateTable.UID = "ID_SLAB_CoordinateTable_EDIT_TABLE";
            this.actEditCoordinateTable.Execute += new ExecuteEventHandler(this.actEditCoordinateTable_Execute);
            this.Items.Add(this.actEditCoordinateTable);

            this.actEditFoundationSlabsTable = new CallAction();
            this.actEditFoundationSlabsTable.Caption = "_Открыть таблицу раскладки фундаментных плит";
            this.actEditFoundationSlabsTable.Cmd = "";
            this.actEditFoundationSlabsTable.UID = "ID_SLAB_FoundationSlabs_EDIT_TABLE";
            this.actEditFoundationSlabsTable.Execute += new ExecuteEventHandler(this.actEditFoundationTable_Execute);
            this.Items.Add(this.actEditFoundationSlabsTable);

            this.actEditSlabRulesTable = new CallAction();
            this.actEditSlabRulesTable.Caption = "_Открыть таблицу правил раскладки плит";
            this.actEditSlabRulesTable.Cmd = "";
            this.actEditSlabRulesTable.UID = "ID_SLAB_RULES_EDIT_TABLE";
            this.actEditSlabRulesTable.Execute += new ExecuteEventHandler(this.actSimplePluginEditTable_Execute);
            this.Items.Add(this.actEditSlabRulesTable);

            this.actEditRailSlabsTable = new CallAction();
            this.actEditRailSlabsTable.Caption = "_Открыть таблицу рельсовых плит";
            this.actEditRailSlabsTable.Cmd = "";
            this.actEditRailSlabsTable.UID = "ID_SLAB_RailSlabs_EDIT_TABLE";
            this.actEditRailSlabsTable.Execute += new ExecuteEventHandler(this.actSimplePluginEditRailSlabsTable_Execute);
            this.Items.Add(this.actEditRailSlabsTable);

            this.actPopulateRailSlab = new CallAction();
            this.actPopulateRailSlab.Caption = "_Заполнить таблицу рельсовых плит";
            this.actPopulateRailSlab.Cmd = "popSlab";
            this.actPopulateRailSlab.UID = "ID_SLAB_RailSlabs_POPULATE_TABLE";
            this.actPopulateRailSlab.Execute += new ExecuteEventHandler(this.actPopulateRailSlab_Execute);
            this.Items.Add(this.actPopulateRailSlab);

            this.actDrawRailSlab = new CallAction();
            this.actDrawRailSlab.Caption = "_Построить БВСП на плане";
            this.actDrawRailSlab.Cmd = "drawSlab";
            this.actDrawRailSlab.UID = "ID_SLAB_RailSlabs_Draw_TABLE";
            this.actDrawRailSlab.Execute += new ExecuteEventHandler(this.actDrawRailSlab_Execute);
            this.Items.Add(this.actDrawRailSlab);

            this.actDrawScheme = new CallAction();
            this.actDrawScheme.Caption = "_Построить схему 1";
            this.actDrawScheme.Cmd = "drawScheme";
            this.actDrawScheme.UID = "ID_SLAB_RailSlabs_Draw_SCHEME";
            this.actDrawScheme.Execute += new ExecuteEventHandler(this.actDrawScheme_Execute);
            this.Items.Add(this.actDrawScheme);

            this.actSaveReports = new CallAction();
            this.actSaveReports.Caption = "_Сохранить объемы";
            this.actSaveReports.Cmd = "saveReports";
            this.actSaveReports.UID = "ID_SLAB_Save_Report";
            this.actSaveReports.Execute += new ExecuteEventHandler(this.actSaveReports_Execute);
            this.Items.Add(this.actSaveReports);

        }

        protected override void OnApplicationInitialized(object sender,
        ApplicationInitializedEventArgs e)
        {
            base.OnApplicationInitialized(sender, e);
        }

        private bool CanApplyPlugins(ICoreCollectionItem item)
        {
            //Проверяем что элемент содержит подобъект
            var container = item.WrappedObject as IAlignmentContainer;
            if (container != null)
            {
                //И именно автомобильную дорогу
                return container.Alignment is Alignment;
            }
            return false;
        }

        private void AddPlugins(ICoreCollectionItem item)
        {
            //Проверяем, что объект ядра содержит подобъект
            var alignmentContainer = (item.WrappedObject as IAlignmentContainer);
            if (alignmentContainer != null)
            {
                //Любую ось (Alignment)
                var alignment = alignmentContainer.Alignment as Alignment;
                if (alignment != null)
                {
                    //Создаем наш плагин
                    var plugin = new RulesTablePlugin(alignment);
                    //Добавляем его в список плагинов подобъекта
                    alignment.Plugins.Add(RulesTablePluginConsts.PluginID, plugin);
                    //Добавляем таблицы в дерево структуры если необходимо
                    if (item is IAlignmentCoreCollectionItem)
                    {
                        //Добавляем таблицу в структуру подобъекта
                        ((IAlignmentCoreCollectionItem)item).AddCoreItem(new RulesTablePluginCoreItem(alignment, plugin));
                        ((IAlignmentCoreCollectionItem)item).AddCoreItem(new RailSlabsPluginCoreItem(alignment, plugin));
                        ((IAlignmentCoreCollectionItem)item).AddCoreItem(new FoundationSlabTablePluginCoreItem(alignment, plugin));
                        ((IAlignmentCoreCollectionItem)item).AddCoreItem(new CoordinateTablePluginCoreItem(alignment, plugin));
                    }
                }
            }
        }

        protected override void OnHandleEvent(object sender, RaiseMessageEventArgs e)
        {
            base.OnHandleEvent(sender, e);
            //Если это событие создания элемента ядра, надо добавить туда наш плагин, если это необходимо
            if (e is AfterCreateCoreCollectionItemEventArgs)
            {
                var create_evnt = (AfterCreateCoreCollectionItemEventArgs)e;
                //Проверяем, необходимо или нет
                if (CanApplyPlugins(create_evnt.Item))
                {
                    //Добавляем плагин
                    AddPlugins(create_evnt.Item);
                }
            }
        }

        private AlignmentWatcher<Alignment> m_Watcher = new AlignmentWatcher<Alignment>();

        private void ActivateAlignment(Alignment alignment)
        {

            var action = ApplicationHost.Current.AddIns["ID_CHANGE_ACTIVE_CORE_ITEM"] as CallAction;

            Predicate<object> match = delegate (object obj)
            {
                var item = (obj as ICoreCollectionItem);
                if (item != null)
                {
                    if (item.WrappedObject is IAlignmentContainer)
                    {
                        if ((item.WrappedObject as IAlignmentContainer).Alignment == alignment)
                        {
                            return true;
                        }
                    }
                }
                return false;
            };


            if (action != null)
            {
                action.PerformExecute(new ExecuteEventArgs(ModelWatcher.Current.Project, match));

            }
        }

        private void actPopulateRailSlab_Execute(object sender, ExecuteEventArgs e)
        {

            Alignment alignment = m_Watcher.CoreAlignment;
            if (alignment == null)
            {
                return;
            }
            object value;
            //Получаем плагин
            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value))
            {
                //Редактируем таблицу
                var plugin = (RulesTablePlugin)value;
                plugin.RailsSlabsTable = new RailSlabsTable(plugin);
                plugin.FoundationSlabTable = new FoundationSlabTable(plugin);
                plugin.CoordinateTable = new CoordinateTable(plugin);
                PopulateRailSlabs(plugin.RulesTable, plugin.RailsSlabsTable, plugin.CoordinateTable);
                CustomPopulateFoundationSlabTable(plugin.FoundationSlabTable, plugin.RailsSlabsTable, plugin.RulesTable, alignment);

            }
        }

        private void actPopulateRailSlabFromMainAlignment_Execute(object sender, ExecuteEventArgs e)
        {
           
            Alignment alignment = m_Watcher.CoreAlignment;
            if (alignment == null)
            {
                return;
            }
            Alignment subAlignment = m_Watcher.CoreAlignment;
            List<Alignment> alignments = AlignmentWatcher.GetAlignments(p => p != alignment).ToList();
            if (alignments.Count != 0 )
            {
                subAlignment  = AlignmentWatcher.GetAlignments(p => p != alignment).First();
            }
            object value;
            object subValue;
            //Получаем плагин
            if (alignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out value) && subAlignment.Plugins.TryGetValue(RulesTablePluginConsts.PluginID, out subValue))
            {

                //Редактируем таблицу
                var plugin = (RulesTablePlugin)value;
                var subPlugin = (RulesTablePlugin)subValue;

                RecalculateRulesTableFromMainAlignment(alignment, subAlignment);

                plugin.RailsSlabsTable = new RailSlabsTable(plugin);
                plugin.FoundationSlabTable = new FoundationSlabTable(plugin);
                plugin.CoordinateTable = new CoordinateTable(plugin);       

                PopulateRailSlabs(plugin.RulesTable, plugin.RailsSlabsTable, plugin.CoordinateTable);
                CustomPopulateFoundationSlabTable(plugin.FoundationSlabTable, plugin.RailsSlabsTable, plugin.RulesTable, alignment);

                if (plugin != subPlugin) {
                    subPlugin.RailsSlabsTable = new RailSlabsTable(subPlugin);
                    subPlugin.FoundationSlabTable = new FoundationSlabTable(subPlugin);
                    subPlugin.CoordinateTable = new CoordinateTable(subPlugin);
                    PopulateRailsSlabsFromMainAlignment(plugin.RailsSlabsTable, alignment, subPlugin.RailsSlabsTable, subPlugin.CoordinateTable, subAlignment);
                    CustomPopulateFoundationSlabTable(subPlugin.FoundationSlabTable, subPlugin.RailsSlabsTable, subPlugin.RulesTable, subAlignment);
                }

            }

        }


        private void actSimplePluginEditTable_Execute(object sender, ExecuteEventArgs e)
        {
            //Проверяем что количестов аргументов соответствует тому что мы отправляли
            if ((e.ObjectParams != null) && (e.ObjectParams.Length == 2))
            {
                //Получаем объекты которые отправили
                var alignment = (Alignment)e.ObjectParams[0];
                var plugin = (RulesTablePlugin)e.ObjectParams[1];
                //Вызываем процедуру редактирования таблицы
                EditTable(alignment, plugin.RulesTable);
            }
        }

        private void actEditFoundationTable_Execute(object sender, ExecuteEventArgs e)
        {
            //Проверяем что количестов аргументов соответствует тому что мы отправляли
            if ((e.ObjectParams != null) && (e.ObjectParams.Length == 2))
            {
                //Получаем объекты которые отправили
                var alignment = (Alignment)e.ObjectParams[0];
                var plugin = (RulesTablePlugin)e.ObjectParams[1];
                //Вызываем процедуру редактирования таблицы
                EditFoundationSlabTable(alignment, plugin.FoundationSlabTable);
            }
        }

        private void actEditCoordinateTable_Execute(object sender, ExecuteEventArgs e)
        {
            if ((e.ObjectParams != null) && (e.ObjectParams.Length == 2))
            {
                //Получаем объекты которые отправили
                var alignment = (Alignment)e.ObjectParams[0];
                var plugin = (RulesTablePlugin)e.ObjectParams[1];
                //Вызываем процедуру редактирования таблицы
                EditCoordinateTable(alignment, plugin.CoordinateTable);
            }
        }

        private void actSimplePluginEditRailSlabsTable_Execute(object sender, ExecuteEventArgs e)
        {
            //Проверяем что количестов аргументов соответствует тому что мы отправляли
            if ((e.ObjectParams != null) && (e.ObjectParams.Length == 2))
            {
                //Получаем объекты которые отправили
                var alignment = (Alignment)e.ObjectParams[0];
                var plugin = (RulesTablePlugin)e.ObjectParams[1];
                //Вызываем процедуру редактирования таблицы
                EditRailSlabsTable(alignment, plugin.RailsSlabsTable);
            }
        }

        public struct RectanglePoints
        {
            public Vector2D lowerLeft;
            public Vector2D upperLeft;
            public Vector2D lowerRight;
            public Vector2D upperRight;
        }

        public RectanglePoints PointsFromInsertPointsAngle(DwgBlock bloc, Vector2D insertLocation, double angle)
        {

            Vector2D centerLL = bloc.Bounds.BottomLeft - bloc.Bounds.Center;
            centerLL.Rotate(angle);
            Vector2D lowerLeft = bloc.Bounds.Center + insertLocation + centerLL;

            Vector2D centerUL = bloc.Bounds.TopLeft - bloc.Bounds.Center;
            centerUL.Rotate(angle);
            Vector2D upperLeft = bloc.Bounds.Center + insertLocation + centerUL;

            Vector2D centerLR = bloc.Bounds.BottomRight - bloc.Bounds.Center;
            centerLR.Rotate(angle);
            Vector2D lowerRight = bloc.Bounds.Center + insertLocation + centerLR;

            Vector2D centerUR = bloc.Bounds.TopRight - bloc.Bounds.Center;
            centerUR.Rotate(angle);
            Vector2D upperRight = bloc.Bounds.Center + insertLocation + centerUR;

            return new RectanglePoints() { lowerLeft = lowerLeft, upperLeft = upperLeft, lowerRight = lowerRight, upperRight = upperRight };
        }

        private void EditTable(Alignment alignment, RulesTable table)
        {
            //Начинаем редактирование с возможностью отмены изменений
            alignment.BeginTransaction("Изменение таблицы правил раскладки плит");
            bool result = false;
            try
            {
                //Вызываем стандартный диалог таблицы с нашей оберткой таблицы
                result = EditTableDlg.Execute("Таблица правил раскладки плит", new RulesTableWrapper(table), alignment.ReadOnly, false);
            }
            finally
            {
                //Если диалог завершен по кнопке Ок
                if (result)
                {
                    //Применяем изменения
                    alignment.Commit();
                    //Обновялем текущий вид
                    var cadView = CadView;
                    if (cadView != null)
                    {
                        cadView.Unlock();
                        cadView.Refresh();
                    }
                }
                else
                {
                    //Откатываем изменения назад
                    alignment.Rollback();
                }
            }

        }


        private void EditFoundationSlabTable(Alignment alignment, FoundationSlabTable table)
        {
            //Начинаем редактирование с возможностью отмены изменений
            alignment.BeginTransaction("Изменение таблицы раскладки фундаментиных плит");
            bool result = false;
            try
            {
                //Вызываем стандартный диалог таблицы с нашей оберткой таблицы
                result = EditTableDlg.Execute("Таблица раскладки фундаментрых плит", new FoundationSlabTableWrapper(table), alignment.ReadOnly, false);
            }
            finally
            {
                //Если диалог завершен по кнопке Ок
                if (result)
                {
                    //Применяем изменения
                    alignment.Commit();
                    //Обновялем текущий вид
                    var cadView = CadView;
                    if (cadView != null)
                    {
                        cadView.Unlock();
                        cadView.Refresh();
                    }
                }
                else
                {
                    //Откатываем изменения назад
                    alignment.Rollback();
                }
            }

        }

        private void EditCoordinateTable(Alignment alignment, CoordinateTable table)
        {
            //Начинаем редактирование с возможностью отмены изменений
            alignment.BeginTransaction("Изменение таблицы координат рельсовых плит");
            bool result = false;
            try
            {
                //Вызываем стандартный диалог таблицы с нашей оберткой таблицы
                result = EditTableDlg.Execute("Таблица раскладки координат рельсовых плит", new CoordinateTableWrapper(table), alignment.ReadOnly, false);
            }
            finally
            {
                //Если диалог завершен по кнопке Ок
                if (result)
                {
                    //Применяем изменения
                    alignment.Commit();
                    //Обновялем текущий вид
                    var cadView = CadView;
                    if (cadView != null)
                    {
                        cadView.Unlock();
                        cadView.Refresh();
                    }
                }
                else
                {
                    //Откатываем изменения назад
                    alignment.Rollback();
                }
            }

        }


        private void EditRailSlabsTable(Alignment alignment, RailSlabsTable table)
        {
            //Начинаем редактирование с возможностью отмены изменений
            alignment.BeginTransaction("Изменение таблицы раскладки плит");
            bool result = false;
            try
            {
                //Вызываем стандартный диалог таблицы с нашей оберткой таблицы
                result = EditTableDlg.Execute("Таблица раскладки плит", new RailSlabsTableWrapper(table), alignment.ReadOnly, false);
            }
            finally
            {
                //Если диалог завершен по кнопке Ок
                if (result)
                {
                    //Применяем изменения
                    alignment.Commit();
                    //Обновялем текущий вид
                    var cadView = CadView;
                    if (cadView != null)
                    {
                        cadView.Unlock();
                        cadView.Refresh();
                    }
                }
                else
                {
                    //Откатываем изменения назад
                    alignment.Rollback();
                }
            }

        }

    }
}

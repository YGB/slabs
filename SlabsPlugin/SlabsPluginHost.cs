﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.ApplicationPlatform;

namespace SlabsPlugin
{
    public class SlabsPluginHost : PluginHost
    {
        protected override Type[] GetModules()
        {
            return new Type[] { typeof(SlabsPlugin) };
        }

        public override string PluginName
        {
            get { return "SlabsPlugin"; }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg;

namespace SlabsPlugin
{
    public static class AlignmentIterator
    {
        public enum AreaType {
            LINE,
            CURVE
        }

        public struct Iteration {
            public double StationStart;
            public double StationEnd;
            public AreaType AreaType;
        }


        static public IEnumerable<Iteration> Iterate(Alignment alignment, RulesTable rules)
        {
            Iteration iteration = new Iteration();


            yield return iteration;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Stg;

namespace SlabsPlugin
{
    public struct RulesTableItem: IStgSerializable
    {
        public string slab; // Тип плиты
        public string slabProfile; // Тип плиты в профиль
        public double stationStart; // Пикетаж начала участк-а
        public double stationEnd; // Пикетаж конца участка
        public double defaultGap; //Стандартный отсуп

        public void LoadFromStg(StgNode node)
        {
            try
            {
                this.slab = node.GetString("slab");
                this.slabProfile = node.GetString("slabProfile");
                this.stationStart = node.GetDouble("stationStart");
                this.stationEnd = node.GetDouble("stationEnd");
                this.defaultGap = node.GetDouble("defaultGap");
            } catch
            {

            }
        }




        public void SaveToStg(StgNode node)
        {
            node.AddString("slab", this.slab);
            node.AddString("slabProfile", this.slabProfile);
            node.AddDouble("stationStart",this.stationStart);
            node.AddDouble("stationEnd",this.stationEnd);
            node.AddDouble("defaultGap", this.defaultGap);

        }
    }
}

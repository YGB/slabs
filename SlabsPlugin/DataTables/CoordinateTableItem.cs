﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Cad.Foundation;
using Topomatic.Stg;

namespace SlabsPlugin
{

    public class CoordinateTableItem : IStgSerializable
    {

        public string slab; // Тип плиты
        public double startStation;
        public Vector2D lowerLeft;
        public Vector2D upperLeft;
        public Vector2D lowerRight;
        public Vector2D upperRight;

        public void LoadFromStg(StgNode node)
        {
            try
            {
                this.slab = node.GetString("slab");

                this.startStation = node.GetDouble("startStation");

                this.lowerLeft = new Vector2D(node.GetDouble("llX"), node.GetDouble("llY"));

                this.upperLeft = new Vector2D(node.GetDouble("ulX"), node.GetDouble("ulY"));

                this.lowerRight = new Vector2D(node.GetDouble("lrX"), node.GetDouble("lrY"));

                this.upperRight = new Vector2D(node.GetDouble("urX"), node.GetDouble("urY"));
            } catch
            {

            }

        }
        public void SaveToStg(StgNode node)
        {
            node.AddString("slab", this.slab);

            node.AddDouble("startStation", this.startStation);

            node.AddDouble("llX", this.lowerLeft.X);
            node.AddDouble("llY", this.lowerLeft.Y);

            node.AddDouble("ulX", this.upperLeft.X);
            node.AddDouble("ulY", this.upperLeft.Y);

            node.AddDouble("lrX", this.lowerRight.X);
            node.AddDouble("lrY", this.lowerRight.Y);

            node.AddDouble("urX", this.upperRight.X);
            node.AddDouble("urY", this.upperRight.Y);

        }
    }
}


﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.FoundationClasses;
using Topomatic.FoundationClasses.Undo;
using Topomatic.Stg;

namespace SlabsPlugin
{

    public abstract class AbstractDataTable: UpdatableObject, IList<IStgSerializable>, IStgSerializable, IItem, IAlignmentContainer
    {
        protected TransactableList<IStgSerializable> m_Items;
        protected object m_Parent = null;

        public IStgSerializable this[int index] 
        { 
            get => m_Items[index]; 
            set => m_Items[index] = value; 
        }

        public int Count => m_Items.Count;
        public bool IsReadOnly => false;
        public object Parent
        {
            get => m_Parent;
            set => throw new NotSupportedException();
        }

        public Alignment Alignment
        {
            get
            {
                var container = Parent as IAlignmentContainer;
                if (container == null)
                    return null;
                return container.Alignment;
            }
        }

        public void Add(IStgSerializable item) => m_Items.Add(item);
        public void Clear() => m_Items.Clear();
        public bool Contains(IStgSerializable item) => m_Items.Contains(item);
        public void CopyTo(IStgSerializable[] array, int arrayIndex) => m_Items.CopyTo(array, arrayIndex);
        public IEnumerator<IStgSerializable> GetEnumerator() => m_Items.GetEnumerator();
        public int IndexOf(IStgSerializable item) => m_Items.IndexOf(item);
        public void Insert(int index, IStgSerializable item) => m_Items.Insert(index, item);
        public void RemoveAt(int index) => m_Items.RemoveAt(index);
        public bool Remove(IStgSerializable item) =>((ICollection<IStgSerializable>)m_Items).Remove(item);
        IEnumerator IEnumerable.GetEnumerator() => m_Items.GetEnumerator();

        public abstract void SaveToStg(StgNode node);
        public abstract void LoadFromStg(StgNode node);
   
    }
    
}

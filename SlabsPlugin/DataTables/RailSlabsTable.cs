﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.FoundationClasses.Undo;
using Topomatic.Stg;

namespace SlabsPlugin
{

    public class RailSlabsTable : AbstractDataTable
    {
        public RailSlabsTable(object parent)
        {

            m_Parent = parent;
            m_Items = new TransactableList<IStgSerializable>(this);
        }

        public IList<RailSlabsTableItem> Items { get => m_Items as IList<RailSlabsTableItem>; }

        public override void LoadFromStg(StgNode node)
        {
            m_Items.InnerList.Clear();
            var array = node.GetArray("RailSlabsTableItems", StgType.Node);
            for (int i = 0; i < array.Count; i++)
            {
                var tableItem = new RailSlabsTableItem();
                tableItem.LoadFromStg(array.GetNode(i));
                m_Items.InnerList.Add(tableItem);
            }
        }

        public override void SaveToStg(StgNode node)
        {
            var array = node.AddArray("RailSlabsTableItems", StgType.Node);
            array.ItemsName = "RailSlabsTableItem";
            for (int i = 0; i < m_Items.Count; i++)
            {
                m_Items[i].SaveToStg(array.AddNode());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Cad.Foundation;
using Topomatic.Stg;

namespace SlabsPlugin
{

    public class RailSlabsTableItem : IStgSerializable
    {

        public string slab; // Тип плиты
        public string slabProfile; // Тип плиты
        public double stationStart; // Пикетаж начала участк-а
        public double stationEnd; // Пикетаж конца участка
        public Vector3D location;
        public Vector3D locationEnd;
        public double angle;

        public double width;

        public void LoadFromStg(StgNode node)
        {
            try
            {
                this.slab = node.GetString("slab");
                this.slabProfile = node.GetString("slabProfile");
                this.stationStart = node.GetDouble("stationStart");
                this.stationEnd = node.GetDouble("stationEnd");
                this.location = new Vector3D(node.GetDouble("X"), node.GetDouble("Y"), node.GetDouble("Z"));
                this.locationEnd = new Vector3D(node.GetDouble("Xend"), node.GetDouble("Yend"), node.GetDouble("Zend"));
                this.angle = node.GetDouble("angle");
                this.width = node.GetDouble("width");
            } catch
            {

            }
        }

        public void SaveToStg(StgNode node)
        {
            node.AddString("slab", this.slab);
            node.AddString("slabProfile", this.slabProfile);
            node.AddDouble("stationStart", this.stationStart);
            node.AddDouble("stationEnd", this.stationEnd);
            node.AddDouble("X", this.location.X);
            node.AddDouble("Y", this.location.Y);
            node.AddDouble("Z", this.location.Z);
            node.AddDouble("Xend", this.locationEnd.X);
            node.AddDouble("Yend", this.locationEnd.Y);
            node.AddDouble("Zend", this.locationEnd.Z);
            node.AddDouble("angle", this.angle);
            node.AddDouble("width", this.width);
        }
    }
}


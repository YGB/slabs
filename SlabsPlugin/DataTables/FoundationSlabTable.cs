﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.FoundationClasses.Undo;
using Topomatic.Stg;

namespace SlabsPlugin
{
    public class FoundationSlabTable : AbstractDataTable
    {
        public FoundationSlabTable(object parent)
        {

            m_Parent = parent;
            m_Items = new TransactableList<IStgSerializable>(this);
        }

        public IList<FoundationSlabTableItem> Items { get => m_Items as IList<FoundationSlabTableItem>; }

        public override void LoadFromStg(StgNode node)
        {
            m_Items.InnerList.Clear();
            var array = node.GetArray("FoundationSlabTableItems", StgType.Node);
            for (int i = 0; i < array.Count; i++)
            {

                var tableItem = new FoundationSlabTableItem();
                tableItem.LoadFromStg(array.GetNode(i));
                m_Items.InnerList.Add(tableItem);
            }
        }

        public override void SaveToStg(StgNode node)
        {
            var array = node.AddArray("FoundationSlabTableItems", StgType.Node);
            array.ItemsName = "FoundationSlabTableItem";
            for (int i = 0; i < m_Items.Count; i++)
            {
                m_Items[i].SaveToStg(array.AddNode());
            }
        }

    }
}

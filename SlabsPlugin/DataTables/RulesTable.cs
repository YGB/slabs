﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.FoundationClasses.Undo;
using Topomatic.Stg;

namespace SlabsPlugin
{

    public class RulesTable : AbstractDataTable
    {
        public RulesTable(object parent)
        {

            m_Parent = parent;
            m_Items = new TransactableList<IStgSerializable>(this);
        }

        public IList<RulesTableItem> Items { get => m_Items as IList<RulesTableItem>; }

        public override void LoadFromStg(StgNode node)
        {
            m_Items.InnerList.Clear();
            var array = node.GetArray("RulesTableItems", StgType.Node);
            for (int i = 0; i < array.Count; i++)
            {
                var tableItem = new RulesTableItem();
                tableItem.LoadFromStg(array.GetNode(i));
                m_Items.InnerList.Add(tableItem);
            }
        }

        public bool CheckEmptySlabs()
        {
            foreach(RulesTableItem item in m_Items)
            {
                if (item.slab == null || item.slabProfile == null)
                {
                    return false;
                }
            }
            return true;
        }

        public override void SaveToStg(StgNode node)
        {
            var array = node.AddArray("RulesTableItems", StgType.Node);
            array.ItemsName = "RulesTableItem";
            for (int i = 0; i < m_Items.Count; i++)
            {
                m_Items[i].SaveToStg(array.AddNode());
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlabsPlugin
{
    public class SlabType
    {
        private string name; // Наименование
        public string Name
        {
            get => name;
            set => name = value;

        }
        private double length; // Длина
        public double Length
        {
            get => length;
            set => length = value;

        }
        private string dxfPath; // Путь к чертежу
        public string DxfPath
        {
            get => dxfPath;
            set => dxfPath = value;

        }
        private double scale = 1.0; // Масштаб чертежа
        public double Scale
        {
            get => scale;
            set => scale = value;

        }
    }

    public class SlabTypes
    {

    }
}

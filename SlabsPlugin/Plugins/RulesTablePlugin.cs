﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using Topomatic.FoundationClasses;
using Topomatic.Stg;

namespace SlabsPlugin
{
    public class RulesTablePlugin : UndoObject, IItem, IStgSerializable, IOwned, IAlignmentContainer
    {
        private object m_Parent = null;
        private RulesTable r_Table;
        private RailSlabsTable rs_Table;
        private FoundationSlabTable fs_Table;
        private CoordinateTable co_Table;

        public RulesTablePlugin(object parent)
        {
            m_Parent = parent;
            r_Table = new RulesTable(this);
            rs_Table = new RailSlabsTable(this);
            fs_Table = new FoundationSlabTable(this);
            co_Table = new CoordinateTable(this);
        }
        public RulesTable RulesTable { get => r_Table; set => r_Table = value; }

        public RailSlabsTable RailsSlabsTable { get => rs_Table; set => rs_Table = value; }

        public FoundationSlabTable FoundationSlabTable { get => fs_Table; set => fs_Table = value; }

        public CoordinateTable CoordinateTable { get => co_Table; set => co_Table = value; }

    #region IItem Members

    /// <summary>
    /// Родитель
    /// </summary>
    public object Parent
        {
            get => m_Parent;
            set => value = value;
        }

        #endregion


        #region IStgSerializable Members

        /// <summary>
        /// Загрузка элементов
        /// </summary>
        /// <param name="node">Узел откуда загружаем</param>
        public void LoadFromStg(StgNode node)
        {
            r_Table.LoadFromStg(node.GetNode("RulesTable"));
            rs_Table.LoadFromStg(node.GetNode("RailSlabsTable"));
            fs_Table.LoadFromStg(node.GetNode("FoundationSlabTable"));
            co_Table.LoadFromStg(node.GetNode("CoordinateTable"));
        }

        /// <summary>
        /// Сохранение элементов
        /// </summary>
        /// <param name="node">Узел куда загружаем</param>
        public void SaveToStg(StgNode node)
        {
            r_Table.SaveToStg(node.AddNode("RulesTable"));
            rs_Table.SaveToStg(node.GetNode("RailSlabsTable"));
            fs_Table.SaveToStg(node.GetNode("FoundationSlabTable"));
            co_Table.SaveToStg(node.GetNode("CoordinateTable"));

        }

        #endregion

        #region IOwned Members

        /// <summary>
        /// Владелец
        /// </summary>
        public object Owner 
        { 
            get => m_Parent;
            set => throw new NotSupportedException();
        }

        #endregion

        #region IAlignmentContainer Members

        /// <summary>
        /// Подобъект
        /// </summary>
        public Alignment Alignment { get => m_Parent is IAlignmentContainer ? ((IAlignmentContainer)m_Parent).Alignment : null; }

        #endregion

        #region IAlignmentPluginsEventHandler Members

        /// <summary>
        /// Ловим события которые подобъект отправляет своим плагинам
        /// </summary>
        /// <param name="sender">Подобъект</param>
        /// <param name="e">Параметры события</param>
        public void HandleEvent(object sender, EventArgs e)
        {
            //События объявлены в Topomatic.Alg.Plugins
            //При необходимости здесь можно перехватить и обработать события типа разрезать подобъект, склить подобъект, копировать и т.п.
        }

        #endregion
    }
}

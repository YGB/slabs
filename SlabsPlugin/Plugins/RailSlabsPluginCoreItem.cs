﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg.Core.Structure;
using Topomatic.FoundationClasses;
using Topomatic.ApplicationPlatform.UserSettings;
using Topomatic.Alg.Runtime.ExpImp;
using Topomatic.Cad.Foundation;
using Topomatic.Stg;
using Topomatic.Alg;
using Topomatic.ApplicationPlatform;
using Topomatic.Controls;
using Topomatic.FoundationClasses.Undo;
using Topomatic.Alg.Core.Structure;
using Topomatic.FoundationClasses;
using Topomatic.ApplicationPlatform.UserSettings;
using Topomatic.Alg.Runtime.ExpImp;
using Topomatic.Cad.Foundation;
using Topomatic.Stg;
using Topomatic.Alg;
using Topomatic.ApplicationPlatform;
using Topomatic.Controls;
using Topomatic.FoundationClasses.Undo;

namespace SlabsPlugin
{
    /// <summary>
    /// Элемент структуры подобъекта платформы Топоматик Робур
    /// Отнаследован от базового класса AlignmentStructureCoreItem<AlignmentPlugin>
    /// Поддерживает интерфесы: 
    /// INamedObject для отображения имени в стркутуре 
    /// IUserSettingsProvider для отображения настроек в списке настроек модели подобъекта
    /// </summary>
    class RailSlabsPluginCoreItem : AlignmentStructureCoreItem<RulesTablePlugin>, INamedObject
    {
        /// <summary>
        /// Класс для импорта в обменном формате Топоматик Робур
        /// </summary>
        private class StgProvider : ExchangeStgProvider<RulesTablePlugin>
        {
            public StgProvider()
            {
            }

            /// <summary>
            /// Сохраняем
            /// </summary>
            /// <param name="node">Узел куда сохраняем</param>
            /// <param name="exchangeObject">Плагин</param>
            protected override void DoExport(StgNode node, RulesTablePlugin exchangeObject)
            {
                var alias_node = node.GetNode("RailSlabsTablePlugin");
                exchangeObject.SaveToStg(alias_node);
            }

            /// <summary>
            /// Загружаем
            /// </summary>
            /// <param name="node">Узел откуда загружаем</param>
            /// <param name="exchangeObject">Плагин</param>
            protected override void DoImport(StgNode node, RulesTablePlugin exchangeObject)
            {
                exchangeObject.BeginUpdate("Импорт");
                try
                {
                    //На данный момент откат изменений не поддерживается
                    if (exchangeObject.TransactionManager != null)
                        exchangeObject.TransactionManager.PushCommand(new IrreversibleCommand());
                    exchangeObject.LoadFromStg(node.GetNode("RailSlabsTablePlugin"));
                }
                finally
                {
                    exchangeObject.EndUpdate();
                }
            }
        }

        /// <summary>
        /// Возвращает массив классов реализующих импорт/экспорт в различные форматы
        /// </summary>
        /// <returns>Массив классов реализующих импорт/экспорт в различные форматы</returns>
        protected override ExchangeProvider<RulesTablePlugin>[] GetProviders()
        {
            return new ExchangeProvider<RulesTablePlugin>[] { new StgProvider() };
        }

        public RailSlabsPluginCoreItem(Alignment alignment, RulesTablePlugin wrappedObject)
            : base(alignment, wrappedObject)
        {
        }

        #region INamedObject Members

        /// <summary>
        /// Возвращает имя элемента в структуре подобъекта
        /// </summary>
        public string Name
        {
            get
            {
                return "Таблица раскладки плит";
            }
            set
            {
                //throw new NotSupportedException();
            }
        }

        #endregion

        /// <summary>
        /// Вызывается когда пользователь выбирает команду открыть или два раза щелкает на элементе в структуре проекта
        /// </summary>
        /// <param name="sender">Отправитель команды</param>
        /// <param name="e">Параметры команды</param>
        protected override void OpenForEdit(object sender, MenuActionEventArgs e)
        {
            //Находим необходимую нам команду по ее ID
            var action = (CallAction)ApplicationHost.Current.AddIns["ID_SLAB_RailSlabs_EDIT_TABLE"];
            //Создаем и заполняем список параметров
            var args = new ExecuteEventArgs();
            args.ObjectParams = new object[] { Alignment, WrappedObject };
            //Вызываем команду
            action.PerformExecute(args);
        }

        /// <summary>
        /// Список возможных действий для пользователя
        /// </summary>
        protected override AlignmentStructureCoreItem.MenuOptions Options
        {
            get
            {
                var alignment = Alignment;
                //Если подобъект находится в режиме коллективного редактирования - запретить все кроме экспорта
                if (alignment.IsLimitedChange && alignment.HasSynchronizedAlignment)
                {
                    return MenuOptions.CanExport;
                }
                return base.Options;
            }
        }
    }
}

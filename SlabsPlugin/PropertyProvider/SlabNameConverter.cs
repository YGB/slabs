﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SlabsPlugin
{
    public static class SlabNameConverter
    {
        public static string DataToWrapper(string name)
        {
            if (name != null)
            {
                return name.Replace("SlabMod_", "").Replace("_dim", "");
            } else
            {
                return name;
            }
        }

        public static string WrapperToData(string name, bool withDimensions = false)
        {
            if (name != null)
            {
                if (withDimensions == true)
                {
                    return String.Format("SlabMod_{0}_dim", name);
                }
                else
                {
                    return String.Format("SlabMod_{0}", name);
                }
            } else
            {
                return name;
            }
        }
    }
}

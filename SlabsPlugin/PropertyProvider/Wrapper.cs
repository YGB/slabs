﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using Topomatic.Alg;
using Topomatic.Alg.Runtime.Design;
using Topomatic.Alg.Stationing;
using Topomatic.ComponentModel;
using Topomatic.ComponentModel.Design;
using Topomatic.Controls;
using Topomatic.Controls.Dialogs;
using Topomatic.Stg;

namespace SlabsPlugin
{
    /*
        Коротко о управлении отображением свойств. 

        За отображение конкретного свойства в инспекторе отвечает класс PropertyProvider. 
        Назначить собственный провайдер на свойство можно аттрибутом PropertyProvider
            Метод GetProperties возвращает те свойства инспектора, которые необходимо отобразить для заданного свойства класса.
            См. пример ObjectProvider.
        За отображение в конкретном свойстве инспектора отвечает тип PropertyTypeConverter. 
            Назначить собственный конвертер на свойство можно аттрибутом PropertyTypeConverter
            При необходимости изменить отображение свойства, можно назначить только конвертер.
            См. пример ExampleEnumConverter и ObjectConverter
        За отображение в конкретном свойстве инспектора отвечает тип PropertyEditor. 
            Назначить собственный редактор на свойство можно аттрибутом PropertyTypeEditor
            При необходимости изменить только редактирование свойства, можно назначить только редактор.
            См. пример ListEditor и ObjectEditor

        Для редактирования пикетажа мы используем наш провайер StationPropertyProvider. Чтобы иметь возможность выбирать пикетаж в окне плана
        используем аттрибут StationFromPlan. Чтобы это работало класс у которого инспектируются свойства должен поддерживать интерфейс
        IStationingContainer, а для работы аттрибута StationFromPlan при инспектировании в собственном диалоге необходимо, чтобы диалог был унаследован
        от SimpleDlg и вызван через метод PostExecute. Метод PostExecute позволяет нам закрыть диалог, выполнить какие-либо действия и 
        показать диалог обратно. 

        Для тестирования можно воспользоваться следующим псеводокодом:

            Alignment alignment = ...

            EditTableDlg.Execute("Test", new Wrapper(alignment.Stationing), false, false);

    */

    enum ExampleEnum
    {
        Apple,
        Potato,
        Banana
    }

    class ExampleObject
    {
        public string Name
        {
            get;
            set;
        }
    }

    //Инспектируемый объект
    class Item : IStationingContainer
    {
        //Интерфейс пикетажа для работы StationFromPlan и StationPropertyProvider
        private IAlgStationing m_Stationing;

        public Item(IAlgStationing stationing)
        {
            m_Stationing = stationing;
            this.ConditionalListProp = "Value1";
        }

        //То что Вас интересовало изначально - кнопка для выбора пикета с плана
        [PropertyProvider(typeof(StationPropertyProvider)), StationFromPlan]
        public double StationFromPlan
        {
            get;
            set;
        }

        //Реализация собственного выбора через модальный диалог для сложного объекта
        [PropertyProvider(typeof(ObjectProvider))]
        public ExampleObject Obj
        {
            get;
            set;
        }

        //Реализация отображения перечисления
        //PropertyUpdateSequence(PropertyUpdateSequence.Reload) означат, что при изменении этого свойства
        //все свойства будут пересозданы заново, для того чтобы управлять, например ReadOnly 
        [PropertyTypeConverter(typeof(ExampleEnumConverter)), PropertyUpdateSequence(PropertyUpdateSequence.Reload)]
        public ExampleEnum Enum
        {
            get;
            set;
        }

        //Условие, для аттрибута ConditionalReadOnly - имя свойства совпадает с параметром аттрибута
        //Если выбрали банан - то свойство ReadOnly
        private bool IsReadOnlyConditional
        {
            get
            {
                return this.Enum == ExampleEnum.Banana;
            }
        }

        //Простое строковое свойство, с возможностью редактирования как вручную, так и выбора из списка значений
        [ConditionalReadOnly("IsReadOnlyConditional"), PropertyEditor(typeof(ListEditor))]
        public string ConditionalListProp
        {
            get;
            set;
        }

        #region IStationingContainer

        //Интерфейс пикетажа для работы StationFromPlan и StationPropertyProvider
        [Browsable(false)]
        public IAlgStationing Stationing
        {
            get
            {
                return m_Stationing;
            }
        }

        #endregion
    }

    //Список инспектируемых объектов
    class Wrapper : List<Item>, IActivator, ISupportClipboard, IStationingContainer
    {
        //Интерфейс пикетажа для работы StationFromPlan и StationPropertyProvider
        private IAlgStationing m_Stationing;

        public Wrapper(IAlgStationing stationing)
        {
            m_Stationing = stationing;
            this.Add(new Item(m_Stationing) { StationFromPlan = 0.0, Enum = ExampleEnum.Apple, Obj = new ExampleObject() { Name = "obj1" } });
            this.Add(new Item(m_Stationing) { StationFromPlan = 10.0, Enum = ExampleEnum.Apple, Obj = new ExampleObject() { Name = "obj2" } });
            this.Add(new Item(m_Stationing) { StationFromPlan = 20.0, Enum = ExampleEnum.Apple, Obj = new ExampleObject() { Name = "obj3" } });
        }

        #region ISupportClipboard
        /*
         * Интерфейс позволяет управлять кнопками копировать и вставить в таблице
         */

        //Уникальный идентефикатор содержимого
        public string AliasName
        {
            get
            {
                return "OurCliboardUnicalUID";
            }
        }

        //Можно скопировать 
        public bool CanCopy
        {
            get
            {
                return true;
            }
        }

        //Можно вставить
        public bool CanPaste
        {
            get
            {
                return true;
            }
        }


        //Загрузка элемента из буфера обмена
        public void Load(object obj, StgNode node)
        {
            var item = obj as Item;
            //TODO: тут загрузка из node
            throw new NotImplementedException();
        }

        //Сохранени элемента в буфер обмена
        public void Save(object obj, StgNode node)
        {
            var item = obj as Item;
            //TODO: тут сохранение в node            
            throw new NotImplementedException();
        }

        #endregion

        #region IActivator
        /*
         * Интерфейс говорит о том, что список поддерживает создание элементов
         */


        //Можно ли создавать
        public bool CanCreateInstance
        {
            get
            {
                return true;
            }
        }

        //Создать новый экземпляр
        public object CreateInstance()
        {
            return new Item(m_Stationing);
        }

        #endregion

        #region IStationingContainer

        //Интерфейс пикетажа для работы StationFromPlan и StationPropertyProvider

        public IAlgStationing Stationing
        {
            get
            {
                return m_Stationing;
            }
        }

        #endregion
    }


    /// <summary>
    /// Простой конвертер для перечислений, обычно мы наследум от BaseEnumConverter для простоты разработки
    /// </summary>
    class ExampleEnumConverter : BaseEnumConverter
    {
        public ExampleEnumConverter()
        {
            this.Dictionary[ExampleEnum.Apple] = "Яблоко";
            this.Dictionary[ExampleEnum.Potato] = "Картофель";
            this.Dictionary[ExampleEnum.Banana] = "Банан";
        }

    }

    /// <summary>
    /// Простой редактор для списоков, обычно мы наследум от StandardValueEditor для простоты разработки
    /// </summary>

    class ListEditor : StandardValueEditor
    {
        protected override IEnumerable OnGetStandardValues(IPropertyTypeDescriptorContext context)
        {
            yield return "Value1";
            yield return "Value2";
            yield return "Value3";
        }
    }


    /// <summary>
    /// Провайдер для сложного свойства
    /// </summary>
    class ObjectProvider : PropertyProvider
    {
        /// <summary>
        /// Конвертер, который выводит строковое представление нашего объекта
        /// </summary>
        class ObjectConverter : PropertyTypeConverter
        {
            public override bool CanConvertFromString(Type sourceType)
            {
                throw new NotSupportedException();
            }

            public override bool CanConvertToString(Type sourceType)
            {
                return true;
            }

            public override object ConvertFromString(string value)
            {
                throw new NotSupportedException();
            }

            public override string ConvertToString(object value)
            {
                return ((ExampleObject)value).Name;
            }
        }

        /// <summary>
        /// Редактор для нашего объекта
        /// </summary>
        class ObjectEditor : PropertyEditor
        {
            public ObjectEditor()
            {
            }

            /// <summary>
            /// Стиль редактора
            /// </summary>
            /// <param name="context">Информация о свойстве и текущем значении</param>
            /// <returns>Стиль редактора</returns>
            public override PropertyTypeEditorEditStyle GetEditStyle(IPropertyTypeDescriptorContext context)
            {
                //Если необходимо реализовать одну или несколько собственных кнопок, то 
                //назначаем PropertyTypeEditorEditStyle.Custom и перекрывем метод GetCustomButtons где возвращаем рисунки кнопок
                return PropertyTypeEditorEditStyle.Modal;
            }


            public override object EditValue(IPropertyTypeDescriptorContext context, IPropertyWindowsFormsEditorService editorService, int button)
            {
                //button - индекс нажатой кнопки (в порядке, который вернул метод GetCustomButtons)
                if (button == 0)
                {
                    //Изменение объекта
                    var old_value = (ExampleObject)context.Value;
                    var new_value = new ExampleObject() { Name = old_value.Name + " новое" };
                    MessageDlg.Show(String.Format("Было {0} стало {1}", old_value.Name, new_value.Name));
                    return new_value;
                }
                return base.EditValue(context, editorService, button);
            }
        }


        /// <summary>
        /// Свойство инспектора для нашего объекта
        /// </summary>
        class ObjectProperty : SimpleProperty
        {
            private ObjectConverter m_Converter = new ObjectConverter();

            private ObjectEditor m_Editor = new ObjectEditor();

            public ObjectProperty(PropertyInfo property, object instance, object[] attributes)
                : base(property, instance, attributes)
            {
            }

            public override PropertyTypeConverter Converter
            {
                get
                {
                    return m_Converter;
                }
            }

            public override PropertyEditor Editor
            {
                get
                {
                    return m_Editor;
                }
            }

            public override bool IsEditable
            {
                get
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Метод возвращает свойства инспектора
        /// </summary>
        /// <param name="value">Инспектируемый объект</param>
        /// <param name="property">Информация о свойства</param>
        /// <param name="attributes">Аттрибуты свойства</param>
        /// <returns></returns>
        public override CustomProperty[] GetProperties(object value, PropertyInfo property, object[] attributes)
        {
            var item = value as Item;
            if (item != null)
            {
                //мы возвращаем одно свойство, но их может быть несколько, если необходимо
                return new CustomProperty[] { new ObjectProperty(property, value, attributes) };
            }
            return base.GetProperties(value, property, attributes);
        }
    }

}

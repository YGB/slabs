﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Topomatic.ComponentModel;
using Topomatic.ComponentModel.Design;
using Topomatic.Dwg.Layer;


namespace SlabsPlugin
{

	class ProfileSlabTypePropertyEditor : StandardValueEditor
	{

		protected override IEnumerable OnGetStandardValues(IPropertyTypeDescriptorContext context)
		{
			var cadView = SlabsConfig.Instance.cadView;
			if (cadView == null)
			{
				cadView = SlabsConfig.Instance.module.CadView;

			}

			var layer = DrawingLayer.GetDrawingLayer(cadView);
			if (layer != null)
			{
				var drawing = layer.Drawing;

				var elements = drawing.Blocks.Names.Keys;


				foreach (var s in elements.Where(el => ((el.ToLower().StartsWith("ngp") && !el.ToLower().EndsWith("_dim") && el.ToLower().EndsWith("_profile")))))
				{
					yield return s;
				}
			}

		}
	}
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Topomatic.Alg;
using static Topomatic.Alg.Plan.PlanLine;

namespace SlabsPlugin
{
    public enum PlanPartType { 
        Line,
        Curve
    }

    public struct PlanPart
    {
        public PlanPart(double startStation, double endStation, PlanPartType type, string description, Vertex vertex)
        {
            StartStation = startStation;
            EndStation = endStation;
            Type = type;
            Description = description;
            Vert = vertex;
        }

        public double StartStation;
        public double EndStation;
        public PlanPartType Type;
        public string Description;
        public Vertex Vert;
       
        
    }

    public static class PlanConverter
    {
        public static bool IsCurve(Vertex vertex)
        {
            return vertex.PlanData.StartStation == vertex.PlanData.EndStation ? false : true;
        }

        public static List<PlanPart> AlignmentToRules(Alignment alignment)
        {

            List<PlanPart> planParts = new List<PlanPart>();
            for (int i = 0; i < alignment.Plan.Count - 1; i++)
            {
                Vertex curVertex = alignment.Plan[i];
                Vertex nextVertex = alignment.Plan[i + 1];
                if (IsCurve(curVertex)) // Вертекс прямой
                {
                    var startStation = curVertex.PlanData.StartStation;
                    var endStation = curVertex.PlanData.EndStation;
                    planParts.Add(new PlanPart(startStation, endStation, PlanPartType.Curve, String.Format("Кривая № {0} R{1}", i, curVertex[0].R), curVertex));
                }
                planParts.Add(new PlanPart(curVertex.PlanData.EndStation, nextVertex.PlanData.StartStation, PlanPartType.Line, "Прямая", curVertex));
            }

            return planParts;
        }

        public static List<PlanPart> AlignmentAndRulesToParts(Alignment alignment, RulesTable rules)
        {
            List<PlanPart> planParts = new List<PlanPart>();
            List<double> ruleThresholds = new List<double>();

            foreach (RulesTableItem rule in rules)
            {
                ruleThresholds.Add(rule.stationStart);
                ruleThresholds.Add(rule.stationEnd);
            }
            double startStation;
            double endStation;
            string description;
            double curStation;
            List<double> thresholds;
            for (int i = 0; i < alignment.Plan.Count - 1; i++)
            {
                Vertex curVertex = alignment.Plan[i];
                Vertex nextVertex = alignment.Plan[i + 1];
                if (IsCurve(curVertex)) // Вертекс прямой
                {
                    startStation = curVertex.PlanData.StartStation;
                    endStation = curVertex.PlanData.EndStation;
                    description = String.Format("Кривая № {0} R{1}", i, curVertex[0].R);

                    thresholds = ruleThresholds.Where(threshold => threshold > startStation && threshold < endStation).ToList();

                    if (thresholds.Count() > 0)
                    {
                        foreach (var threshold in thresholds)
                        {
                            planParts.Add(new PlanPart(startStation, threshold, PlanPartType.Curve, description, curVertex));
                            startStation = threshold;
                        }
                        planParts.Add(new PlanPart(startStation, endStation, PlanPartType.Curve, description, curVertex));

                    } else
                    {

                        planParts.Add(new PlanPart(startStation, endStation, PlanPartType.Curve, description, curVertex));
                    }
                }

                startStation = curVertex.PlanData.EndStation;
                endStation = nextVertex.PlanData.StartStation;
                description = "Прямая";

                thresholds = ruleThresholds.Where(threshold => threshold > startStation && threshold < endStation).ToList();

                if (thresholds.Count() > 0)
                {
                    foreach (var threshold in thresholds)
                    {
                        planParts.Add(new PlanPart(startStation, threshold, PlanPartType.Curve, description, curVertex));
                        startStation = threshold;
                    }
                    planParts.Add(new PlanPart(startStation, endStation, PlanPartType.Curve, description, curVertex));

                }
                else
                {

                    planParts.Add(new PlanPart(startStation, endStation, PlanPartType.Curve, description, curVertex));
                }
            }


            return planParts;

        }
    }
}
